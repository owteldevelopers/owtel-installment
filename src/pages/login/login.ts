import { Component, Input, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { AlertController, LoadingController, Loading } from 'ionic-angular';
import { ToastController } from "ionic-angular/index";

import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Device } from '@ionic-native/device';

import { Methods } from "../../globals/methods";
import { ApplyPage } from "../apply/apply";
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { MobileNumberVerificationPage } from "../mobile-number-verification/mobile-number-verification";
import { MobileNumberAttentionPage } from "../mobile-number-attention/mobile-number-attention";
import { MobileNumberMatchingPage } from "../mobile-number-matching/mobile-number-matching";

import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Network } from '@ionic-native/network';

import { TranslateService } from '@ngx-translate/core';
import * as $ from 'jquery';
import { LocalNotifications } from '@ionic-native/local-notifications';


@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
})

export class LoginPage {
    TAG: String = "LoginPage";

    validations_form: FormGroup;
    loading: Loading;
    uuid: any = "";
    @Input() getCapital: any;
    noSpecialChar : any;

    prompt: any;
    loader: any;
    button: any;
    tabBarElement : any;

    @ViewChild('idHKID') idHKID;
    @ViewChild('idFirst') idFirst;
    @ViewChild('idMiddle') idMiddle;
    @ViewChild('idLast') idLast;
    @ViewChild('number') number;

    constructor(public navCtrl: NavController,
         public localNotifications: LocalNotifications
        , private platform: Platform
        , private device: Device
        , private methods: Methods
        , public loadingCtrl: LoadingController
        , public alertCtrl: AlertController
        , public androidPermissions: AndroidPermissions
        , public authService: AuthServiceProvider
        , private network: Network
        , private toast: ToastController
        , private translate: TranslateService) {

        this.tabBarElement = $('.tabbar').length;

    }

    ionViewDidLoad() {
        if(this.tabBarElement == 1){

            $('.tabbar').css({ display: "none" })
            //this.tabBarElement.style.display = 'none';
        }
        console.log('ionViewDidLoad LoginPage');

    }



    /**
     * Ask for SMS permission upon entering the page
     */
    ionViewWillEnter() {
        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_SMS).then(
            (success) => console.log('Permission granted'),
            (err) => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_SMS)
        );

        this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_SMS]);

    }




    /**
     * Check internet connection
     */
    ionViewDidEnter() {

        let msg = "";


        this.translate.get('PROMPT').subscribe(
            translated => {
                this.prompt = translated;
            }
        );

        this.translate.get('LOADER').subscribe(
            translated => {
                this.loader = translated;
            }
        );

        this.translate.get('BUTTON').subscribe(
            translated => {
                this.button = translated;
            }
        );
    }


/*     initPushNotification(){

        this.authService.getPushNotification().subscribe(
        resp => {
            console.log(resp);

            var get_data = resp.data[0];

            this.localNotifications.schedule({
                id: get_data.id,
                text: '<p>'+get_data.message+'</p><a href = "'+get_data.redirect+'"><img src = "'+get_data.image_path+'"/></a>',
                title: get_data.title,
                led: 'FF0000',
                trigger: { at: new Date(new Date().getTime() + 3600) }

            });

            }

        );

    }   */

    /**
     * Login function
     */

    /* login() {
        let hkidF = this.idFirst.value;
        if (hkidF.trim() != "") {
            if (hkidF.length == 1) {
                hkidF = hkidF + ' ';
            }
        }

        let id = hkidF + this.idMiddle.value + this.idLast.value;

        if ((id.trim() != "") && (this.number.value.trim() != "")) {
            if (this.network.type != 'none') {
                this.loading = this.methods.createLoader(this.loadingCtrl, `<ion-spinner >` + this.loader.VERIFY + `</ion-spinner>`);
                this.loading.present().then(() => {
                    this.authService.verifyID(id, this.number.value, 1).subscribe(
                        resp => {
                            console.log(resp);
                            this.loading.dismiss();

                            var status = resp.status;
                            if (status == "success") {
                                var second = resp.seconds;
                                var title = resp.title;
                                var subTitle = resp.subTitle;

                                this.navCtrl.push(MobileNumberVerificationPage, {
                                    id: id
                                    , number: this.number.value
                                    , numberMask: this.methods.masknumber(this.number.value)
                                    , time: second
                                    , title: title
                                    , subTitle: subTitle
                                });

                            } else if (status == "unauthorized") {
                                var title = resp.title;
                                var subTitle = resp.subTitle;

                                this.navCtrl.push(MobileNumberAttentionPage, {
                                    id: id
                                    , number: this.number.value
                                    , numberMask: this.methods.masknumber(this.number.value)
                                    , title: title
                                    , subTitle: subTitle
                                });

                            } else if (status == "mismatched") {
                                var title = resp.title;
                                var subTitle = resp.subTitle;

                                this.navCtrl.push(MobileNumberMatchingPage, {
                                    title: title
                                    , subTitle: subTitle
                                });

                            } else {
                                this.navCtrl.push(ApplyPage);

                            }
                        }, error => {
                            console.log(error);
                            this.loading.dismiss();

                            if (error.name == 'TimeoutError') {
                                this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_ERR, this.prompt.MESSAGE.NET, this.button.OK);

                            } else {
                                this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.GEN_ERR, this.button.OK);

                            }
                        })
                });
            } else {
                this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);
            }

        } else {
            this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.INV_HKID_NUM, this.button.OK);
        }


    } */

    login() {
        let hkidF = this.idHKID.value.toUpperCase();
        /* if (hkidF.trim() != "") {
            if (hkidF.length == 1) {
                hkidF = hkidF + ' ';
            }
        } */

        let id = hkidF /* + this.idMiddle.value + this.idLast.value */;

        var firstString = id.substr(0, 1);
        var secString = id.substr(1, 1);
        var thirdString = id.substr(2, 1);
        var fourthString = id.substr(3, 1);
        var fifthString = id.substr(4, 1);
        var sixthString = id.substr(5, 1);
        var seventhString = id.substr(6, 1);
        var eightString = id.substr(7, 1);
        var ninthString = id.substr(8, 1);

        if ((id.trim() != "") && (this.number.value.trim() != "")) {
            if (this.network.type != 'none') {
                this.loading = this.methods.createLoader(this.loadingCtrl, `<ion-spinner >` + this.loader.VERIFY + `</ion-spinner>`);
                this.loading.present().then(() => {
                    this.authService.verifyID(id, this.number.value, 1).subscribe(
                        resp => {
                            console.log(resp);


                            var status = resp.status;
                            if (status == "success") {

                                //this.tabBarElement.style.display = 'block';

                                var second = resp.seconds;
                                var title = resp.title;
                                var subTitle = resp.subTitle;

                                this.navCtrl.push(MobileNumberVerificationPage, {
                                      id: id
                                    , number: this.number.value
                                    , numberMask: this.methods.masknumber(this.number.value)
                                    , time: second
                                    , title: title
                                    , subTitle: subTitle
                                });

                            } else if (status == "unauthorized") {
                                var title = resp.title;
                                var subTitle = resp.subTitle;

                                this.navCtrl.push(MobileNumberAttentionPage, {
                                    id: id
                                    , number: this.number.value
                                    , numberMask: this.methods.masknumber(this.number.value)
                                    , title: title
                                    , subTitle: subTitle
                                });

                            } else if (status == "mismatched") {
                                var title = resp.title;
                                var subTitle = resp.subTitle;

                                this.navCtrl.push(MobileNumberMatchingPage, {
                                    title: title
                                    , subTitle: subTitle
                                });

                            } else {

                                let format_special_letter = new RegExp("^[^<>{}\"/|;:.,~!?@#$%^=&*\\]\\\\()\\[¿§«»ω⊙¤°℃℉€¥£¢¡®©0-9_+]*$");
                                let format_special_num = new RegExp("^[^<>{}\"/|;:.,~!?@#$%^=&*\\]\\\\()\\[¿§«»ω⊙¤°℃℉€¥£¢¡®©A-Za-z_+]*$");
                                let format_special_char = new RegExp("[^<>{}\"/|;:.,~!?@#$%^=&*\\]\\\\()\\[¿§«»ω⊙¤°℃℉€¥£¢¡®©_+]*$");

                                let format_o = new RegExp("^[Oo]*$");

                                if (firstString.match(format_special_num)) {
                                    this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.INV_LETTER_ONLY, this.button.OK);
                                } else if (thirdString.match(format_special_letter) || fourthString.match(format_special_letter) || fifthString.match(format_special_letter) || sixthString.match(format_special_letter) || seventhString.match(format_special_letter) || eightString.match(format_special_letter)) {
                                    this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.INV_NUMBERS_ONLY, this.button.OK);
                                } /* else if (ninthString.match(format_o)) {
                                    this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.INV_CHARACTER_O, this.button.OK);
                                } */ /* else if (ninthString.match(format_special_char) && ninthString != '') {
                                    this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.INV_LAST_CHAR, this.button.OK);
                                } */ else if ((id.trim().length >= 10)) {
                                    this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.INV_HKID_MAX, this.button.OK);
                                } else if ((id.trim().length < 9)) {
                                    this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.INV_HKID_MAX, this.button.OK);
                                } else {
                                    this.navCtrl.push(ApplyPage);
                                }

                                //this.navCtrl.push(ApplyPage);

                            }

                            this.loading.dismiss();
                        }, error => {
                            console.log(error);
                            this.loading.dismiss();

                            if (error.name == 'TimeoutError') {
                                this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_ERR, this.prompt.MESSAGE.NET, this.button.OK);

                            } else {
                                this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.GEN_ERR, this.button.OK);

                            }
                        })
                });
            } else {
                this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);
            }

        } else {
            this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.INV_HKID_NUM, this.button.OK);
        }


    }

    /**
     * Open application page
     */
    newApplicant() {
        this.navCtrl.push(ApplyPage);
    }

    /**
     * For autofocus to next textbox
     *
     * @param myText
     *
     * TODO: optimize because this is called twice
     */
    onChangeTextFirst(myText) {

        if (myText.value.length == 2) {
            this.idMiddle.setFocus();

        }

        $('input[type="text"]').val(function(){
            return this.value.toUpperCase();
        });

    }

    /**
     * For autofocus to next textbox
     *
     * @param myText
     *
     * TODO: optimize because this is called twice
     */



    onKeyupHKID(e) {
        let elementChecker: string;
        elementChecker = e.target.value;

        let format = new RegExp('^[A-Za-z0-9]+$');

        var firstString = elementChecker.substr(0, 1);
        var secString = elementChecker.substr(1, 1);
        var thirdString = elementChecker.substr(2, 1);
        var fourthString = elementChecker.substr(3, 1);
        var fifthString = elementChecker.substr(4, 1);
        var sixthString = elementChecker.substr(5, 1);
        var seventhString = elementChecker.substr(6, 1);
        var eightString = elementChecker.substr(7, 1);
        var ninthString = elementChecker.substr(8, 1);
       /*
        console.log(this.device.version); */


        let platformVersion = this.platform.versions();

        if (platformVersion.android) {
            let androidV = platformVersion.android.num;
            console.log(androidV);
            if (androidV <= 4.4) {
                if (secString.charCodeAt(0) >= 48 && secString.charCodeAt(0) <= 57) {
                    this.getCapital = secString.replace(secString, firstString + ' ').concat(secString);
                }
            } else {
                if ((firstString.charCodeAt(0) >= 48 && firstString.charCodeAt(0) <= 57) || (firstString.charCodeAt(0) == 32) || (firstString.charCodeAt(0) == 95)) {
                    this.getCapital = elementChecker.slice(0, -1);

                } else if ((secString.charCodeAt(0) >= 48 && secString.charCodeAt(0) <= 57)) {
                    this.getCapital = secString.replace(secString, firstString + ' ').concat(secString);

                } else if (secString.charCodeAt(0) == 95) {
                    this.getCapital = elementChecker.slice(0, -1);

                } else if ((thirdString.charCodeAt(0) >= 65 && thirdString.charCodeAt(0) <= 90) || (thirdString.charCodeAt(0) == 32) || (thirdString.charCodeAt(0) == 95) || (fourthString.charCodeAt(0) >= 65 && fourthString.charCodeAt(0) <= 90) || (fourthString.charCodeAt(0) == 32) || (fourthString.charCodeAt(0) == 95) || (fifthString.charCodeAt(0) >= 65 && fifthString.charCodeAt(0) <= 90) || (fifthString.charCodeAt(0) == 32) || (fifthString.charCodeAt(0) == 95) || (sixthString.charCodeAt(0) >= 65 && sixthString.charCodeAt(0) <= 90) || (sixthString.charCodeAt(0) == 32) || (sixthString.charCodeAt(0) == 95) || (seventhString.charCodeAt(0) >= 65 && seventhString.charCodeAt(0) <= 90) || (seventhString.charCodeAt(0) == 32) || (seventhString.charCodeAt(0) == 95) || (eightString.charCodeAt(0) >= 65 && eightString.charCodeAt(0) <= 90) || (eightString.charCodeAt(0) == 32) || (eightString.charCodeAt(0) == 95)) {
                    this.getCapital = elementChecker.slice(0, -1);

                } else if ((ninthString.charCodeAt(0) == 79) || (ninthString.charCodeAt(0) == 32) || (ninthString.charCodeAt(0) == 95)) {
                    this.getCapital = elementChecker.slice(0, -1);
                } else {
                    this.getCapital = elementChecker.replace(/[^\w\s]/gi, '');
                }
            }

        }else {
            if ((firstString.charCodeAt(0) >= 48 && firstString.charCodeAt(0) <= 57) || (firstString.charCodeAt(0) == 32) || (firstString.charCodeAt(0) == 95)) {
                this.getCapital = elementChecker.slice(0, -1);

            } else if ((secString.charCodeAt(0) >= 48 && secString.charCodeAt(0) <= 57)) {
                this.getCapital = secString.replace(secString, firstString + ' ').concat(secString);

            } else if (secString.charCodeAt(0) == 95) {
                this.getCapital = elementChecker.slice(0, -1);

            } else if ((thirdString.charCodeAt(0) >= 65 && thirdString.charCodeAt(0) <= 90) || (thirdString.charCodeAt(0) == 32) || (thirdString.charCodeAt(0) == 95) || (fourthString.charCodeAt(0) >= 65 && fourthString.charCodeAt(0) <= 90) || (fourthString.charCodeAt(0) == 32) || (fourthString.charCodeAt(0) == 95) || (fifthString.charCodeAt(0) >= 65 && fifthString.charCodeAt(0) <= 90) || (fifthString.charCodeAt(0) == 32) || (fifthString.charCodeAt(0) == 95) || (sixthString.charCodeAt(0) >= 65 && sixthString.charCodeAt(0) <= 90) || (sixthString.charCodeAt(0) == 32) || (sixthString.charCodeAt(0) == 95) || (seventhString.charCodeAt(0) >= 65 && seventhString.charCodeAt(0) <= 90) || (seventhString.charCodeAt(0) == 32) || (seventhString.charCodeAt(0) == 95) || (eightString.charCodeAt(0) >= 65 && eightString.charCodeAt(0) <= 90) || (eightString.charCodeAt(0) == 32) || (eightString.charCodeAt(0) == 95)) {
                this.getCapital = elementChecker.slice(0, -1);

            } else if ((ninthString.charCodeAt(0) == 79) || (ninthString.charCodeAt(0) == 32) || (ninthString.charCodeAt(0) == 95)) {
                this.getCapital = elementChecker.slice(0, -1);
            } else {
                this.getCapital = elementChecker.replace(/[^\w\s]/gi, '');
            }
        }

        $('.inputHKID').find('input[type="text"]').val(function () {
            return this.value.toUpperCase();
        });


    }

    noSpecialCharEvnt(e) {
        let elementChecker: string;
        let format = new RegExp('^[A-Za-z0-9]+$');
        elementChecker = e.target.value;

        if (!format.test(elementChecker)) {
            this.noSpecialChar = elementChecker.slice(0, -1);
        }

    }



}
