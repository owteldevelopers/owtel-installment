var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AlertController, LoadingController } from 'ionic-angular';
import { ToastController } from "ionic-angular/index";
import { Methods } from "../../globals/methods";
import { ApplyPage } from "../apply/apply";
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { MobileNumberVerificationPage } from "../mobile-number-verification/mobile-number-verification";
import { MobileNumberAttentionPage } from "../mobile-number-attention/mobile-number-attention";
import { MobileNumberMatchingPage } from "../mobile-number-matching/mobile-number-matching";
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Network } from '@ionic-native/network';
import { TranslateService } from '@ngx-translate/core';
var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, methods, loadingCtrl, alertCtrl, androidPermissions, authService, network, toast, translate) {
        this.navCtrl = navCtrl;
        this.methods = methods;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.androidPermissions = androidPermissions;
        this.authService = authService;
        this.network = network;
        this.toast = toast;
        this.translate = translate;
        this.TAG = "LoginPage";
        this.uuid = "";
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    /**
     * Ask for SMS permission upon entering the page
     */
    LoginPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_SMS).then(function (success) { return console.log('Permission granted'); }, function (err) { return _this.androidPermissions.requestPermission(_this.androidPermissions.PERMISSION.READ_SMS); });
        this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_SMS]);
    };
    /**
     * Check internet connection
     */
    LoginPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        var msg = "";
        // this.network.onConnect().subscribe(data => {
        //     msg = "Connected.";
        //     this.methods.createToast(this.toast, 1, msg, 'bottom');
        // }, error => console.error(error));
        // this.network.onDisconnect().subscribe(data => {
        //     msg = "Disconnected.";
        //     this.methods.createToast(this.toast, 1, msg, 'bottom');
        // }, error => console.error(error));
        this.translate.get('PROMPT').subscribe(function (translated) {
            _this.prompt = translated;
        });
        this.translate.get('LOADER').subscribe(function (translated) {
            _this.loader = translated;
        });
        this.translate.get('BUTTON').subscribe(function (translated) {
            _this.button = translated;
        });
    };
    /**
     * Login function
     */
    LoginPage.prototype.login = function () {
        var _this = this;
        var hkidF = this.idFirst.value;
        if (hkidF.trim() != "") {
            if (hkidF.length == 1) {
                hkidF = hkidF + ' '; /** auto fill of space if user enters only one character */
            }
        }
        var id = hkidF + this.idMiddle.value + this.idLast.value;
        if ((id.trim() != "") && (this.number.value.trim() != "")) {
            if (this.network.type != 'none') {
                this.loading = this.methods.createLoader(this.loadingCtrl, "<ion-spinner >" + this.loader.VERIFY + "</ion-spinner>");
                this.loading.present().then(function () {
                    _this.authService.verifyID(id, _this.number.value, 1).subscribe(function (resp) {
                        console.log(resp);
                        _this.loading.dismiss();
                        var status = resp.status;
                        if (status == "success") {
                            var second = resp.seconds;
                            var title = resp.title;
                            var subTitle = resp.subTitle;
                            _this.navCtrl.push(MobileNumberVerificationPage, {
                                id: id,
                                number: _this.number.value,
                                numberMask: _this.methods.masknumber(_this.number.value),
                                time: second,
                                title: title,
                                subTitle: subTitle
                            });
                        }
                        else if (status == "unauthorized") {
                            var title = resp.title;
                            var subTitle = resp.subTitle;
                            _this.navCtrl.push(MobileNumberAttentionPage, {
                                id: id,
                                number: _this.number.value,
                                numberMask: _this.methods.masknumber(_this.number.value),
                                title: title,
                                subTitle: subTitle
                            });
                        }
                        else if (status == "mismatched") {
                            var title = resp.title;
                            var subTitle = resp.subTitle;
                            _this.navCtrl.push(MobileNumberMatchingPage, {
                                title: title,
                                subTitle: subTitle
                            });
                        }
                        else {
                            _this.navCtrl.push(ApplyPage);
                        }
                    }, function (error) {
                        console.log(error);
                        _this.loading.dismiss();
                        if (error.name == 'TimeoutError') {
                            _this.methods.showErrorOkButton(_this.alertCtrl, _this.prompt.TITLE.NET_ERR, _this.prompt.MESSAGE.NET, _this.button.OK);
                        }
                        else {
                            _this.methods.showErrorOkButton(_this.alertCtrl, '', _this.prompt.MESSAGE.GEN_ERR, _this.button.OK);
                        }
                    });
                });
            }
            else {
                this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);
            }
        }
        else {
            this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.INV_HKID_NUM, this.button.OK);
        }
    };
    /**
     * Open application page
     */
    LoginPage.prototype.newApplicant = function () {
        this.navCtrl.push(ApplyPage);
    };
    /**
     * For autofocus to next textbox
     *
     * @param myText
     *
     * TODO: optimize because this is called twice
     */
    LoginPage.prototype.onChangeTextFirst = function (myText) {
        if (myText.value.length == 2) {
            this.idMiddle.setFocus();
        }
    };
    /**
     * For autofocus to next textbox
     *
     * @param myText
     *
     * TODO: optimize because this is called twice
     */
    LoginPage.prototype.onChangeTextSecond = function (myText) {
        if (myText.value.length == 6) {
            this.idLast.setFocus();
        }
    };
    __decorate([
        ViewChild('idFirst'),
        __metadata("design:type", Object)
    ], LoginPage.prototype, "idFirst", void 0);
    __decorate([
        ViewChild('idMiddle'),
        __metadata("design:type", Object)
    ], LoginPage.prototype, "idMiddle", void 0);
    __decorate([
        ViewChild('idLast'),
        __metadata("design:type", Object)
    ], LoginPage.prototype, "idLast", void 0);
    __decorate([
        ViewChild('number'),
        __metadata("design:type", Object)
    ], LoginPage.prototype, "number", void 0);
    LoginPage = __decorate([
        Component({
            selector: 'page-login',
            templateUrl: 'login.html',
        }),
        __metadata("design:paramtypes", [NavController,
            Methods,
            LoadingController,
            AlertController,
            AndroidPermissions,
            AuthServiceProvider,
            Network,
            ToastController,
            TranslateService])
    ], LoginPage);
    return LoginPage;
}());
export { LoginPage };
//# sourceMappingURL=login.js.map