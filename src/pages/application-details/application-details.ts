import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {DocumentsPage} from "../documents/documents";
import {SchedulePage} from "../schedule/schedule";
import {PayPage} from "../pay/pay";

/**
 * Generated class for the ApplicationDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-application-details',
  templateUrl: 'application-details.html',
})
export class ApplicationDetailsPage {

  navParam : any;
  constructor(public navCtrl: NavController
      , public navParams: NavParams) {
    this.navParam = navParams.data.obj;
    console.log(this.navParam.statusCode);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ApplicationDetailsPage');
  }

  btnDocs() {
    console.log('btnDocs Clicked!');
    this.navCtrl.push(DocumentsPage, {instId: this.navParam.id, status: this.navParam.statusCode});
  }

  btnSched() {
    console.log('btnSched Clicked!');
    this.navCtrl.push(SchedulePage, {instId: this.navParam.id});
  }

  btnPay() {
    console.log('btnPlay Clicked!');
    this.navCtrl.push(PayPage, {instId: this.navParam.id});
  }
}
