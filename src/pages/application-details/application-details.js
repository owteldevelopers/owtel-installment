var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DocumentsPage } from "../documents/documents";
import { SchedulePage } from "../schedule/schedule";
import { PayPage } from "../pay/pay";
/**
 * Generated class for the ApplicationDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ApplicationDetailsPage = /** @class */ (function () {
    function ApplicationDetailsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.navParam = navParams.data.obj;
        console.log(this.navParam.statusCode);
    }
    ApplicationDetailsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ApplicationDetailsPage');
    };
    ApplicationDetailsPage.prototype.btnDocs = function () {
        console.log('btnDocs Clicked!');
        this.navCtrl.push(DocumentsPage, { instId: this.navParam.id, status: this.navParam.statusCode });
    };
    ApplicationDetailsPage.prototype.btnSched = function () {
        console.log('btnSched Clicked!');
        this.navCtrl.push(SchedulePage, { instId: this.navParam.id });
    };
    ApplicationDetailsPage.prototype.btnPay = function () {
        console.log('btnPlay Clicked!');
        this.navCtrl.push(PayPage, { instId: this.navParam.id });
    };
    ApplicationDetailsPage = __decorate([
        Component({
            selector: 'page-application-details',
            templateUrl: 'application-details.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams])
    ], ApplicationDetailsPage);
    return ApplicationDetailsPage;
}());
export { ApplicationDetailsPage };
//# sourceMappingURL=application-details.js.map