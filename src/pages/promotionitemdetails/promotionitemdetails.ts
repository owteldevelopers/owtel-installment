import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Slides  } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { LoadingController, Loading, AlertController } from "ionic-angular/index";

import { Storage } from '@ionic/storage';
import { Network } from '@ionic-native/network';

import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { Methods } from "../../globals/methods";
import { LoginPage } from "../login/login";

/**
 * Generated class for the PromotionitemdetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-promotionitemdetails',
  templateUrl: 'promotionitemdetails.html',
})
export class PromotionitemdetailsPage {
  @ViewChild(Slides) slides: Slides;
  prompt: any;
  loader: any;
  button: any;
  loading: Loading;
  promoItemDetails: any;
  product_name: any;
  promoDetails : any;


  constructor(public navCtrl: NavController
    , public loadingCtrl: LoadingController
    , public alertCtrl: AlertController
    , private authService: AuthServiceProvider
    , public methods: Methods
    , private storage: Storage
    , private network: Network
    , private translate: TranslateService
    , public navParams: NavParams) {
  }

  ionViewDidEnter() {
    this.translate.get('PROMPT').subscribe(
      translated => {
        this.prompt = translated;
      }
    );

    this.translate.get('LOADER').subscribe(
      translated => {
        this.loader = translated;
      }
    );

    this.translate.get('BUTTON').subscribe(
      translated => {
        this.button = translated;
      }
    );

    this.getPromotionDetails();
  }

  getPromotionDetails() {
    if (this.network.type != 'none') {
      this.loading = this.methods.createLoader(this.loadingCtrl, `<ion-spinner >` + this.loader.LOAD + `</ion-spinner>`);
      this.loading.present().then(() => {
        this.getPromotionItemDetailsRequest();
      });
    } else {
      this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);
    }
  }

  getPromotionItemDetailsRequest(refresher?) {

    let product_code = this.navParams.data.product_code;
    let id = this.navParams.data.id;

    console.log(id);
    console.log(product_code);

    
    this.authService.getPromotionItemDetails(product_code, id).subscribe(
      resp => {

        if (refresher != null) refresher.complete();
        status = resp.status;

        if (status == "success") {
          let details = resp.data;

          details.forEach((v, i) => {
            this.promoItemDetails = v.img_url;
            this.product_name = this.prodCapitalize(v.name);

          });

          this.authService.getPromotionItemDetails(product_code, id).subscribe(
            resp => {
              this.promoDetails = resp.data;

            }
          );

          

          console.log(details);

          this.loading.dismiss();



        } else if (status == "unauthorized") {
          this.storage.clear();
          this.navCtrl.setRoot(LoginPage);

        } else {
          this.methods.showErrorOkButton(this.alertCtrl, '', resp.message, this.button.OK);

        }
      }, error => {
        console.log(error);

        if (refresher != null) refresher.complete();
        this.loading.dismiss();


        if (error.name == 'TimeoutError') {
          this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_ERR, this.prompt.MESSAGE.NET, this.button.OK);

        } else {
          this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.GEN_ERR, this.button.OK);

        }
      }
    );

  }

  slideChanged(){
    let currentIndex = this.slides.getActiveIndex();
    /* if (currentIndex == 3) {
      this.slides.stopAutoplay();
    } */
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    if (this.network.type != 'none') {
      this.getPromotionItemDetailsRequest(refresher);
    } else {
      refresher.complete();
      this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);

    }
  }

  prodCapitalize(product_name){
    var lower = String(product_name).toLowerCase();
    return lower.replace(/(^| )(\w)/g, function (x) {
      return x.toUpperCase();
    });

  }

 

}
