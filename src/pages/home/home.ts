import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Refresher, Platform } from 'ionic-angular';
import { LoadingController, Loading, AlertController } from "ionic-angular/index";

import { Storage } from '@ionic/storage';
import { Network } from '@ionic-native/network';

import { ApplicationDetailsPage } from "../application-details/application-details";
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { Methods } from "../../globals/methods";
import { LoginPage } from "../login/login";
import { ReferencePage } from "../reference/reference";
import { TranslateService } from '@ngx-translate/core';

import introJs from 'intro.js/intro.js';



@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {
    userName: String = "";
    genErr: String;

    loading: Loading;
    installment_details: Array<{ id: string, status: string, date: string, statusCode: number }>;

    prompt: any;
    loader: any;
    button: any;
    getHKID : any;

    constructor(public navCtrl: NavController
        , public navParams: NavParams
        , public loadingCtrl: LoadingController
        , public alertCtrl: AlertController
        , private authService: AuthServiceProvider
        , public methods: Methods
        , private storage: Storage
        , private network: Network
        , private translate: TranslateService) {

            

    }

    /**
     * Variable init()
     * Call getInstallments
     */

   /*  ngAfterViewInit(): void {
        this.intro();
    }

    intro() {
        let intro = introJs.introJs();
        intro.setOptions({
            steps: [
                {
                    intro: "Hello world!"
                },
                {
                    element: '#step1',
                    intro: "This is a tooltip.",
                    position: 'bottom'

                },
                {
                    element: '#step2',
                    intro: "Ok, wasn't that fun?",
                    position: 'bottom'
                }
            ]
        });
        intro.start();
    } */
    ionViewDidLoad() {
        console.log('ionViewDidLoad HomePage');
        this.storage.get(this.authService.storageKey).then((hkid) => {
            this.getHKID = hkid;
        });

        this.translate.get('PROMPT').subscribe(
            translated => {
                this.prompt = translated;
            }
        );

        this.translate.get('LOADER').subscribe(
            translated => {
                this.loader = translated;
            }
        );

        this.translate.get('BUTTON').subscribe(
            translated => {
                this.button = translated;
            }
        );

        this.getInstallments();


    }


    /**
     * method to populate installment details
     */
    getInstallments() {
        if (this.network.type != 'none') {
            this.loading = this.methods.createLoader(this.loadingCtrl, `<ion-spinner >` + this.loader.LOAD + `</ion-spinner>`);
            
            this.loading.present().then(() => {
                this.getInstallmentsRequest();
            });
        } else {
            
            this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);
        }
    }

    getInstallmentsRequest(refresher?) {
        this.storage.get(this.authService.storageKey).then((hkid) => {
            this.authService.getInstallments(hkid).subscribe(
                resp => {
                    console.log(resp);
                    if (refresher != null) refresher.complete();
  

                    var status = resp.status;
                    if (status == "success") {
                        let details = resp.details;

                        console.log(details);

                        details.forEach((v, i) => {
                            this.userName = v.user;
                            this.installment_details = v.installments;

                        });
                    
                    /** auto-logout */
                    
                    } else if (status == "unauthorized") { 
                        this.storage.clear();
                        this.navCtrl.setRoot(LoginPage);

                    } else {
                        this.methods.showErrorOkButton(this.alertCtrl, '', resp.message, this.button.OK);

                    }

                    this.loading.dismiss();
                }, error => {
                    console.log(error);
                    if (refresher != null) refresher.complete();
                    this.loading.dismiss();

                    if (error.name == 'TimeoutError') {
                        this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_ERR, this.prompt.MESSAGE.NET, this.button.OK);

                    } else {
                        this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.GEN_ERR, this.button.OK);

                    }
                }
            );
        });
    }
    /**
     * Next Page
     *
     * @param detailsObj
     */
    buttonClick(detailsObj) {
        this.navCtrl.push(ApplicationDetailsPage, { obj: detailsObj });
    }

    //TODO change status to statuscode
    apply(type) {
        console.log(this.prompt);

        switch (type) {
            case 1:
                this.showConfirmDialog();
                //this.createNewApplication();
                //this.createReference();
                break;
            case 2:
                let msg = "";
                let status = this.installment_details[0].statusCode; // get the recent installment status
                switch (status) {
                    case 1:
                        msg = this.prompt.MESSAGE.HAS_PENDING;
                        this.methods.showErrorOkButton(this.alertCtrl, '', msg, this.button.OK);
                        break;
                    case 2:
                        msg = this.prompt.MESSAGE.HAS_APPROVED;
                        this.methods.showErrorOkButton(this.alertCtrl, '', msg, this.button.OK);
                        break;
                    case 3:
                        msg = this.prompt.MESSAGE.HAS_INST;
                        this.methods.showErrorOkButton(this.alertCtrl, '', msg, this.button.OK);
                        break;
                    default:
                        this.showConfirmDialogExist();
                }
                break;
            default:
                break;
        }
    }

    createReference() {
        // this.createNewApplication();
        this.storage.get(this.authService.storageKey).then((hkid) => {
            this.authService.getRetailCustId(hkid).subscribe(
                resp => {
                    let details = resp.data[0].RetailCustomerNo;

                    this.navCtrl.setRoot(ReferencePage, {
                        hkid: hkid,
                        custId: details
                    });

                }
            );
        });

    }

    

    showConfirmDialog() {
        let alert = this.alertCtrl.create({
            title: '',
            subTitle: this.prompt.MESSAGE.APPLY,
            buttons: [
                {
                    text: this.button.CANCEL,
                    handler: () => {
                        alert.dismiss();
                    }
                }, {
                    text: this.button.CONFIRM,
                    handler: () => {
                        this.createNewApplication();
                    }
                }
            ]
        });
        alert.present();
    }

    showConfirmDialogExist() {
        let alert = this.alertCtrl.create({
            title: '',
            subTitle: this.prompt.MESSAGE.APPLY,
            buttons: [
                {
                    text: this.button.CANCEL,
                    handler: () => {
                        alert.dismiss();
                    }
                }, {
                    text: this.button.CONFIRM,
                    handler: () => {
                        this.createNewApplication();
                    }
                }
            ]
        });
        alert.present();
    }

    showReference() {

        let alert = this.alertCtrl.create({
            title: '',
            subTitle: this.prompt.MESSAGE.SUCCESS_ADD,
            buttons: [
              {
                    text: this.button.OK,
                    handler: () => {
                        this.createReference();
                    }
                }
            ]
        });
        alert.present();
    }



    createNewApplication() {
        if (this.network.type != 'none') {
            this.loading = this.methods.createLoader(this.loadingCtrl, `<ion-spinner >` + this.loader.LOAD + `</ion-spinner>`);
            this.loading.present().then(() => {
                this.storage.get(this.authService.storageKey).then((hkid) => {
                    this.authService.createInstallment(hkid).subscribe(
                        resp => {
                            console.log(resp);

                            this.loading.dismiss();
                            var status = resp.status;

                            if (status == "success") {

                                this.showReference();
                               //this.methods.showErrorOkButton(this.alertCtrl, '', resp.message, this.button.OK);


                                let details = resp.details;
                                details.forEach((v, i) => {
                                    this.userName = v.user;
                                    this.installment_details = v.installments;

                                });

                            } else if (status == "unauthorized") { /** auto-logout */
                                this.storage.clear();
                                this.navCtrl.setRoot(LoginPage);

                            } else {
                                this.methods.showErrorOkButton(this.alertCtrl, '', resp.message, this.button.OK);

                            }
                        }, error => {
                            console.log(error);
                            this.loading.dismiss();

                            if (error.name == 'TimeoutError') {
                                this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_ERR, this.prompt.MESSAGE.NET, this.button.OK);

                            } else {
                                this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.GEN_ERR, this.button.OK);

                            }
                        }
                    );
                });
            });
        } else {
            this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);
        }
    }

    doRefresh(refresher) {
        console.log('Begin async operation', refresher);
        if (this.network.type != 'none') {
            this.getInstallmentsRequest(refresher);
        } else {
            refresher.complete();
            this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);

        }
    }

    



}
