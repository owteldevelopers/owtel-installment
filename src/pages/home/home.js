var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { LoadingController, AlertController } from "ionic-angular/index";
import { Storage } from '@ionic/storage';
import { Network } from '@ionic-native/network';
import { ApplicationDetailsPage } from "../application-details/application-details";
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { Methods } from "../../globals/methods";
import { LoginPage } from "../login/login";
import { TranslateService } from '@ngx-translate/core';
var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, navParams, loadingCtrl, alertCtrl, authService, methods, storage, network, translate) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.authService = authService;
        this.methods = methods;
        this.storage = storage;
        this.network = network;
        this.translate = translate;
        this.userName = "";
    }
    /**
     * Variable init()
     * Call getInstallments
     */
    HomePage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad HomePage');
        this.translate.get('PROMPT').subscribe(function (translated) {
            _this.prompt = translated;
        });
        this.translate.get('LOADER').subscribe(function (translated) {
            _this.loader = translated;
        });
        this.translate.get('BUTTON').subscribe(function (translated) {
            _this.button = translated;
        });
        this.getInstallments();
    };
    /**
     * method to populate installment details
     */
    HomePage.prototype.getInstallments = function () {
        var _this = this;
        if (this.network.type != 'none') {
            this.loading = this.methods.createLoader(this.loadingCtrl, "<ion-spinner >" + this.loader.LOAD + "</ion-spinner>");
            this.loading.present().then(function () {
                _this.getInstallmentsRequest();
            });
        }
        else {
            this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);
        }
    };
    HomePage.prototype.getInstallmentsRequest = function (refresher) {
        var _this = this;
        this.storage.get(this.authService.storageKey).then(function (hkid) {
            _this.authService.getInstallments(hkid).subscribe(function (resp) {
                console.log(resp);
                if (refresher != null)
                    refresher.complete();
                _this.loading.dismiss();
                var status = resp.status;
                if (status == "success") {
                    var details = resp.details;
                    details.forEach(function (v, i) {
                        _this.userName = v.user;
                        _this.installment_details = v.installments;
                    });
                }
                else if (status == "unauthorized") { /** auto-logout */
                    _this.storage.clear();
                    _this.navCtrl.setRoot(LoginPage);
                }
                else {
                    _this.methods.showErrorOkButton(_this.alertCtrl, '', resp.message, _this.button.OK);
                }
            }, function (error) {
                console.log(error);
                if (refresher != null)
                    refresher.complete();
                _this.loading.dismiss();
                if (error.name == 'TimeoutError') {
                    _this.methods.showErrorOkButton(_this.alertCtrl, _this.prompt.TITLE.NET_ERR, _this.prompt.MESSAGE.NET, _this.button.OK);
                }
                else {
                    _this.methods.showErrorOkButton(_this.alertCtrl, '', _this.prompt.MESSAGE.GEN_ERR, _this.button.OK);
                }
            });
        });
    };
    /**
     * Next Page
     *
     * @param detailsObj
     */
    HomePage.prototype.buttonClick = function (detailsObj) {
        this.navCtrl.push(ApplicationDetailsPage, { obj: detailsObj });
    };
    //TODO change status to statuscode
    HomePage.prototype.apply = function (type) {
        console.log(this.prompt);
        switch (type) {
            case 1:
                this.createNewApplication();
                break;
            case 2:
                var msg = "";
                var status_1 = this.installment_details[0].statusCode; // get the recent installment status
                switch (status_1) {
                    case 1:
                        msg = this.prompt.MESSAGE.HAS_PENDING;
                        this.methods.showErrorOkButton(this.alertCtrl, '', msg, this.button.OK);
                        break;
                    case 2:
                        msg = this.prompt.MESSAGE.HAS_APPROVED;
                        this.methods.showErrorOkButton(this.alertCtrl, '', msg, this.button.OK);
                        break;
                    case 3:
                        msg = this.prompt.MESSAGE.HAS_INST;
                        this.methods.showErrorOkButton(this.alertCtrl, '', msg, this.button.OK);
                        break;
                    default:
                        this.showConfirmDialog();
                }
                break;
            default:
                break;
        }
    };
    HomePage.prototype.showConfirmDialog = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: '',
            subTitle: this.prompt.MESSAGE.APPLY,
            buttons: [
                {
                    text: this.button.CANCEL,
                    handler: function () {
                        alert.dismiss();
                    }
                }, {
                    text: this.button.CONFIRM,
                    handler: function () {
                        _this.createNewApplication();
                    }
                }
            ]
        });
        alert.present();
    };
    HomePage.prototype.createNewApplication = function () {
        var _this = this;
        if (this.network.type != 'none') {
            this.loading = this.methods.createLoader(this.loadingCtrl, "<ion-spinner >" + this.loader.LOAD + "</ion-spinner>");
            this.loading.present().then(function () {
                _this.storage.get(_this.authService.storageKey).then(function (hkid) {
                    _this.authService.createInstallment(hkid).subscribe(function (resp) {
                        console.log(resp);
                        _this.loading.dismiss();
                        var status = resp.status;
                        if (status == "success") {
                            _this.methods.showErrorOkButton(_this.alertCtrl, '', resp.message, _this.button.OK);
                            var details = resp.details;
                            details.forEach(function (v, i) {
                                _this.userName = v.user;
                                _this.installment_details = v.installments;
                            });
                        }
                        else if (status == "unauthorized") { /** auto-logout */
                            _this.storage.clear();
                            _this.navCtrl.setRoot(LoginPage);
                        }
                        else {
                            _this.methods.showErrorOkButton(_this.alertCtrl, '', resp.message, _this.button.OK);
                        }
                    }, function (error) {
                        console.log(error);
                        _this.loading.dismiss();
                        if (error.name == 'TimeoutError') {
                            _this.methods.showErrorOkButton(_this.alertCtrl, _this.prompt.TITLE.NET_ERR, _this.prompt.MESSAGE.NET, _this.button.OK);
                        }
                        else {
                            _this.methods.showErrorOkButton(_this.alertCtrl, '', _this.prompt.MESSAGE.GEN_ERR, _this.button.OK);
                        }
                    });
                });
            });
        }
        else {
            this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);
        }
    };
    HomePage.prototype.doRefresh = function (refresher) {
        console.log('Begin async operation', refresher);
        if (this.network.type != 'none') {
            this.getInstallmentsRequest(refresher);
        }
        else {
            refresher.complete();
            this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);
        }
    };
    HomePage = __decorate([
        Component({
            selector: 'page-home',
            templateUrl: 'home.html'
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            LoadingController,
            AlertController,
            AuthServiceProvider,
            Methods,
            Storage,
            Network,
            TranslateService])
    ], HomePage);
    return HomePage;
}());
export { HomePage };
//# sourceMappingURL=home.js.map