import { DocumentsPage } from './../documents/documents';
import { Component } from '@angular/core';
import { App, NavController, NavParams, ViewController } from 'ionic-angular';

import { TranslateService } from '@ngx-translate/core';
import { LoadingController, Loading, AlertController } from "ionic-angular/index";

import { Storage } from '@ionic/storage';
import { Network } from '@ionic-native/network';

import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { Methods } from "../../globals/methods";
import { LoginPage } from "../login/login";

/**
 * Generated class for the UnlistdocPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-unlistdoc',
  templateUrl: 'unlistdoc.html',
})
export class UnlistdocPage {

  prompt: any;
  loader: any;
  button: any;
  loading: Loading;
  optdoc: any;
  columnName : any;
  navParam : any;

  constructor(
     public viewCtrl: ViewController
    ,public navCtrl: NavController
    , public loadingCtrl: LoadingController
    , public alertCtrl: AlertController
    , private authService: AuthServiceProvider
    , public methods: Methods
    , private storage: Storage
    , private network: Network
    , private translate: TranslateService
    , public appCtrl: App
    , public navParams: NavParams) {

    this.navParam = navParams.data
  }

  
  IonViewDidLoad() {
    console.log('Load');
    this.translate.get('PROMPT').subscribe(
      translated => {
        this.prompt = translated;
      }
    );

    this.translate.get('LOADER').subscribe(
      translated => {
        this.loader = translated;
      }
    );

    this.translate.get('BUTTON').subscribe(
      translated => {
        this.button = translated;
      }
    );

    this.getUnlistDocs();
  }

  ionViewWillEnter (){
    console.log('Load');
    this.translate.get('PROMPT').subscribe(
      translated => {
        this.prompt = translated;
      }
    );

    this.translate.get('LOADER').subscribe(
      translated => {
        this.loader = translated;
      }
    );

    this.translate.get('BUTTON').subscribe(
      translated => {
        this.button = translated;
      }
    );

    this.getUnlistDocs();
  }


  getUnlistDocs() {

    let instId = this.navParams.data.instId;

   
    this.storage.get(this.authService.storageKey).then((hkid) => {

      this.authService.getUnlistDocs(hkid, instId).subscribe(
        resp => {
          console.log()
          console.log(resp);

          var status = resp.status;

          if (status == "success") {
            let details = resp.details;

            if(details.length == 0){
               
              this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NO_DOC_AVAIL, this.button.OK);
                 this.viewCtrl.dismiss();
                return false;
              

            }else{
              this.optdoc = details;
            }
           
           

          } else if (status == "unauthorized") {
            this.storage.clear();
            this.navCtrl.setRoot(LoginPage);

          } else {
            this.methods.showErrorOkButton(this.alertCtrl, '', resp.message, this.button.OK);

          }
        }, error => {
          console.log(error);



          if (error.name == 'TimeoutError') {
            this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_ERR, this.prompt.MESSAGE.NET, this.button.OK);

          } else {
            this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.GEN_ERR, this.button.OK);

          }
        }
      );
    });

  }

  public getDocs(name) {
    let instId = this.navParams.data.instId;
    let status = this.navParams.data.status;
    let value = 'X';
    
    switch (name) {
      case 'HKID':
        this.columnName = 'HKIDFN';
        break;
      case 'Passport':
        this.columnName = 'PassportFN';
        break;
      case 'VISA':
        this.columnName = 'VisaFN';
        break;
      case 'Address Proof':
        this.columnName = 'APFN';
        break;
      case 'Contract Front':
        this.columnName = 'Contract1FN';
        break;
      case 'Contract Back':
        this.columnName = 'ContractLFN';
        break;
      case 'Application Form':
        this.columnName = 'AFFN';
        break;
      case 'Proof of Income':
        this.columnName = 'POI';
        break;
    }
    
    
    //console.log(instId);
    if (this.network.type != 'none') {

      this.storage.get(this.authService.storageKey).then((hkid) => {
        this.authService.getUpdateUnlistDocs(instId, this.columnName, 1, value).subscribe(
          resp => {
            console.log(resp);
            
            var status = resp.status;

            if (status == "success") {
              
             
               this.viewCtrl.dismiss();
              

              /* this.appCtrl.getRootNav().push(DocumentsPage, {
                instId: this.navParam.instId,
                status: this.navParam.status
                }).then(() => {
                  let index = 0;
                  this.navCtrl.remove(index);
              }); */

           /*    this.appCtrl.getRootNav().push(DocumentsPage, {
                instId: this.navParam.instId,
                status: this.navParam.status
              }).then(() => {
                let index = this.viewCtrl.index;
                this.navCtrl.remove(index);
              }) */

             

              
            /*   this.appCtrl.getRootNav().pop(DocumentsPage, {
                instId: this.navParam.instId,
                status: this.navParam.status
                
              }) */
              

            } else if (status == "unauthorized") { 
              this.storage.clear();
              this.navCtrl.setRoot(LoginPage);

            } else {
            
              this.loading.dismiss();

             
              this.methods.showErrorOkButton(this.alertCtrl, '', resp.message, this.button.OK);

            }
          }, error => {
            console.log(error);
           


           
            if (error.name == 'TimeoutError') {
              
              this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_ERR, this.prompt.MESSAGE.NET, this.button.OK);

            } else if ((error.status >= 400) || (error.status == 500)) {
             
              
              this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.GEN_ERR, this.button.OK);

            } else {

              
              this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_INT, this.prompt.MESSAGE.NET, this.button.OK);
             

            }
          }
        );
      });
      // });
    } else {

    
      this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);

    }
  }

  closeModal() {
  
    this.viewCtrl.dismiss();
  }

}
