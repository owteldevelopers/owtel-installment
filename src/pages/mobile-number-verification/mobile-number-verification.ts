import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { LoadingController, Loading, AlertController, Platform } from "ionic-angular/index";

import { Storage } from '@ionic/storage';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Network } from '@ionic-native/network';

import { Methods } from "../../globals/methods";
import { MobileNumberSuccessPage } from "../mobile-number-success/mobile-number-success";
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { LoginPage } from "../login/login";
import { MobileNumberAttentionPage } from "../mobile-number-attention/mobile-number-attention";

import { TranslateService } from '@ngx-translate/core';



declare var SMS: any;

@Component({
  selector: 'page-mobile-number-verification',
  templateUrl: 'mobile-number-verification.html',
})
export class MobileNumberVerificationPage {
  @ViewChild('code') code;

  public navParam;

  loading: Loading;

  prompt: any;
  loader: any;
  button: any;
  verification_code : any;

  validations_form: FormGroup;
  isClickable: boolean;
  isVerificationSent: boolean = false;
  timer: number;
  maxTime: number;
  timeDisp: String;
  otpMask: String = "OWTEL Installment verification code";
  otp: String = "";
  disabled: boolean = false;

  constructor(public navCtrl: NavController
    , public navParams: NavParams
    , public formBuilder: FormBuilder
    , private methods: Methods
    , public loadingCtrl: LoadingController
    , public alertCtrl: AlertController
    , public platform: Platform
    , public authService: AuthServiceProvider
    , private storage: Storage
    , private network: Network
    , private translate: TranslateService) {
    this.navParam = navParams.data;
    console.log(navParams)

    this.validations_form = this.formBuilder.group({
      code: new FormControl(
        '', Validators.compose([
          Validators.minLength(5),
          Validators.required
        ])
      )
    });
  }

  /**
   * Enable timer when the page is active
   */
  ionViewDidLoad() {
    console.log('ionViewDidLoad MobileNumberVerificationPage');
    this.enableTimer(this.navParam.time); //initial

    this.translate.get('PROMPT').subscribe(
      translated => {
        this.prompt = translated;
      }
    );

    this.translate.get('LOADER').subscribe(
      translated => {
        this.loader = translated;
      }
    );

    this.translate.get('BUTTON').subscribe(
      translated => {
        this.button = translated;
      }
    );
  }

  /**
   * Listener to new sms
   */
  ionViewDidEnter() {
    this.platform.ready().then((readySource) => {

      if (SMS) SMS.startWatch(() => {
        console.log('watching started');
      }, Error => {
        console.log('failed to start watching');
      });

      document.addEventListener('onSMSArrive', (e: any) => {
        var sms = e.data;
        var smsBody = sms.body;
        if (smsBody.indexOf(this.otpMask) > -1) {
          console.log("OTP-----" + smsBody.split("is ").pop());
          this.otp = smsBody.split("is ").pop().slice(0, 5);
          this.sendVerificationCode();
        }
      });
    });
  }

  /**
   * Send verification code
   */
  public sendVerificationCode() {
    if (this.network.type != 'none') {
      /** everytime user clicks the button, timer will reset */
      clearTimeout(this.timer);

      if (this.code.value != "") {
        this.otp = this.code.value;
      }

      this.loading = this.methods.createLoader(this.loadingCtrl, `<ion-spinner>` + this.loader.VERIFY + `</ion-spinner>`);
      this.loading.present().then(() => {
        this.authService.verifyMobile(this.navParam.id, this.navParam.number, this.otp).subscribe(
          resp => {
            console.log(resp);
            var status = resp.status;
            if (status == "success") {
              var title = resp.title;
              var subTitle = resp.subTitle;

              console.log(title);

              this.storage.set(this.authService.storageKey, this.navParam.id);  //Add to phone storage
              this.navCtrl.push(MobileNumberSuccessPage, {
                id: this.navParam.id
                , title: title
                , subTitle: subTitle
              });

            } else if (status == "unauthorized") {
              this.navCtrl.setRoot(LoginPage);

            } else {
              this.loading.dismiss();

              this.methods.showErrorOkButton(this.alertCtrl, '', resp.message, this.button.OK);

              this.enableTimer(resp.seconds);

            }
          }, error => {
            console.log(error);
            if (error.name == 'TimeoutError') {
              this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_ERR, this.prompt.MESSAGE.NET, this.button.OK);

            } else {
              this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.GEN_ERR, this.button.OK);
            }
          }
        )
      });
    } else {
      this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);
    }
  }

  public enableTimer(seconds) {
    /** Show resend sms label and timer */
    if (!this.isVerificationSent) {
      this.isVerificationSent = true;
    }

    /** Starts the timer */
    if (this.isVerificationSent) {
      this.isClickable = false;
      this.maxTime = seconds;
      this.formatTime(seconds);
      this.startTimer();
    }
  }

  public back() {
    this.navCtrl.pop();
  }

  startTimer() {
    this.timer = setTimeout(x => {
      console.log(this.maxTime);

      if (this.maxTime <= 0) { }

      if (this.maxTime > 0) {
        this.maxTime -= 1;
        this.formatTime(this.maxTime);
        this.startTimer();
      }

      if (this.maxTime == 1) {
        this.isClickable = true;
        this.disabled = false
      }

    }, 1000);
  }

  /**
   * Format the time to as minute:seconds
   */
  formatTime(time) {
    let minStr = "";
    let secStr = "";

    let min = time / 60;
    let sec = time % 60;

    if (min < 10) {
      minStr = "0" + Math.trunc(min);
    } else {
      minStr = Math.trunc(min).toString();
    }

    if (sec < 10) {
      secStr = "0" + Math.trunc(sec);
    } else {
      secStr = Math.trunc(sec).toString();
    }

    this.timeDisp = minStr + ":" + secStr;
  }

  /**
   * Called when Resend SMS is clicked
   */
  resendCode() {
    if (this.isClickable) {
      if (this.network.type != 'none') {
        this.isClickable = false;
        this.validations_form.reset(); //Disables the button

        this.loading = this.methods.createLoader(this.loadingCtrl, `<ion-spinner>` + this.loader.SMS + `</ion-spinner>`);
        this.loading.present().then(() => {
          this.authService.verifyID(this.navParam.id, this.navParam.number, 2).subscribe(
            resp => {
              console.log(resp);

              this.loading.dismiss();
              var status = resp.status;

              if (status == "success") {
                this.enableTimer(resp.seconds);

              } else {
                this.methods.showErrorOkButton(this.alertCtrl, '', resp.message, this.button.OK);

              }
            }, error => {
              console.log(error);
              this.loading.dismiss();

              if (error.name == 'TimeoutError') {
                this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_ERR, this.prompt.MESSAGE.NET, this.button.OK);

              } else {
                this.methods.showErrorOkButton(this.alertCtrl, '', error, this.button.OK);

              }
            })
        });
      } else {
        this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);

      }
    }
  }

  public sendVerificationCodeCall() {
    if (this.isClickable) {
      if (this.network.type != 'none') {
        this.isClickable = false;
        this.validations_form.reset(); //Disables the button
        /** everytime user clicks the button, timer will reset */


        if (this.code.value != "") {
          this.otp = this.code.value;
        }

      this.authService.verificationCode(this.navParam.number).subscribe(
          resp => {

        this.verification_code = resp;

        console.log(this.verification_code);

          let valid_num = '+852 ' + this.navParam.number;

           /*  let valid_num = '+639 358877161'; */

            console.log(this.verification_code);

            this.authService.verifyMobileCall(valid_num, this.verification_code).subscribe(
              resp => {

                this.authService.verifyID(this.navParam.id, this.navParam.number, 2).subscribe(
                  responseData => {
                    this.enableTimer(responseData.seconds);

                  });
                //this.enableTimer(resp.seconds);

              }, error => {
                console.log(error);
                if (error.name == 'TimeoutError') {
                  this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_ERR, this.prompt.MESSAGE.NET, this.button.OK);

                } else {
                  this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.GEN_ERR, this.button.OK);
                }
              }
            )

          }
        );

      } else {
        this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);
      }
  }


  }

  getVerificationCode(number){
    this.authService.verificationCode(number).subscribe(
      resp => {


        return resp;
        //resp;

      }, error => {
        console.log(error);
        if (error.name == 'TimeoutError') {
          this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_ERR, this.prompt.MESSAGE.NET, this.button.OK);

        } else {
          this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.GEN_ERR, this.button.OK);
        }
      }
    )
  }
}
