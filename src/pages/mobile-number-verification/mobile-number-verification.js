var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { LoadingController, AlertController, Platform } from "ionic-angular/index";
import { Storage } from '@ionic/storage';
import { Network } from '@ionic-native/network';
import { Methods } from "../../globals/methods";
import { MobileNumberSuccessPage } from "../mobile-number-success/mobile-number-success";
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { LoginPage } from "../login/login";
import { TranslateService } from '@ngx-translate/core';
var MobileNumberVerificationPage = /** @class */ (function () {
    function MobileNumberVerificationPage(navCtrl, navParams, formBuilder, methods, loadingCtrl, alertCtrl, platform, authService, storage, network, translate) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.methods = methods;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.platform = platform;
        this.authService = authService;
        this.storage = storage;
        this.network = network;
        this.translate = translate;
        this.isVerificationSent = false;
        this.otpMask = "OWTEL Installment verification code";
        this.otp = "";
        this.disabled = false;
        this.navParam = navParams.data;
        console.log(navParams);
        this.validations_form = this.formBuilder.group({
            code: new FormControl('', Validators.compose([
                Validators.minLength(5),
                Validators.required
            ]))
        });
    }
    /**
     * Enable timer when the page is active
     */
    MobileNumberVerificationPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad MobileNumberVerificationPage');
        this.enableTimer(this.navParam.time); //initial
        this.translate.get('PROMPT').subscribe(function (translated) {
            _this.prompt = translated;
        });
        this.translate.get('LOADER').subscribe(function (translated) {
            _this.loader = translated;
        });
        this.translate.get('BUTTON').subscribe(function (translated) {
            _this.button = translated;
        });
    };
    /**
     * Listener to new sms
     */
    MobileNumberVerificationPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.platform.ready().then(function (readySource) {
            if (SMS)
                SMS.startWatch(function () {
                    console.log('watching started');
                }, function (Error) {
                    console.log('failed to start watching');
                });
            document.addEventListener('onSMSArrive', function (e) {
                var sms = e.data;
                var smsBody = sms.body;
                if (smsBody.indexOf(_this.otpMask) > -1) {
                    console.log("OTP-----" + smsBody.split("is ").pop());
                    _this.otp = smsBody.split("is ").pop().slice(0, 5);
                    _this.sendVerificationCode();
                }
            });
        });
    };
    /**
     * Send verification code
     */
    MobileNumberVerificationPage.prototype.sendVerificationCode = function () {
        var _this = this;
        if (this.network.type != 'none') {
            /** everytime user clicks the button, timer will reset */
            clearTimeout(this.timer);
            if (this.code.value != "") {
                this.otp = this.code.value;
            }
            this.loading = this.methods.createLoader(this.loadingCtrl, "<ion-spinner>" + this.loader.VERIFY + "</ion-spinner>");
            this.loading.present().then(function () {
                _this.authService.verifyMobile(_this.navParam.id, _this.navParam.number, _this.otp).subscribe(function (resp) {
                    console.log(resp);
                    var status = resp.status;
                    if (status == "success") {
                        var title = resp.title;
                        var subTitle = resp.subTitle;
                        _this.storage.set(_this.authService.storageKey, _this.navParam.id); //Add to phone storage
                        _this.navCtrl.push(MobileNumberSuccessPage, {
                            id: _this.navParam.id,
                            title: title,
                            subTitle: subTitle
                        });
                    }
                    else if (status == "unauthorized") {
                        _this.navCtrl.setRoot(LoginPage);
                    }
                    else {
                        _this.loading.dismiss();
                        _this.methods.showErrorOkButton(_this.alertCtrl, '', resp.message, _this.button.OK);
                        _this.enableTimer(resp.seconds);
                    }
                }, function (error) {
                    console.log(error);
                    if (error.name == 'TimeoutError') {
                        _this.methods.showErrorOkButton(_this.alertCtrl, _this.prompt.TITLE.NET_ERR, _this.prompt.MESSAGE.NET, _this.button.OK);
                    }
                    else {
                        _this.methods.showErrorOkButton(_this.alertCtrl, '', _this.prompt.MESSAGE.GEN_ERR, _this.button.OK);
                    }
                });
            });
        }
        else {
            this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);
        }
    };
    MobileNumberVerificationPage.prototype.enableTimer = function (seconds) {
        /** Show resend sms label and timer */
        if (!this.isVerificationSent) {
            this.isVerificationSent = true;
        }
        /** Starts the timer */
        if (this.isVerificationSent) {
            this.isClickable = false;
            this.maxTime = seconds;
            this.formatTime(seconds);
            this.startTimer();
        }
    };
    MobileNumberVerificationPage.prototype.back = function () {
        this.navCtrl.pop();
    };
    MobileNumberVerificationPage.prototype.startTimer = function () {
        var _this = this;
        this.timer = setTimeout(function (x) {
            console.log(_this.maxTime);
            if (_this.maxTime <= 0) { }
            if (_this.maxTime > 0) {
                _this.maxTime -= 1;
                _this.formatTime(_this.maxTime);
                _this.startTimer();
            }
            if (_this.maxTime == 1) {
                _this.isClickable = true;
                _this.disabled = false;
            }
        }, 1000);
    };
    /**
     * Format the time to as minute:seconds
     */
    MobileNumberVerificationPage.prototype.formatTime = function (time) {
        var minStr = "";
        var secStr = "";
        var min = time / 60;
        var sec = time % 60;
        if (min < 10) {
            minStr = "0" + Math.trunc(min);
        }
        else {
            minStr = Math.trunc(min).toString();
        }
        if (sec < 10) {
            secStr = "0" + Math.trunc(sec);
        }
        else {
            secStr = Math.trunc(sec).toString();
        }
        this.timeDisp = minStr + ":" + secStr;
    };
    /**
     * Called when Resend SMS is clicked
     */
    MobileNumberVerificationPage.prototype.resendCode = function () {
        var _this = this;
        if (this.isClickable) {
            if (this.network.type != 'none') {
                this.isClickable = false;
                this.validations_form.reset(); //Disables the button
                this.loading = this.methods.createLoader(this.loadingCtrl, "<ion-spinner>" + this.loader.SMS + "</ion-spinner>");
                this.loading.present().then(function () {
                    _this.authService.verifyID(_this.navParam.id, _this.navParam.number, 2).subscribe(function (resp) {
                        console.log(resp);
                        _this.loading.dismiss();
                        var status = resp.status;
                        if (status == "success") {
                            _this.enableTimer(resp.seconds);
                        }
                        else {
                            _this.methods.showErrorOkButton(_this.alertCtrl, '', resp.message, _this.button.OK);
                        }
                    }, function (error) {
                        console.log(error);
                        _this.loading.dismiss();
                        if (error.name == 'TimeoutError') {
                            _this.methods.showErrorOkButton(_this.alertCtrl, _this.prompt.TITLE.NET_ERR, _this.prompt.MESSAGE.NET, _this.button.OK);
                        }
                        else {
                            _this.methods.showErrorOkButton(_this.alertCtrl, '', error, _this.button.OK);
                        }
                    });
                });
            }
            else {
                this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);
            }
        }
    };
    __decorate([
        ViewChild('code'),
        __metadata("design:type", Object)
    ], MobileNumberVerificationPage.prototype, "code", void 0);
    MobileNumberVerificationPage = __decorate([
        Component({
            selector: 'page-mobile-number-verification',
            templateUrl: 'mobile-number-verification.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            FormBuilder,
            Methods,
            LoadingController,
            AlertController,
            Platform,
            AuthServiceProvider,
            Storage,
            Network,
            TranslateService])
    ], MobileNumberVerificationPage);
    return MobileNumberVerificationPage;
}());
export { MobileNumberVerificationPage };
//# sourceMappingURL=mobile-number-verification.js.map