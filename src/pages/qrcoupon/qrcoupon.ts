import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { LoadingController, Loading, AlertController } from "ionic-angular/index";
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';
import { Network } from '@ionic-native/network';

import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { Methods } from "../../globals/methods";
import { LoginPage } from "../login/login";

/**
 * Generated class for the QrcouponPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-qrcoupon',
  templateUrl: 'qrcoupon.html',
})
export class QrcouponPage {

  prompt: any;
  loader: any;
  button: any;
  loading: Loading;
  imgQR : any;
  detailsQr : any;
  coupon_id : any;


  constructor(public navCtrl: NavController
    , public loadingCtrl: LoadingController
    , public alertCtrl: AlertController
    , private authService: AuthServiceProvider
    , public methods: Methods
    , private storage: Storage
    , private network: Network
    , private translate: TranslateService
    , public navParams: NavParams) {

    this.coupon_id = this.navParams.data.coupon_id;


  }

  ionViewDidLoad() {
    this.translate.get('PROMPT').subscribe(
      translated => {
        this.prompt = translated;
      }
    );

    this.translate.get('LOADER').subscribe(
      translated => {
        this.loader = translated;
      }
    );

    this.translate.get('BUTTON').subscribe(
      translated => {
        this.button = translated;
      }
    );

    this.getQRCode();

  }

  getFormatDate(date){
    var d = new Date(date);

    var day = (d.getDate() < 10 ? '0' : '') + d.getDate();
    // 01, 02, 03, ... 10, 11, 12
    var month = ((d.getMonth() + 1) < 10 ? '0' : '') + (d.getMonth() + 1);
    //Months are zero based
    var curr_year = d.getFullYear();

    var year =  curr_year.toString().substr(-2);

    return day + "/" + month + "/" + curr_year;

  }


  getQRCode(){

    this.loading = this.methods.createCoupon(this.loadingCtrl, `<ion-spinner >` + this.loader.LOAD + `</ion-spinner>`);
    this.loading.present().then(() => {
      this.storage.get(this.authService.storageKey).then((hkid) => {
        this.authService.getQRCode(this.coupon_id, hkid).subscribe(
          resp => {
            status = resp.status;

            if (status == "success") {
              let details = resp.data;

              console.log(details);


              this.imgQR = 'http://api.owtelstorecrm.owtel.com/' +resp.qrcode;
             /*  this.imgQR = 'http://test.apiowtelstorecrm.owtel.com/' + resp.qrcode; */

              this.authService.getQRCode(this.coupon_id, hkid).subscribe(
                resp => {
                  status = resp.status;
                  this.detailsQr = details;
                }
              )

            } else if (status == "unauthorized") {
              this.storage.clear();
              this.navCtrl.setRoot(LoginPage);

            } else {
              this.methods.showErrorOkButton(this.alertCtrl, '', resp.message, this.button.OK);

            }
          }, error => {
            console.log(error);

            if (error.name == 'TimeoutError') {
              this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_ERR, this.prompt.MESSAGE.NET, this.button.OK);

            } else {
              this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.GEN_ERR, this.button.OK);

            }
          }
        );

      });

    });

  }

}
