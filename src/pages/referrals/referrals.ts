import { Component} from '@angular/core';
import { NavController, NavParams} from 'ionic-angular';
import { LoadingController, Loading,  AlertController } from "ionic-angular/index";
import { FormControl } from '@angular/forms';

import { Storage } from '@ionic/storage';
import { Network } from '@ionic-native/network';

import 'rxjs/add/operator/debounceTime';
import { AddreferralPage } from "../addreferral/addreferral";
import { UpdatereferralPage } from "../updatereferral/updatereferral";
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { Methods } from "../../globals/methods";
import { TranslateService } from '@ngx-translate/core';



/**
 * Generated class for the ReferralsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-referrals',
  templateUrl: 'referrals.html',
})
export class ReferralsPage {

  active: number = 1;

  searchTerm: string = '';
  loading: Loading;
  searchControl: FormControl;
  items = [];
  navParam: any;
  prompt: any;
  loader: any;
  button: any;
  searching: any = false;
  details : any;
  retailCustId : any;
  phoneNo :any;
  status :any;
  initialItems: any;
  itemLength : any;
  detail_code : any;
  isDisabled: boolean = false;



  constructor(public navCtrl: NavController
    , public navParams: NavParams
    , public loadingCtrl: LoadingController
    , public alertCtrl: AlertController
    , private authService: AuthServiceProvider
    , public methods: Methods
    , private storage: Storage
    , private network: Network
    , private translate: TranslateService) {
    this.searchControl = new FormControl();
  }

  ionViewWillEnter() {
    this.translate.get('PROMPT').subscribe(
      translated => {
        this.prompt = translated;
      }
    );

    this.translate.get('LOADER').subscribe(
      translated => {
        this.loader = translated;
      }
    );

    this.translate.get('BUTTON').subscribe(
      translated => {
        this.button = translated;
      }
    );

    this.checkStatusCode();
    this.getRefInitialize();
  }



  getRefInitialize() {
    if (this.network.type != 'none') {
      this.loading = this.methods.createLoader(this.loadingCtrl, `<ion-spinner >` + this.loader.LOAD + `</ion-spinner>`);
      this.loading.present().then(() => {
        this.getReferral();
      });
    } else {
      this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);
    }
  }

  initializeItems(){
    this.items = JSON.parse(JSON.stringify(this.initialItems));
  }

  getItems(ev: any) {
    var val = ev.target.value;
    this.initializeItems();

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {

        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');

    setTimeout(() => {
      for (let i = 0; i < 10; i++) {
        this.items.push(this.items.length);
      }

      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 500);
  }

  onSearchInput() {
    this.searching = true;
  }

  referralList(){
    this.storage.get(this.authService.storageKey).then((hkid) => {
      this.authService.getReferral(hkid).subscribe(
        resp => {
          let message = resp.message;
          let data = resp.data;

          this.items = data;

        }, error => {
          console.log(error);
          this.loading.dismiss();

          if (error.name == 'TimeoutError') {
            this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_ERR, this.prompt.MESSAGE.NET, this.button.OK);

          } else if ((error.status >= 400) || (error.status == 500)) {  /** catch http request errors */
            this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.GEN_ERR, this.button.OK);

          } else {
            this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_INT, this.prompt.MESSAGE.NET, this.button.OK);

          }
        }
      );
    });
  }


  getReferral(refresher?) {
      this.storage.get(this.authService.storageKey).then((hkid) => {
        this.authService.getReferral(hkid).subscribe(
          resp => {
            let message = resp.message;
            let data = resp.data;



            this.initialItems = data
            this.items = JSON.parse(JSON.stringify(this.initialItems));
            this.itemLength = data;

            if (refresher != null) refresher.complete();

            this.loading.dismiss();
          }, error => {
            console.log(error);
            if (refresher != null) refresher.complete();
            this.loading.dismiss();

            if (error.name == 'TimeoutError') {
              this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_ERR, this.prompt.MESSAGE.NET, this.button.OK);

            } else if ((error.status >= 400) || (error.status == 500)) {  /** catch http request errors */
              this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.GEN_ERR, this.button.OK);

            } else {
              this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_INT, this.prompt.MESSAGE.NET, this.button.OK);
            }
          }
        );
      });

  }

  addReferral(){
    this.navCtrl.push(AddreferralPage);
  }

  viewReferrals(id){
    this.active = id;
    this.navCtrl.push(UpdatereferralPage,{
      id : id,
    });

  }

  getDetails(details, status){
    let getPhone = [];
    //let getStatus = [];

    details.forEach(e => {
      getPhone.push(e.PhoneNo);
      /* getStatus.push(e.status); */
    });

    let getStatusDisplay = status !== undefined ? status : 'Not Yer Apply';
    return '<p> Mobile Number : <span style = "color:#fff">' + getPhone.join(', ') + '</span></p><p> Status : <span style = "color:#fff">' + getStatusDisplay + '</span></p>';
  }


  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    if (this.network.type != 'none') {
      console.log('refresher starts');
      this.getReferral(refresher);
    } else {
      refresher.complete();
      this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);

    }

  }

  checkStatusCode(){

    this.storage.get(this.authService.storageKey).then((hkid) => {
      this.authService.getInstallments(hkid).subscribe(
        resp => {
          //console.log(resp);

          var status = resp.status;

          resp.details.forEach(v => {
            return this.detail_code = v.installments
          });

         // return this.detail_code = resp.details.installments.length;

        }, error => {
          console.log(error);


          if (error.name == 'TimeoutError') {
            this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_ERR, this.prompt.MESSAGE.NET, this.button.OK);

          } else {

            this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.GEN_ERR, this.button.OK);

          }
        }
      );
    });

  }











}
