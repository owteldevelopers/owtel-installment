import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Contacts, ContactFieldType, IContactFindOptions } from "@ionic-native/contacts";
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { Methods } from "../../globals/methods";
import { LoadingController, Loading, AlertController } from "ionic-angular/index";

import { Network } from '@ionic-native/network';

import { TranslateService } from '@ngx-translate/core';
import { ReferralsPage } from "../referrals/referrals";
import { AddreferralPage } from "../addreferral/addreferral";


/**
 * Generated class for the ContactsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-contacts',
  templateUrl: 'contacts.html',
})
export class ContactsPage {
  ourtype : ContactFieldType[] = ["displayName"];
  contactFound = [];
  groupedContacts = [];
  group : any;
  checked = [];
  loading : any;
  prompt: any;
  loader: any;
  button: any;
  result: any;
  initialItems : any;


  constructor(public navCtrl: NavController, public navParams: NavParams, public contacts: Contacts,
      public loadingCtrl: LoadingController
    , public alertCtrl: AlertController
    , private authService: AuthServiceProvider
    , public methods: Methods
    , private storage: Storage
    , private network: Network
    , private translate: TranslateService) {



  }


  ionViewWillEnter() {
    console.log('ionViewDidLoad ContactsPage');

    this.translate.get('PROMPT').subscribe(
      translated => {
        this.prompt = translated;
      }
    );

    this.translate.get('LOADER').subscribe(
      translated => {
        this.loader = translated;
      }
    );

    this.translate.get('BUTTON').subscribe(
      translated => {
        this.button = translated;
      }
    );


    this.initializeContacts();




    //console.log(this.checked);
  }

  initializeContacts(){

    var options = {
      filter: "",
      multiple: true,
      hasPhoneNumber: true
    };

    this.loading = this.methods.loadDuration(this.loadingCtrl, `<ion-spinner >` + this.loader.LOAD + `</ion-spinner>`);
    this.loading.present().then(() => {
      this.contacts.find(["*"], options).then(contcs => {
        //console.log(contcs);
        let getPush = [];
        contcs.forEach((value, index) => {
          getPush.push({ 'name': value.displayName, 'checked': false, 'phone_no' : value.phoneNumbers[0].value});
        });

        const sorted = getPush.sort((a, b) => a.name > b.name ? 1 : -1);

        const grouped = sorted.reduce((groups, contact) => {
        const letter = contact.name.charAt(0);

          groups[letter] = groups[letter] || [];
          groups[letter].push(contact);

          return groups;
        }, {});

        this.initialItems = Object.keys(grouped).map(key => ({ key, contacts: grouped[key] }));
        this.result = JSON.parse(JSON.stringify(this.initialItems));

        if (this.result){
          this.loading.dismiss();
        }

      });



    });


  }

  getPhone(phone_number){
    let removeWhiteSpace = phone_number.replace(/\s/g, '');

    return removeWhiteSpace.split(",");
  }

  initializeItems() {
    this.result = JSON.parse(JSON.stringify(this.initialItems));
  }

  getItems(ev) {

    this.initializeItems();

    var val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {

      this.result = this.result.map((item) => {
        if (item && item.contacts && item.contacts.length > 0) {
          item.contacts = item.contacts.filter(contact => {
            if (!contact.name) return false;
            return contact.name.toLowerCase().indexOf(val.toLowerCase()) > -1;
          });
        }
        return item;
      })
    }


  }

  addCheckbox(event, checkbox: String, getNum) {
    let getPhone = [];

    getNum.forEach(e => {
      getPhone.push({'phone' : e});
    });

    //console.log(getPhone);


    if (event.checked) {
      this.storage.get(this.authService.storageKey).then((hkid) => {
        this.checked.push({ 'hkid' : hkid, 'name': checkbox, 'remarks' : '', 'phone': getPhone});
      });


    } else {
      let index = this.removeCheckedFromArray(checkbox, getPhone);
      this.checked.splice(index, 1);
    }
  }

  //Removes checkbox from array when you uncheck it
  removeCheckedFromArray(checkbox: String, getPhone) {

    return this.checked.findIndex((category) => {
     // return category === checkbox;
     return category === checkbox

    })
  }



  onKeyUp(ev){
    const value = ev.target.value;

    if (value && value.trim() != '') {
      this.groupedContacts.forEach(e => {
        this.group = e.contacts.filter((item) => {
          return (item.toLowerCase().indexOf(value.toLowerCase()) > -1);
        });
      });
    }

  }

  addContacts(){
    console.log('pumasok');

    if (this.network.type != 'none') {
      this.loading = this.methods.createLoader(this.loadingCtrl, `<ion-spinner >` + this.loader.PLS_WAIT + `</ion-spinner>`);
      this.loading.present().then(() => {

        this.storage.get(this.authService.storageKey).then((hkid) => {

          let getPhoneDataArray = [];
          let getNameFirst = [];
          let getNameSecond = [];
          let getMobile = [];
          let getCheckPhone = [];
          let getValue = [];
          //let getVal = [];

          this.authService.getReferral(hkid).subscribe(
            resp => {
              resp.data.forEach(e => {
                let getPhone = e.phone;

                getNameFirst.push(e.name)


                getPhone.forEach(getDataPhone => {
                  getPhoneDataArray.push(getDataPhone.PhoneNo);
                });
              });

              this.checked.forEach(check => {
                let checkingPhones = check.phone;
                let checkingName = check.name;

                getNameSecond.push(check.name)


                checkingPhones.forEach(getData => {
                  //console.log(getData.phone);
                  getCheckPhone.push(getData.phone);
                })
              });


              getCheckPhone.forEach(phoneInput => {
                getMobile.push(getPhoneDataArray.find(k => k == phoneInput));
              });


              let first = getNameFirst.map(v => v.replace(/\s+/g, ''));
              let second = getNameSecond.map(v => v.toUpperCase());

              let getVal = getCheckPhone.filter(item => getPhoneDataArray.indexOf(item) < 0);
              let getValName = second.filter(item => first.indexOf(item) < 0);

              var mySet = new Set(getMobile).size == 1;

              if (getMobile[0] === undefined && getMobile.length == 1) {

                this.addContactInfo();
                return;
              } else if (getMobile.length > 1 && mySet == true) {
                this.addContactInfo();
                return;
              } else {
                this.alertexistInfo(getMobile, getVal, getValName);
                return;
              }


            },
          );

        });


      });
    } else {
      this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);
    }


  }

  addContactInfo(){

    this.authService.addBatch(this.checked).subscribe(
      resp => {

        let alert = this.alertCtrl.create({
          title: '',
          subTitle: this.checked.length + ' ' + this.prompt.MESSAGE.SUCCESS_REF_SMALL,
          buttons: [
            {
              text: this.button.OK,
              handler: () => {
                this.navCtrl.setRoot(ReferralsPage);
              }
            }
          ]
        });
        alert.present();


        this.loading.dismiss();

      }, error => {

        this.loading.dismiss();

        if (error.name == 'TimeoutError') {
          this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_ERR, this.prompt.MESSAGE.NET, this.button.OK);

        }

      })
  }

  addContactInfoByPhone(mobile) {
    //console.log(this.checked);

    this.authService.addBatch(this.checked).subscribe(
      resp => {

        let alert = this.alertCtrl.create({
          title: '',
          subTitle: this.checked.length + ' ' + this.prompt.MESSAGE.SUCCESS_REF_SMALL,
          buttons: [
            {
              text: this.button.OK,
              handler: () => {
                this.navCtrl.setRoot(ReferralsPage);
              }
            }
          ]
        });
        alert.present();


        this.loading.dismiss();

      }, error => {

        this.loading.dismiss();

        if (error.name == 'TimeoutError') {
          this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_ERR, this.prompt.MESSAGE.NET, this.button.OK);

        }

      })
  }


  alertexistInfo(getMobile, getVal, getValName) {
    let mobileLength = getMobile.filter(function (value) { return value !== undefined }).length;
    let mobileLengthUndefined = getMobile.filter(function (value) { return value === undefined }).length;

    let getNewData = this.checked;
    let getDataName = [];


    var i,v;

    for (i = getNewData.length - 1; i >= 0; --i) {
      let getName = getNewData[i].name.toUpperCase();


      var valName = getValName.find(x => x === getName);



      if (getName != valName) {
            getNewData.splice(i, 1);
          }

      }

    console.log(getNewData);


    this.authService.addBatch(getNewData).subscribe(
      resp => {

        let alert = this.alertCtrl.create({
          title: '',
          subTitle: mobileLengthUndefined + ' ' + this.prompt.MESSAGE.SUCCESS_REF_SMALL + '<br>' + mobileLength + ' ' + 'referrals failed',
          buttons: [
            {
              text: this.button.OK,
              handler: () => {
                this.navCtrl.setRoot(ReferralsPage);
              }
            }
          ]
        });
        alert.present();

        this.loading.dismiss();

      }, error => {

        this.loading.dismiss();

        if (error.name == 'TimeoutError') {
          this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_ERR, this.prompt.MESSAGE.NET, this.button.OK);

        }

      })


  }


  cancel(){
      let currentIndex = this.navCtrl.getActive().index;
      this.navCtrl.push(AddreferralPage).then(() => {
          this.navCtrl.remove(currentIndex);
      });
  }

}
