import { Component, ViewChild, QueryList, ViewChildren } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { LoadingController, Loading, AlertController } from "ionic-angular/index";
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Storage } from '@ionic/storage';
import { Network } from '@ionic-native/network';

import 'rxjs/add/operator/debounceTime';

import { UpdatereferralPage } from "../updatereferral/updatereferral";
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { Methods } from "../../globals/methods";
import { TranslateService } from '@ngx-translate/core';
import { ContactsPage } from "../contacts/contacts";
import { ReferralsPage } from "../referrals/referrals";

import { HomePage } from "../home/home";
/**
 * Generated class for the AddreferralPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-addreferral',
  templateUrl: 'addreferral.html',
})
export class AddreferralPage {
  contactListPage: Component;

  public anArray: any = [];
  data: boolean = false;

  @ViewChild('refName') refName;
  @ViewChild('refMobile') refMobile;
  //@ViewChild('refMobileMult') refMobileMult;
  @ViewChild('refRemarks') refRemarks;

  @ViewChildren('refMobileMult') refMobileMult: QueryList<any>;


  getCheckName: any;
  getCheckNumber: any;
  loading: Loading;
  searchControl: FormControl;
  items: any;
  navParam: any;
  prompt: any;
  loader: any;
  button: any;
  searching: any = false;
  details: any;
  retailCustId: any;
  validations_form: FormGroup;
  getMobile: any;
  getInfo: any;
  mobile: any;
  convertArrMobile: any;

  constructor(public navCtrl: NavController
    , public navParams: NavParams
    , public loadingCtrl: LoadingController
    , public alertCtrl: AlertController
    , private authService: AuthServiceProvider
    , public methods: Methods
    , private storage: Storage
    , private network: Network
    , public formBuilder: FormBuilder
    , private translate: TranslateService) {

    this.translate.get('PROMPT').subscribe(
      translated => {
        this.prompt = translated;
      }
    );

    this.translate.get('LOADER').subscribe(
      translated => {
        this.loader = translated;
      }
    );

    this.translate.get('BUTTON').subscribe(
      translated => {
        this.button = translated;
      }
    );

    this.validations_form = this.formBuilder.group({
      refName: new FormControl(
        '', Validators.compose([
          Validators.required
        ])
      )
    });


  }

  ngAfterViewInit() {
    this.refMobileMult.forEach(function (el) {
      console.log(el);
    });
  }

  addReferral() {
    let mobile_push = [];
    let mobile2 = [];

    let name = this.refName.value.trim();

    let mobile1 = this.refMobile.value;
    this.refMobileMult.forEach(function (el) {
      mobile2.push(el._value);
    });

    //let remarks = this.refRemarks.value;


    if (mobile2.length == 0) {
      this.mobile = mobile1;
      this.convertArrMobile = this.mobile;

    } else {
      mobile_push.push(mobile1, mobile2);
      this.mobile = mobile_push.join();
      this.convertArrMobile = this.mobile.split(",");
    }

    console.log(this.mobile);

    if (this.network.type != 'none') {
      this.loading = this.methods.createLoader(this.loadingCtrl, `<ion-spinner >` + this.loader.PLS_WAIT + `</ion-spinner>`);
      this.loading.present().then(() => {


        this.storage.get(this.authService.storageKey).then((hkid) => {

          let getPhoneDataArray = [];
          let getMobile = [];


          this.authService.getReferral(hkid).subscribe(
            resp => {
              resp.data.forEach(e => {
                let getPhone = e.phone;
                getPhone.forEach(getDataPhone => {
                  getPhoneDataArray.push(getDataPhone.PhoneNo);
                });
              });

              console.log(this.convertArrMobile);

              if (mobile2.length == 0) {
                getMobile.push(getPhoneDataArray.find(k => k == this.convertArrMobile));;
              } else {
                this.convertArrMobile.forEach(phoneInput => {
                  getMobile.push(getPhoneDataArray.find(k => k == phoneInput));
                });

              }

              if (name == '') {
                this.methods.showErrorOkButton(this.alertCtrl, '', 'Name required', this.button.OK);
                this.loading.dismiss();
                return
              } else if (mobile1 == '') {
                this.methods.showErrorOkButton(this.alertCtrl, '', 'Phone number required', this.button.OK);
                this.loading.dismiss();
                return
              } else if (mobile1.length < 8) {
                this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NUM_LENGTH, this.button.OK);
                this.loading.dismiss();
                return
              }


              var mySet = new Set(getMobile).size == 1;

              if (getMobile.indexOf(undefined) > -1) {
                this.addInfo(hkid, name, this.mobile);
                return;
              } else {
                this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.EXIST_CUST, this.button.OK);
                this.loading.dismiss();
                return;
              }



            },
          );

        });
      });
    } else {
      this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);
    }
  }

  allEqual(arr) {
    return new Set(arr).size == 1;
  }


  addInfo(hkid, name, mobile) {
    this.authService.addReferral(hkid, name, mobile).subscribe(
      resp => {
        console.log(resp);
        this.showConfirm(mobile);


      }, error => {
        console.log(error);
        this.loading.dismiss();

        if (error.name == 'TimeoutError') {
          this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_ERR, this.prompt.MESSAGE.NET, this.button.OK);

        }

      })
  }

  alertexistInfo() {
    let alert = this.alertCtrl.create({
      title: '',
      subTitle: 'Already an OWTEL customer',
      buttons: [
        {
          text: this.button.OK,
          handler: () => {
            this.loading.dismiss();
          }
        }
      ]
    });
    alert.present();
  }

  showConfirm(mobile) {

    let alert = this.alertCtrl.create({
      title: '',
      subTitle: this.prompt.MESSAGE.SUCCESS_REF,
      buttons: [
        {
          text: this.button.OK,
          handler: () => {
            this.navCtrl.setRoot(ReferralsPage);
          }
        }
      ]
    });
    alert.present();

  }



  getContacts() {

    this.navCtrl.push(ContactsPage);
  }

  Add() {
    this.refMobileMult.forEach(function (el) {
      console.log(el);
    });

    this.anArray.push({ 'value': '' });
  }


  Delete(index) {
    this.anArray.splice(index, 1);
  }


  Remove(index) {
    this.anArray.splice(index, 1);
  }

  onKeyupName(e: any) {
    let elementChecker: string;
    elementChecker = e.target.value;
    let getCheck = elementChecker.slice(-1);
    let convertToString = getCheck.toString();
    console.log(convertToString);
    if ((convertToString.charCodeAt(0) >= 33 && convertToString.charCodeAt(0) <= 47) || (convertToString.charCodeAt(0) >= 58 && convertToString.charCodeAt(0) <= 64) || (convertToString.charCodeAt(0) >= 91 && convertToString.charCodeAt(0) <= 96) || (convertToString.charCodeAt(0) >= 123 && convertToString.charCodeAt(0) <= 126) || (convertToString.charCodeAt(0) >= 128 && convertToString.charCodeAt(0) <= 253)) {
      this.getCheckName = elementChecker.slice(0, -1);
    }
  }

  onKeyupNumber(e: any) {
    let elementChecker: string;
    elementChecker = e.target.value;
    let getCheck = elementChecker.slice(-1);
    let convertToString = getCheck.toString();

    if ((convertToString.charCodeAt(0) >= 33 && convertToString.charCodeAt(0) <= 47) || (convertToString.charCodeAt(0) >= 58 && convertToString.charCodeAt(0) <= 64) || (convertToString.charCodeAt(0) >= 91 && convertToString.charCodeAt(0) <= 96) || (convertToString.charCodeAt(0) >= 123 && convertToString.charCodeAt(0) <= 126) || (convertToString.charCodeAt(0) >= 128 && convertToString.charCodeAt(0) <= 253)) {
      this.getCheckNumber = elementChecker.slice(0, -1);
    }
  }




}
