import { PromotionitemsPage } from './../promotionitems/promotionitems';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { LoadingController, Loading, AlertController } from "ionic-angular/index";

import { Storage } from '@ionic/storage';
import { Network } from '@ionic-native/network';

import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { Methods } from "../../globals/methods";
import { LoginPage } from "../login/login";




/**
 * Generated class for the PromotionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-promotion',
  templateUrl: 'promotion.html',
})
export class PromotionPage {

  prompt: any;
  loader: any;
  button: any;
  loading: Loading;
  promotionDetails : any;
  featuresImage : any;
  active: number = 1;

  constructor(public navCtrl: NavController
    , public loadingCtrl: LoadingController
    , public alertCtrl: AlertController
    , private authService: AuthServiceProvider
    , public methods: Methods
    , private storage: Storage
    , private network: Network
    , private translate: TranslateService
    , public navParams: NavParams) {
  }



  ionViewWillEnter() {
    console.log('ionViewDidLoad PromotionPage');
    this.translate.get('PROMPT').subscribe(
      translated => {
        this.prompt = translated;
      }
    );

    this.translate.get('LOADER').subscribe(
      translated => {
        this.loader = translated;
      }
    );

    this.translate.get('BUTTON').subscribe(
      translated => {
        this.button = translated;
      }
    );

    this.getPromotion();

  }

  getPromotion() {
    if (this.network.type != 'none') {
      this.loading = this.methods.createLoader(this.loadingCtrl, `<ion-spinner >` + this.loader.LOAD + `</ion-spinner>`);
      this.loading.present().then(() => {
        this.getPromotionRequest();
      });
    } else {
      this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);
    }
  }

  getPromotionRequest(refresher?){
      this.authService.getPromotion().subscribe(
        resp => {

          if (refresher != null) refresher.complete();

          var status = resp.status;

          if (status == "success") {
            let details = resp.data;

            
            console.log(details);
            this.promotionDetails = details;

          } else if (status == "unauthorized") {
            this.storage.clear();
            this.navCtrl.setRoot(LoginPage);

          } else {
            this.methods.showErrorOkButton(this.alertCtrl, '', resp.message, this.button.OK);

          }

         this.loading.dismiss();
        }, error => {
         
          if (refresher != null) refresher.complete();
          this.loading.dismiss();

          if (error.name == 'TimeoutError') {
            this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_ERR, this.prompt.MESSAGE.NET, this.button.OK);

          } else {
            this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.GEN_ERR, this.button.OK);

          }
        }
      );
  }

  getDiscount(amount){
   
    var concat_string = amount.split("$").pop();
    var get_discount = Math.round(parseInt(concat_string.replace(',','')) - (parseInt(concat_string.replace(',','')) * 0.2));
    
   return 'HK$' + get_discount.toString();

   
  }


  getPromotionItems(id){
    this.active = id;

    this.navCtrl.push(PromotionitemsPage, {
      category_id: id
    });
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    if (this.network.type != 'none') {
      this.getPromotionRequest(refresher);
    } else {
      refresher.complete();
      this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);

    }
  }

}
