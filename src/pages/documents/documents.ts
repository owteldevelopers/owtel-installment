import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, normalizeURL, ModalController, ModalOptions } from 'ionic-angular';
import { ActionSheetController, AlertController, LoadingController, Loading } from 'ionic-angular/index';

import { Storage } from '@ionic/storage';
import { ImageViewerController } from 'ionic-img-viewer';

import { Camera, CameraOptions } from '@ionic-native/camera';
import { Network } from '@ionic-native/network';
import { Device } from '@ionic-native/device';

import { Methods } from "../../globals/methods";
import * as $ from 'jquery';

import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { LoginPage } from "../login/login";
import { TranslateService } from '@ngx-translate/core';
import { CacheService } from "ionic-cache";
import { ReferencePage } from "../reference/reference";
import { UnlistdocPage } from "../unlistdoc/unlistdoc";




@Component({
  selector: 'page-documents',
  templateUrl: 'documents.html',
})
export class DocumentsPage {
  TAG: String = "DocumentsPage";
  htmlVariable: String = "success";
  @ViewChild('promptId') promptId;

  navParam: any;
  showPrompt: any;
  getData: any;
  loading: Loading;
  gallery: Array<{ id: number, name: string, src: string, isEmpty: boolean, isIcon: boolean }>;
  uploadGallery: Array<{ id: number, image: string }>;
  showButton: Array<{ src: string, isSuccess: boolean }>;
  upload_id: string;
  public imageCacheKey: string = 'image-cache-key';

  button: any;
  loader: any;
  prompt: any;
  list: any;

  doc1: string;
  doc2: string;
  doc3: string;
  doc4: string;
  doc5: string;
  doc6: string;
  doc7: string;
  doc8: string;

  isShowing_1: boolean = false;
  isShowing_2: boolean = false;
  isShowing_3: boolean = false;
  isShowing_4: boolean = false;
  isShowing_5: boolean = false;
  isShowing_6: boolean = false;
  isShowing_7: boolean = false;
  isShowing_8: boolean = false;

  getLocal : any;
  getAPI: any;
  setInterval : any;
  getHKID: any;
  getInstId : any;
  getNavParams : any;
  status : any;


  constructor(public navCtrl: NavController
    , public navParams: NavParams
    , private device: Device
    , public alertCtrl: AlertController
    , public cache: CacheService
    , public loadingCtrl: LoadingController
    , public actionSheetCtrl: ActionSheetController
    , private camera: Camera
    , private methods: Methods
    , private authService: AuthServiceProvider
    , private storage: Storage
    , private network: Network
    , public modalCtrl: ModalController
    , private translate: TranslateService) {
    this.navParam = navParams.data;
    
    this.status = this.navParam.status; 
    console.log(this.navParam.status);
  }

  /**
   * Variable init()
   * Call getDocuments method
   */
  ionViewDidLoad() {
    console.log('ionViewDidLoad DocumentsPage');
    console.log(this.navParam.status);

    this.getInstId = this.navParam.instId

    console.log(this.getHKID);
    console.log(this.getInstId);

    this.translate.get('PROMPT').subscribe(
      translated => {
        this.prompt = translated;
      }
    );

    this.translate.get('LOADER').subscribe(
      translated => {
        this.loader = translated;
      }
    );

    this.translate.get('BUTTON').subscribe(
      translated => {
        this.button = translated;
      }
    );

    this.translate.get('LIST').subscribe(
      translated => {
        this.doc1 = translated.HKID;
        this.doc2 = translated.PASSPORT;
        this.doc3 = translated.VISA;
        this.doc4 = translated.APPL_FORM;
        this.doc5 = translated.CONT_FRONT;
        this.doc6 = translated.CONT_BACK;
        this.doc7 = translated.ADDR_PROOF;
        this.doc8 = translated.PROOF_INC;

      }
    );

    this.getDocuments();
    this.autoRefresh();
  }
  
  
  
  ionViewWillLeave() {
    if (this.setInterval) {
      clearTimeout(this.setInterval);
    }
  }

  autoRefresh() {
    this.setInterval = setInterval(() => {
      this.storage.get(this.authService.storageKey).then((hkid) => {
        this.authService.getInstallmentDetails(hkid, this.navParam.instId, this.authService.reqType1).subscribe(

          resp => {
            var status = resp.status;
            var details_proof = resp.details;
            var getLocalData = this.gallery;
            
            if (this.isArrayEqual(details_proof, getLocalData) == false) {
              this.getDocuments();
            }else{
            
            }

            this.loading.dismiss();
          }
        );
      });
    }, 10000);
  }

  isArrayEqual(obj1, obj2)
  {
    if (obj1.length != obj2.length || obj2.length == 0)
    return false;

    var arr1 = [];
    var arr2 = [];

    obj1.forEach(e => {
      arr1.push(e.name);
    });

    obj2.forEach(f => {
      arr2.push(f.name);
    });

    for (let i = 0; i < arr1.length; i++) {
      if (arr2.indexOf(arr1[i]) == -1) {
        return false;
      }
    }

    return true;
  }

  /**
   * Get customer's documents
   */
  getDocuments() {
    this.loading = this.loadingCtrl.create({
      content: '<ion-spinner >' + this.loader.LOAD + '</ion-spinner>'
    });

    if (this.network.type != 'none') {
      this.loading.present().then(() => {
        this.storage.get(this.authService.storageKey).then((hkid) => {
          this.authService.getInstallmentDetails(hkid, this.navParam.instId, this.authService.reqType1).subscribe(

            resp => {

              console.log(resp);
              var status = resp.status;
              var details_proof = resp.details;

              if (status == "success") {

                this.gallery = resp.details;



              } else if (status == "unauthorized") { /** auto-logout */
                this.storage.clear();
                this.navCtrl.setRoot(LoginPage);

              } else {
                this.methods.showErrorOkButton(this.alertCtrl, '', resp.message, this.button.OK);

              }
              this.loading.dismiss();
            }, error => {
              console.log(error);
              this.loading.dismiss();

              if (error.name == 'TimeoutError') {
                this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_ERR, this.prompt.MESSAGE.NET, this.button.OK);

              } else if ((error.status >= 400) || (error.status == 500)) {  /** catch http request errors */
                this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.GEN_ERR, this.button.OK);

              } else {
                this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_INT, this.prompt.MESSAGE.NET, this.button.OK);

              }
            }
          );
        });
      });
    } else {

      this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);
    }
  }

  /**
   * Show action sheet
   * @param docID
     */
  showDialog(docID) {
    if (this.status == 1) {
      let actionSheet = this.actionSheetCtrl.create({
        title: this.prompt.TITLE.CHOOSER,
        buttons: [
          {
            icon: 'camera',
            text: this.button.CAMERA,
            handler: () => {
              console.log('Show Camera clicked');
              console.log(docID);
              this.takePhoto(docID);
            }
          },
          {
            icon: 'images',
            text: this.button.GALLERY,
            handler: () => {
              console.log('Open gallery clicked');
              this.openGallery(docID);
            }
          }
        ]
      });

      actionSheet.present();
    }
  }

  /**
   * Use device camera
   *
   * @param docID: document ID
   */
  private takePhoto(docID) {
    console.log('Load Image clicked!');

    const options: CameraOptions = {
      destinationType: this.camera.DestinationType.DATA_URL
    }

    this.camera.getPicture(options).then((imageData) => {
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.gallery.forEach((v, i) => {
        if (v.id == docID) {
          v.src = base64Image;
          v.isEmpty = false;


          let upload = [{
            id: docID,
            name: v.name,
            image: base64Image
          }];
          this.uploadSingle(upload);

          return;
        }
      });
    }, (err) => {

      this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.CAMERA, err + "", this.button.OK);
    });

  }

  /**
   * Open device storage
   *
   * @param docID: document ID
     */
  private openGallery(docID): void {
    const options: CameraOptions = {
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.DATA_URL,
      //quality: 50,
      encodingType: this.camera.EncodingType.JPEG,
      /* targetWidth: 500,
      targetHeight: 500 */

      quality: 100,
      /* targetWidth: 1000,
      targetHeight: 1000, */
    }

    this.camera.getPicture(options).then((imageData) => {
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.gallery.forEach((v, i) => {
        if (v.id == docID) {
          v.src = base64Image;
          v.isEmpty = false;

          let upload = [{
            id: docID,
            name: v.name,
            image: base64Image
          }];
          this.uploadSingle(upload);

          return;
        }
      });
    }, (err) => {
      console.log(err)
      this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.GALLERY, err + "", this.button.OK);
    });
  }

  /**
   * Send image in base64encoded string
   */
  public upload() {

    /**
     * upload only new images
     *
     * @type {Array}
     */
    this.uploadGallery = [];
    this.gallery.forEach((v, i) => {
      var substring = "base64,"; //see if is new image by substr data:image/jpeg;base64,
      if (v.src.indexOf(substring) !== -1) {
        let upload = {
          id: v.id,
          name: v.name,
          image: v.src
        }

        console.log(this.TAG + " " + v.src);
        this.uploadGallery.push(upload);
      }
    });

    if (this.uploadGallery.length > 0) {
      if (this.network.type != 'none') {
        this.loading = this.methods.createLoader(this.loadingCtrl, `<ion-spinner >` + this.loader.UPLOAD + `</ion-spinner>`);
        this.loading.present().then(() => {

          this.storage.get(this.authService.storageKey).then((hkid) => {
            this.authService.uploadDocuments(hkid, this.navParam.instId, this.uploadGallery).subscribe(
              resp => {
                console.log(resp);

                var status = resp.status;

                if (status == "success") {
                  this.loading.dismiss();
                  this.showConfirmDialog(resp.message);

                } else if (status == "unauthorized") { /** auto-logout */
                  this.storage.clear();
                  this.navCtrl.setRoot(LoginPage);

                } else {
                  this.loading.dismiss();
                  this.methods.showErrorOkButton(this.alertCtrl, '', resp.message, this.button.OK);

                }
              }, error => {
                console.log(error);

                this.loading.dismiss();
                if (error.name == 'TimeoutError') {
                  this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_ERR, this.prompt.MESSAGE.NET, this.button.OK);

                } else if ((error.status >= 400) || (error.status == 500)) {  /** catch http request errors */
                  this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.GEN_ERR, this.button.OK);

                } else {
                  /** added to prevent error response status:0 url: null*/
                  this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_INT, this.prompt.MESSAGE.NET, this.button.OK);

                }
              }
            );
          });
        });
      } else {
        this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);

      }
    } else {
      this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NO_IMAGE, this.button.OK);

    }
  }


  public uploadSingle(upload) {
    console.log("ImageID: " + upload[0].id);

    this.upload_id = upload[0].id;

    if (this.network.type != 'none') {
      this.isShowing(upload[0].id, true);

      $('.getFrontImage_' + upload[0].id).remove();
      $('.getPrompt_' + upload[0].id).hide();

      // this.loading = this.methods.createLoader(this.loadingCtrl, `<ion-spinner >` + this.loader.UPLOAD + `</ion-spinner>`);
      // this.loading.present().then(() => {

      this.storage.get(this.authService.storageKey).then((hkid) => {
        this.authService.uploadDocuments(hkid, this.navParam.instId, upload).subscribe(
          resp => {
            console.log(resp);
            console.log(upload);
            this.isShowing(upload[0].id, false);

            var status = resp.status;

            if (status == "success") {

              this.loading.dismiss();
              this.methods.getUploadSuccess(upload[0].id);


            } else if (status == "unauthorized") { /** auto-logout */
              this.storage.clear();
              this.navCtrl.setRoot(LoginPage);

            } else {
              // this.loading.dismiss();
              this.loading.dismiss();

              this.methods.getUploadFailed(upload[0].id);
              //this.methods.showErrorOkButton(this.alertCtrl, '', resp.message, this.button.OK);

            }
          }, error => {
            console.log(error);
            this.isShowing(upload[0].id, false);


            // this.loading.dismiss();

            if (error.name == 'TimeoutError') {
              this.methods.getUploadFailed(upload[0].id);
              this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_ERR, this.prompt.MESSAGE.NET, this.button.OK);

            } else if ((error.status >= 400) || (error.status == 500)) {
              this.methods.getUploadFailed(upload[0].id);
              /** catch http request errors */
              this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.GEN_ERR, this.button.OK);

            } else {

              /** added to prevent error response status:0 url: null*/
              this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_INT, this.prompt.MESSAGE.NET, this.button.OK);
              this.methods.getUploadFailed(upload[0].id);

            }
          }
        );
      });
      // });
    } else {

      this.methods.getUploadFailed(upload[0].id);
      this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);

    }
  }


  /**
   * return to previous page after document upload
   *
   * @param message
   */
  showConfirmDialog(message) {
    let alert = this.alertCtrl.create({
      title: '',
      subTitle: message,
      buttons: [
        {
          text: this.button.OK,
          handler: () => {
            this.navCtrl.pop();
          }
        }
      ]
    });
    alert.present();
  }

  isShowing(imageID, value) {
    this["isShowing_" + imageID] = value;
  }

  getShow(imageID) {
    return this["isShowing_" + imageID];

  }

  /*  getImageSrc(imgSrc, imageId){
     var getData = "";
     if (imgSrc.includes("Owtel-Shop-no-image.jpg")) {
       getData +=  $('.getFrontImage_' + imageId).remove();
     } else if (imgSrc.includes("default.png")) {
       getData += $('.getFrontImage_' + imageId).remove();
     } else  {
       getData += 'assets/icon/cloud-upload.png';
     }
     return getData;
   } */

  /* Initialize image icon if uploaded */
  getImageSrc(imgSrc, imageId, isEmpty) {
    var getData = "";

    if (imgSrc.includes("Owtel-Shop-no-image.jpg")) {
      getData += $('.getFrontImage_' + imageId).remove();
    } else if (imgSrc.includes("default.png")) {
      getData += $('.getFrontImage_' + imageId).remove();
    } else if ($('.getFrontImage_' + imageId).find('img').attr('src') == "assets/icon/red.png" || this.network.type == "none") {
      getData += $('.getFrontImage_' + this.upload_id).empty().append('<img name="" src="assets/icon/red.png" class = "prompt-icon-failed"/>');
    } else {
      if ($('ion-spinner').length == 2) {
        getData += $('.getFrontImage_' + this.upload_id).hide();
      } else {
        getData += $('.getFrontImage_' + this.upload_id).show();
        getData += $('.getFrontImage_' + imageId).empty().append('<img name="" src="assets/icon/cloud-upload.png" class = "prompt-icon" />');

      }

    }

    return getData;
  }

  createReference(){
    this.navCtrl.push(ReferencePage, {
      instId: this.navParam.instId
    });
  }


  viewUnlistDocs() {

    let myModal = this.modalCtrl.create(UnlistdocPage, {
      instId: this.navParam.instId,
      status: this.navParam.status
      
    })

    myModal.onDidDismiss(() => this.getDocuments());



    myModal.present();
   
  }



  
}
