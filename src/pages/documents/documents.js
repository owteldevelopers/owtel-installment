var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ActionSheetController, AlertController, LoadingController } from 'ionic-angular/index';
import { Storage } from '@ionic/storage';
import { Camera } from '@ionic-native/camera';
import { Network } from '@ionic-native/network';
import { Methods } from "../../globals/methods";
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { LoginPage } from "../login/login";
import { TranslateService } from '@ngx-translate/core';
var DocumentsPage = /** @class */ (function () {
    function DocumentsPage(navCtrl, navParams, alertCtrl, loadingCtrl, actionSheetCtrl, camera, methods, authService, storage, network, translate) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.camera = camera;
        this.methods = methods;
        this.authService = authService;
        this.storage = storage;
        this.network = network;
        this.translate = translate;
        this.TAG = "DocumentsPage";
        this.htmlVariable = "success";
        this.isShowing_1 = false;
        this.isShowing_2 = false;
        this.isShowing_3 = false;
        this.isShowing_4 = false;
        this.isShowing_5 = false;
        this.isShowing_6 = false;
        this.isShowing_7 = false;
        this.isShowing_8 = false;
        this.navParam = navParams.data;
        console.log(this.navParam.status);
    }
    /**
     * Variable init()
     * Call getDocuments method
     */
    DocumentsPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad DocumentsPage');
        this.translate.get('PROMPT').subscribe(function (translated) {
            _this.prompt = translated;
        });
        this.translate.get('LOADER').subscribe(function (translated) {
            _this.loader = translated;
        });
        this.translate.get('BUTTON').subscribe(function (translated) {
            _this.button = translated;
        });
        this.translate.get('LIST').subscribe(function (translated) {
            _this.doc1 = translated.HKID;
            _this.doc2 = translated.PASSPORT;
            _this.doc3 = translated.VISA;
            _this.doc4 = translated.APPL_FORM;
            _this.doc5 = translated.CONT_FRONT;
            _this.doc6 = translated.CONT_BACK;
            _this.doc7 = translated.ADDR_PROOF;
            _this.doc8 = translated.PROOF_INC;
            _this.gallery = [
                { id: 1, name: _this.doc1, src: "assets/imgs/default.png", isEmpty: true },
                { id: 2, name: _this.doc2, src: "assets/imgs/default.png", isEmpty: true },
                { id: 3, name: _this.doc3, src: "assets/imgs/default.png", isEmpty: true },
                { id: 4, name: _this.doc4, src: "assets/imgs/default.png", isEmpty: true },
                { id: 5, name: _this.doc5, src: "assets/imgs/default.png", isEmpty: true },
                { id: 6, name: _this.doc6, src: "assets/imgs/default.png", isEmpty: true },
                { id: 7, name: _this.doc7, src: "assets/imgs/default.png", isEmpty: true },
                { id: 8, name: _this.doc8, src: "assets/imgs/default.png", isEmpty: true }
            ];
        });
        this.getDocuments();
    };
    /**
     * Get customer's documents
     */
    DocumentsPage.prototype.getDocuments = function () {
        var _this = this;
        this.loading = this.loadingCtrl.create({
            content: '<ion-spinner >' + this.loader.LOAD + '</ion-spinner>'
        });
        if (this.network.type != 'none') {
            this.loading.present().then(function () {
                _this.storage.get(_this.authService.storageKey).then(function (hkid) {
                    _this.authService.getInstallmentDetails(hkid, _this.navParam.instId, _this.authService.reqType1).subscribe(function (resp) {
                        console.log(resp);
                        var status = resp.status;
                        if (status == "success") {
                            if (resp.details.length > 0) {
                                _this.gallery = resp.details;
                            }
                        }
                        else if (status == "unauthorized") { /** auto-logout */
                            _this.storage.clear();
                            _this.navCtrl.setRoot(LoginPage);
                        }
                        else {
                            _this.methods.showErrorOkButton(_this.alertCtrl, '', resp.message, _this.button.OK);
                        }
                        _this.loading.dismiss();
                    }, function (error) {
                        console.log(error);
                        _this.loading.dismiss();
                        if (error.name == 'TimeoutError') {
                            _this.methods.showErrorOkButton(_this.alertCtrl, _this.prompt.TITLE.NET_ERR, _this.prompt.MESSAGE.NET, _this.button.OK);
                        }
                        else if ((error.status >= 400) || (error.status == 500)) { /** catch http request errors */
                            _this.methods.showErrorOkButton(_this.alertCtrl, '', _this.prompt.MESSAGE.GEN_ERR, _this.button.OK);
                        }
                        else {
                            _this.methods.showErrorOkButton(_this.alertCtrl, _this.prompt.TITLE.NET_INT, _this.prompt.MESSAGE.NET, _this.button.OK);
                        }
                    });
                });
            });
        }
        else {
            this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);
        }
    };
    /**
     * Show action sheet
     * @param docID
       */
    DocumentsPage.prototype.showDialog = function (docID) {
        var _this = this;
        if (this.navParam.status == 1) {
            var actionSheet = this.actionSheetCtrl.create({
                title: this.prompt.TITLE.CHOOSER,
                buttons: [
                    {
                        icon: 'camera',
                        text: this.button.CAMERA,
                        handler: function () {
                            console.log('Show Camera clicked');
                            _this.takePhoto(docID);
                        }
                    },
                    {
                        icon: 'images',
                        text: this.button.GALLERY,
                        handler: function () {
                            console.log('Open gallery clicked');
                            _this.openGallery(docID);
                        }
                    }
                ]
            });
            actionSheet.present();
        }
    };
    /**
     * Use device camera
     *
     * @param docID: document ID
     */
    DocumentsPage.prototype.takePhoto = function (docID) {
        var _this = this;
        console.log('Load Image clicked!');
        var options = {
            destinationType: this.camera.DestinationType.DATA_URL
        };
        this.camera.getPicture(options).then(function (imageData) {
            var base64Image = 'data:image/jpeg;base64,' + imageData;
            _this.gallery.forEach(function (v, i) {
                if (v.id == docID) {
                    v.src = base64Image;
                    v.isEmpty = false;
                    var upload = [{
                            id: docID,
                            name: v.name,
                            image: base64Image
                        }];
                    _this.uploadSingle(upload);
                    return;
                }
            });
        }, function (err) {
            _this.methods.showErrorOkButton(_this.alertCtrl, _this.prompt.TITLE.CAMERA, err + "", _this.button.OK);
        });
    };
    /**
     * Open device storage
     *
     * @param docID: document ID
       */
    DocumentsPage.prototype.openGallery = function (docID) {
        var _this = this;
        var options = {
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            destinationType: this.camera.DestinationType.DATA_URL,
            quality: 50,
            encodingType: this.camera.EncodingType.JPEG,
            targetWidth: 500,
            targetHeight: 500
        };
        this.camera.getPicture(options).then(function (imageData) {
            var base64Image = 'data:image/jpeg;base64,' + imageData;
            _this.gallery.forEach(function (v, i) {
                if (v.id == docID) {
                    v.src = base64Image;
                    v.isEmpty = false;
                    var upload = [{
                            id: docID,
                            name: v.name,
                            image: base64Image
                        }];
                    _this.uploadSingle(upload);
                    return;
                }
            });
        }, function (err) {
            console.log(err);
            _this.methods.showErrorOkButton(_this.alertCtrl, _this.prompt.TITLE.GALLERY, err + "", _this.button.OK);
        });
    };
    /**
     * Send image in base64encoded string
     */
    DocumentsPage.prototype.upload = function () {
        var _this = this;
        /**
         * upload only new images
         *
         * @type {Array}
         */
        this.uploadGallery = [];
        this.gallery.forEach(function (v, i) {
            var substring = "base64,"; //see if is new image by substr data:image/jpeg;base64,
            if (v.src.indexOf(substring) !== -1) {
                var upload = {
                    id: v.id,
                    name: v.name,
                    image: v.src
                };
                console.log(_this.TAG + " " + v.src);
                _this.uploadGallery.push(upload);
            }
        });
        if (this.uploadGallery.length > 0) {
            if (this.network.type != 'none') {
                this.loading = this.methods.createLoader(this.loadingCtrl, "<ion-spinner >" + this.loader.UPLOAD + "</ion-spinner>");
                this.loading.present().then(function () {
                    _this.storage.get(_this.authService.storageKey).then(function (hkid) {
                        _this.authService.uploadDocuments(hkid, _this.navParam.instId, _this.uploadGallery).subscribe(function (resp) {
                            console.log(resp);
                            var status = resp.status;
                            if (status == "success") {
                                _this.loading.dismiss();
                                _this.showConfirmDialog(resp.message);
                            }
                            else if (status == "unauthorized") { /** auto-logout */
                                _this.storage.clear();
                                _this.navCtrl.setRoot(LoginPage);
                            }
                            else {
                                _this.loading.dismiss();
                                _this.methods.showErrorOkButton(_this.alertCtrl, '', resp.message, _this.button.OK);
                            }
                        }, function (error) {
                            console.log(error);
                            _this.loading.dismiss();
                            if (error.name == 'TimeoutError') {
                                _this.methods.showErrorOkButton(_this.alertCtrl, _this.prompt.TITLE.NET_ERR, _this.prompt.MESSAGE.NET, _this.button.OK);
                            }
                            else if ((error.status >= 400) || (error.status == 500)) { /** catch http request errors */
                                _this.methods.showErrorOkButton(_this.alertCtrl, '', _this.prompt.MESSAGE.GEN_ERR, _this.button.OK);
                            }
                            else {
                                /** added to prevent error response status:0 url: null*/
                                _this.methods.showErrorOkButton(_this.alertCtrl, _this.prompt.TITLE.NET_INT, _this.prompt.MESSAGE.NET, _this.button.OK);
                            }
                        });
                    });
                });
            }
            else {
                this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);
            }
        }
        else {
            this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NO_IMAGE, this.button.OK);
        }
    };
    DocumentsPage.prototype.uploadSingle = function (upload) {
        var _this = this;
        console.log("ImageID: " + upload[0].id);
        if (this.network.type != 'none') {
            this.isShowing(upload[0].id, true);
            // this.loading = this.methods.createLoader(this.loadingCtrl, `<ion-spinner >` + this.loader.UPLOAD + `</ion-spinner>`);
            // this.loading.present().then(() => {
            this.storage.get(this.authService.storageKey).then(function (hkid) {
                _this.authService.uploadDocuments(hkid, _this.navParam.instId, upload).subscribe(function (resp) {
                    console.log(resp);
                    _this.isShowing(upload[0].id, false);
                    var status = resp.status;
                    if (status == "success") {
                        _this.loading.dismiss();
                        var getImageId = document.getElementById("demo");
                        _this.showPrompt = "<img name = \"\" src = \"assets/icon/cloud-upload.png\" class = \"prompt-icon\">";
                        /* this.gallery.forEach((v, i) => {*/
                        /* });*/
                    }
                    else if (status == "unauthorized") { /** auto-logout */
                        _this.storage.clear();
                        _this.navCtrl.setRoot(LoginPage);
                    }
                    else {
                        // this.loading.dismiss();
                        _this.methods.showErrorOkButton(_this.alertCtrl, '', resp.message, _this.button.OK);
                    }
                }, function (error) {
                    console.log(error);
                    _this.isShowing(upload[0].id, false);
                    // this.loading.dismiss();
                    if (error.name == 'TimeoutError') {
                        _this.methods.showErrorOkButton(_this.alertCtrl, _this.prompt.TITLE.NET_ERR, _this.prompt.MESSAGE.NET, _this.button.OK);
                    }
                    else if ((error.status >= 400) || (error.status == 500)) { /** catch http request errors */
                        _this.methods.showErrorOkButton(_this.alertCtrl, '', _this.prompt.MESSAGE.GEN_ERR, _this.button.OK);
                    }
                    else {
                        /** added to prevent error response status:0 url: null*/
                        _this.methods.showErrorOkButton(_this.alertCtrl, _this.prompt.TITLE.NET_INT, _this.prompt.MESSAGE.NET, _this.button.OK);
                    }
                });
            });
            // });
        }
        else {
            this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);
        }
    };
    /**
     * return to previous page after document upload
     *
     * @param message
     */
    DocumentsPage.prototype.showConfirmDialog = function (message) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: '',
            subTitle: message,
            buttons: [
                {
                    text: this.button.OK,
                    handler: function () {
                        _this.navCtrl.pop();
                    }
                }
            ]
        });
        alert.present();
    };
    DocumentsPage.prototype.isShowing = function (imageID, value) {
        this["isShowing_" + imageID] = value;
    };
    DocumentsPage.prototype.getShow = function (imageID) {
        return this["isShowing_" + imageID];
    };
    __decorate([
        ViewChild('myMap'),
        __metadata("design:type", Object)
    ], DocumentsPage.prototype, "myMap", void 0);
    DocumentsPage = __decorate([
        Component({
            selector: 'page-documents',
            templateUrl: 'documents.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            AlertController,
            LoadingController,
            ActionSheetController,
            Camera,
            Methods,
            AuthServiceProvider,
            Storage,
            Network,
            TranslateService])
    ], DocumentsPage);
    return DocumentsPage;
}());
export { DocumentsPage };
//# sourceMappingURL=documents.js.map