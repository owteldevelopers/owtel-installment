import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the MobileNumberMatchingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-mobile-number-matching',
  templateUrl: 'mobile-number-matching.html',
})
export class MobileNumberMatchingPage {
  private navParam;
  
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.navParam = navParams.data;
    console.log(this.navParam);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MobileNumberMatchingPage');
  }

  back() {
    this.navCtrl.pop();
  }

}
