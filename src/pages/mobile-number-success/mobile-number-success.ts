import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Storage } from '@ionic/storage';

import { HomePage } from "../home/home";
import { Device } from '@ionic-native/device';

import { TabsPage } from "../tabs/tabs";

/**
 * Generated class for the MobileNumberSuccessPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-mobile-number-success',
  templateUrl: 'mobile-number-success.html',
})
export class MobileNumberSuccessPage {
  public navParam;

  constructor(public navCtrl: NavController, public navParams: NavParams, private device: Device) {
    this.navParam = navParams.data;
    console.log(this.device.uuid);

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MobileNumberSuccessPage');
    console.log()
  }

  /**
   * Called when button Continue is clicked!
   */
  loginUser() {
    this.navCtrl.setRoot(TabsPage);
    /* this.navCtrl.setRoot(HomePage); */
  }
}
