var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomePage } from "../home/home";
/**
 * Generated class for the MobileNumberSuccessPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MobileNumberSuccessPage = /** @class */ (function () {
    function MobileNumberSuccessPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.navParam = navParams.data;
    }
    MobileNumberSuccessPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MobileNumberSuccessPage');
    };
    /**
     * Called when button Continue is clicked!
     */
    MobileNumberSuccessPage.prototype.loginUser = function () {
        this.navCtrl.setRoot(HomePage);
    };
    MobileNumberSuccessPage = __decorate([
        Component({
            selector: 'page-mobile-number-success',
            templateUrl: 'mobile-number-success.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams])
    ], MobileNumberSuccessPage);
    return MobileNumberSuccessPage;
}());
export { MobileNumberSuccessPage };
//# sourceMappingURL=mobile-number-success.js.map