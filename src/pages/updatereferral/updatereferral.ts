import { Component, ViewChild, ViewChildren, QueryList } from '@angular/core';
import { NavController, NavParams, Keyboard } from 'ionic-angular';
import { LoadingController, Loading, AlertController } from "ionic-angular/index";
import { FormControl } from '@angular/forms';

import { Storage } from '@ionic/storage';
import { Network } from '@ionic-native/network';

import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { Methods } from "../../globals/methods";
import { TranslateService } from '@ngx-translate/core';
import { ReferralsPage } from "../referrals/referrals";

/**
 * Generated class for the UpdatereferralPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
 
@Component({
  selector: 'page-updatereferral',
  templateUrl: 'updatereferral.html',
})
export class UpdatereferralPage {
  @ViewChild('refName') refName;
  @ViewChild('refcustId') refcustId;
  //@ViewChild('refMobile') refMobile;
  //@ViewChild('refMobileMult') refMobileMult;
  @ViewChild('refRemarks') refRemarks;
  
  @ViewChildren('refMobileMulti') refMobileMulti: QueryList<any>;

  

  public anArray: any = [];
  getCheckName : any;
  getCheckNumber: any;
  isDisabled: boolean = false; 
  isEdit : boolean = false;
  name : any;
  mobile_num: any;
  status: any;

  active: number = 1;

  loading: Loading;
  items: any;
  navParam: any;
  prompt: any;
  loader: any;
  button: any;
  info: any;
  mobileStringId =  [];
  mobileStringPhone = [];
  public mobile: any = [];
  hide : any;
  

  constructor(public navCtrl: NavController
    , public navParams: NavParams
    , public loadingCtrl: LoadingController
    , public alertCtrl: AlertController
    , private authService: AuthServiceProvider
    , public methods: Methods
    , private storage: Storage
    , private network: Network
    , public keyboard : Keyboard
    , private translate: TranslateService) {
  }

 
  ionViewDidLoad() {
    this.isDisabled = false;
    this.isEdit = false;

    this.translate.get('PROMPT').subscribe(
      translated => {
        this.prompt = translated;
      }
    );

    this.translate.get('LOADER').subscribe(
      translated => {
        this.loader = translated;
      }
    );

    this.translate.get('BUTTON').subscribe(
      translated => {
        this.button = translated;
      }
    );
    
   

    this.getReferralId()
    

  }

  keyboardCheck() {
    return this.keyboard.isOpen();
  }

  


  getReferralId() {

    let getCustId = this.navParams.data.id

    this.loading = this.loadingCtrl.create({
      content: '<ion-spinner >' + this.loader.LOAD + '</ion-spinner>'
    });

    if (this.network.type != 'none') {
      this.loading.present().then(() => {
       
        this.authService.getReferralById(getCustId).subscribe(
            resp => {
              let message = resp.message;
              let data = resp.data;

               this.info = data;

              this.loading.dismiss();
            }, error => {
              console.log(error);
              this.loading.dismiss();

              if (error.name == 'TimeoutError') {
                this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_ERR, this.prompt.MESSAGE.NET, this.button.OK);

              } else if ((error.status >= 400) || (error.status == 500)) {  /** catch http request errors */
                this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.GEN_ERR, this.button.OK);

              } else {
                this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_INT, this.prompt.MESSAGE.NET, this.button.OK);

              }
            }
          );
        });
     
    } else {

      this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);
    }
  }


  Add() {
    this.anArray.push({ 'value': '' });
  }

  Remove(index){
    this.anArray.splice(index, 1);
  }

  RemovePhone(data, index) {
    data.splice(index, 1);
  }

  AddUpdate() {
    this.mobile.push({ 'value': '' });
  }

  enableFields(){
    this.isDisabled = true;
    this.isEdit = true;

  }


  deleteConfirm() {
      let alert = this.alertCtrl.create({
        title: 'Delete Referral',
        message: 'Do you want to delete this referral?',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Yes',
            handler: () => {
             this.deleteReferral();
            }
          }
        ]
      });
      alert.present();
  }



  deleteReferral() {

    let getCustId = this.navParams.data.id

    this.loading = this.loadingCtrl.create({
      content: '<ion-spinner >' + this.loader.LOAD + '</ion-spinner>'
    });

    if (this.network.type != 'none') {
      this.loading.present().then(() => {

        this.authService.deleteReferral(getCustId).subscribe(
          resp => {
            
            this.navCtrl.setRoot(ReferralsPage);
            //this.filterItems(data, this.searchTerm);

            this.loading.dismiss();
          }, error => {
            console.log(error);
            this.loading.dismiss();

            if (error.name == 'TimeoutError') {
              this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_ERR, this.prompt.MESSAGE.NET, this.button.OK);

            } else if ((error.status >= 400) || (error.status == 500)) {  /** catch http request errors */
              this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.GEN_ERR, this.button.OK);

            } else {
              this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_INT, this.prompt.MESSAGE.NET, this.button.OK);

            }
          }
        );
      });

    } else {

      this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);
    }
  }

  updateReferral() {
    let mobile = [];
    let mobile2 = [];

    let name = this.refName.value;
    let refcustId = this.refcustId.value;
    let remarks = '';

    console.log(refcustId);

    this.refMobileMulti.forEach(function (phone) {
      let getElement = phone._elementRef.nativeElement.id;
      let id = (getElement == 'undefined' ? '0' : getElement);

      mobile2.push({ 'id': id, 'phone': phone._value });
       
     
    });


    /* mobile.push(mobile2); */

    if (this.network.type != 'none') {
      this.loading = this.methods.createLoader(this.loadingCtrl, `<ion-spinner >` + this.loader.PLS_WAIT + `</ion-spinner>`);
      this.loading.present().then(() => {

    
        this.storage.get(this.authService.storageKey).then((hkid) => {

          let getPhoneDataArray = [];
          let getMobile = [];
         // let convertArrMobile = mobile2;
          let pushMobile = [];
          let pushName = [];

          this.authService.getReferral(hkid).subscribe(
            resp => {
              let getResp = resp.data;

              console.log(getResp);
              getResp.forEach(e => {
                let getPhone = e.phone;
                getPhone.forEach(getDataPhone => {
                  getPhoneDataArray.push(getDataPhone.PhoneNo);
                });
              });

              mobile2.forEach(e => {
                pushMobile.push(e.phone);
                
              })
              
              pushMobile.forEach(phoneInput => {
                getMobile.push(getPhoneDataArray.find(k => k == phoneInput));
              });


              this.authService.getReferralById(refcustId).subscribe(
                    response => {

                      let respdata = response.data
                      let arrayData = [];
                      let arrayPhoneData = [];

                      

                      respdata.forEach(element => {
                        element.phone.forEach(e => {
                          arrayData.push(e.PhoneNo);
                        });
                      });

                     

                      let getVal = pushMobile.filter(item => arrayData.indexOf(item) < 0);
                      //console.log(getVal);

                      if(getVal.length == 0){
                        this.updateData(refcustId, name, remarks, mobile2);
                        
                        return;
                      } else if(getVal.length >= 1){

                        getVal.forEach(phoneInput => {
                          arrayPhoneData.push(getPhoneDataArray.find(k => k == phoneInput));
                        });

                       // console.log(arrayPhoneData);
                      

                        if (arrayPhoneData.length == 0){
                          this.updateData(refcustId, name, remarks, mobile2);
                          return;
                        } else if (arrayPhoneData.length >= 1 && arrayPhoneData.indexOf(undefined) > -1) {
                          this.updateData(refcustId, name, remarks, mobile2);
                          return;
                        }else{
                          this.alertexistInfo();
                          return
                        }

                      }     
                  });
            },
          );
          
          });
        
      });
    
      
    } else {
      this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);
    }
  }

  alertexistInfo() {
    let alert = this.alertCtrl.create({
      title: '',
      subTitle: 'Already an OWTEL customer',
      buttons: [
        {
          text: this.button.OK,
          handler: () => {
            this.loading.dismiss();
          }
        }
      ]
    });
    alert.present();
  }

  updateData(refcustId, name, remarks, mobile2){
    console.log(remarks);
    this.authService.updateReferral(refcustId, name, remarks, mobile2).subscribe(
      resp => {
        console.log(resp);

        this.showConfirm();
        this.loading.dismiss();

      }, error => {
        console.log(error);
        this.loading.dismiss();

        if (error.name == 'TimeoutError') {
          this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_ERR, this.prompt.MESSAGE.NET, this.button.OK);

        }

      })
  }

  showConfirmExist(mobile) {
    let checkMobile = [];
    let checkTrue = true;

    mobile.forEach(element => {
      checkMobile.push(element.phone);
    });

    
    if (this.chkDuplicates(checkMobile, checkTrue)) {
      let alert = this.alertCtrl.create({
        title: '',
        subTitle: 'Already an OWTEL customer',
        buttons: [
          {
            text: this.button.OK,
            handler: () => {

            }
          }
        ]
      });
      alert.present();

    }
  }

  showConfirm(){

    let alert = this.alertCtrl.create({
      title: '',
      subTitle: this.prompt.MESSAGE.UPDATE_REF,
      buttons: [
        {
          text: this.button.OK,
          handler: () => {
            this.navCtrl.setRoot(ReferralsPage);
          }
        }
      ]
    });
    alert.present();

  }

  chkDuplicates(arr, justCheck) {
    var len = arr.length, tmp = {}, arrtmp = arr.slice(), dupes = [];
    arrtmp.sort();
    while (len--) {
      var val = arrtmp[len];
      if (/nul|nan|infini/i.test(String(val))) {
        val = String(val);
      }
      if (tmp[JSON.stringify(val)]) {
        if (justCheck) { return true; }
        dupes.push(val);
      }
      tmp[JSON.stringify(val)] = true;
    }
    return justCheck ? false : dupes.length ? dupes : null;
  }

  getStatus(status){
    return status == undefined ? 'Not yet apply' : status;
  }

  getMobile(mobile){
    return mobile == undefined ? '' : mobile;
  }


  onKeyupName(e: any) {
    let elementChecker: string;
    elementChecker = e.target.value;
    let getCheck = elementChecker.slice(-1);
    let convertToString = getCheck.toString();
    console.log(convertToString);
    if ((convertToString.charCodeAt(0) >= 33 && convertToString.charCodeAt(0) <= 47) || (convertToString.charCodeAt(0) >= 58 && convertToString.charCodeAt(0) <= 64) || (convertToString.charCodeAt(0) >= 91 && convertToString.charCodeAt(0) <= 96) || (convertToString.charCodeAt(0) >= 123 && convertToString.charCodeAt(0) <= 126) || (convertToString.charCodeAt(0) >= 128 && convertToString.charCodeAt(0) <= 253)) {
      this.getCheckName = elementChecker.slice(0, -1);
    }
  }

  onKeyupNumber(e: any) {
    let elementChecker: string;
    elementChecker = e.target.value;
    let getCheck = elementChecker.slice(-1);
    let convertToString = getCheck.toString();

    if ((convertToString.charCodeAt(0) >= 33 && convertToString.charCodeAt(0) <= 47) || (convertToString.charCodeAt(0) >= 58 && convertToString.charCodeAt(0) <= 64) || (convertToString.charCodeAt(0) >= 91 && convertToString.charCodeAt(0) <= 96) || (convertToString.charCodeAt(0) >= 123 && convertToString.charCodeAt(0) <= 126) || (convertToString.charCodeAt(0) >= 128 && convertToString.charCodeAt(0) <= 253)) {
      this.getCheckNumber = elementChecker.slice(0, -1);
    }
  }

  
  


}
