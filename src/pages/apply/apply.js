var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { Network } from '@ionic-native/network';
import { MobileNumberVerificationPage } from "../mobile-number-verification/mobile-number-verification";
import { Methods } from '../../globals/methods';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { MobileNumberAttentionPage } from "../mobile-number-attention/mobile-number-attention";
import { MobileNumberMatchingPage } from "../mobile-number-matching/mobile-number-matching";
import { TranslateService } from '@ngx-translate/core';
var ApplyPage = /** @class */ (function () {
    function ApplyPage(navCtrl, loadingCtrl, alertCtrl, navParams, formBuilder, methods, authService, network, translate) {
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.methods = methods;
        this.authService = authService;
        this.network = network;
        this.translate = translate;
        this.navParam = "";
        this.validations_form = this.formBuilder.group({
            firstName: new FormControl('', Validators.compose([
                Validators.required
            ])), hkidFirst: new FormControl('', Validators.compose([
                Validators.required,
                Validators.pattern('^[A-Z ]+$')
            ])), hkidMid: new FormControl('', Validators.compose([
                Validators.required,
                Validators.minLength(6),
                Validators.pattern('^[0-9]+$')
            ])), hkidLast: new FormControl('', Validators.compose([
                Validators.required,
                Validators.pattern('^[A-Z0-9]+$')
            ])), number: new FormControl('', Validators.compose([
                Validators.required,
                Validators.pattern('^[0-9]+$')
            ]))
        });
    }
    ApplyPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad ApplyPage hello');
        this.translate.get('PROMPT').subscribe(function (translated) {
            _this.prompt = translated;
        });
        this.translate.get('LOADER').subscribe(function (translated) {
            _this.loader = translated;
        });
        this.translate.get('BUTTON').subscribe(function (translated) {
            _this.button = translated;
        });
    };
    ApplyPage.prototype.apply = function () {
        var _this = this;
        console.log("Apply clicked!");
        var hkidF = this.idFirst.value.trim();
        var hkidM = this.idMiddle.value.trim();
        var hkidL = this.idLast.value.trim();
        if (hkidF != "") {
            if (hkidF.length == 1) {
                hkidF = hkidF + ' '; /** auto fill of space if user enters only one character */
            }
        }
        var id = hkidF + hkidM + hkidL;
        if (id.length != 9) {
            this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.INV_HKID, this.button.OK);
            return;
        }
        if (this.network.type != 'none') {
            this.loading = this.methods.createLoader(this.loadingCtrl, "<ion-spinner >" + this.loader.PLS_WAIT + "</ion-spinner>");
            this.loading.present().then(function () {
                _this.authService.apply(_this.firstName.value, _this.lastName.value, id, _this.number.value, 1).subscribe(function (resp) {
                    console.log(resp);
                    _this.loading.dismiss();
                    var status = resp.status;
                    if (status == "success") {
                        var second = resp.seconds;
                        var title = resp.title;
                        var subTitle = resp.subTitle;
                        _this.navCtrl.push(MobileNumberVerificationPage, {
                            id: id,
                            number: _this.number.value,
                            numberMask: _this.methods.masknumber(_this.number.value),
                            time: second,
                            title: title,
                            subTitle: subTitle
                        });
                    }
                    else if (status == "unauthorized") {
                        var title = resp.title;
                        var subTitle = resp.subTitle;
                        _this.navCtrl.push(MobileNumberAttentionPage, {
                            id: id,
                            number: _this.number.value,
                            numberMask: _this.methods.masknumber(_this.number.value),
                            title: title,
                            subTitle: subTitle
                        });
                    }
                    else if (status == "mismatched") {
                        var title = resp.title;
                        var subTitle = resp.subTitle;
                        _this.navCtrl.push(MobileNumberMatchingPage, {
                            title: title,
                            subTitle: subTitle
                        });
                    }
                    else {
                        _this.methods.showErrorOkButton(_this.alertCtrl, '', resp.message, _this.button.OK);
                    }
                }, function (error) {
                    console.log(error);
                    _this.loading.dismiss();
                    if (error.name == 'TimeoutError') {
                        _this.methods.showErrorOkButton(_this.alertCtrl, _this.prompt.TITLE.NET_ERR, _this.prompt.MESSAGE.NET, _this.button.OK);
                    }
                    else {
                        _this.methods.showErrorOkButton(_this.alertCtrl, '', _this.prompt.MESSAGE.GEN_ERR, _this.button.OK);
                    }
                });
            });
        }
        else {
            this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);
        }
    };
    ApplyPage.prototype.back = function () {
        this.navCtrl.pop();
    };
    /**
     * For autofocus to next textbox
     *
     * @param myText
     *
     * TODO: optimize because this is called twice
     */
    ApplyPage.prototype.onChangeTextFirst = function (myText) {
        if (myText.value.length == 2) {
            this.idMiddle.setFocus();
        }
    };
    /**
     * For autofocus to next textbox
     *
     * @param myText
     *
     * TODO: optimize because this is called twice
     */
    ApplyPage.prototype.onChangeTextSecond = function (myText) {
        if (myText.value.length == 6) {
            this.idLast.setFocus();
        }
    };
    __decorate([
        ViewChild('firstName'),
        __metadata("design:type", Object)
    ], ApplyPage.prototype, "firstName", void 0);
    __decorate([
        ViewChild('lastName'),
        __metadata("design:type", Object)
    ], ApplyPage.prototype, "lastName", void 0);
    __decorate([
        ViewChild('idFirst'),
        __metadata("design:type", Object)
    ], ApplyPage.prototype, "idFirst", void 0);
    __decorate([
        ViewChild('idMiddle'),
        __metadata("design:type", Object)
    ], ApplyPage.prototype, "idMiddle", void 0);
    __decorate([
        ViewChild('idLast'),
        __metadata("design:type", Object)
    ], ApplyPage.prototype, "idLast", void 0);
    __decorate([
        ViewChild('number'),
        __metadata("design:type", Object)
    ], ApplyPage.prototype, "number", void 0);
    ApplyPage = __decorate([
        Component({
            selector: 'page-apply',
            templateUrl: 'apply.html',
        }),
        __metadata("design:paramtypes", [NavController,
            LoadingController,
            AlertController,
            NavParams,
            FormBuilder,
            Methods,
            AuthServiceProvider,
            Network,
            TranslateService])
    ], ApplyPage);
    return ApplyPage;
}());
export { ApplyPage };
//# sourceMappingURL=apply.js.map