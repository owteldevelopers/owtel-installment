import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { IonicPage, NavController, NavParams, LoadingController, Loading, AlertController, Platform } from 'ionic-angular';
import { Network } from '@ionic-native/network';

import { MobileNumberVerificationPage } from "../mobile-number-verification/mobile-number-verification";
import { Methods } from '../../globals/methods';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { MobileNumberAttentionPage } from "../mobile-number-attention/mobile-number-attention";
import { MobileNumberMatchingPage } from "../mobile-number-matching/mobile-number-matching";
import { TranslateService } from '@ngx-translate/core';
import * as $ from 'jquery';


@Component({
    selector: 'page-apply',
    templateUrl: 'apply.html',
})
export class ApplyPage {
    @ViewChild('firstName') firstName;
    @ViewChild('lastName') lastName;
    @ViewChild('idFirst') idFirst;
    @ViewChild('idMiddle') idMiddle;
    @ViewChild('idLast') idLast;
    @ViewChild('number') number;

    @ViewChild('idHKID') idHKID;

    validations_form: FormGroup;
    loading: Loading;

    prompt: any;
    loader: any;
    button: any;
    getCapitalData : any;

    private navParam: any = "";

    constructor(public navCtrl: NavController
        , public loadingCtrl: LoadingController
        , private platform: Platform
        , public alertCtrl: AlertController
        , public navParams: NavParams
        , public formBuilder: FormBuilder
        , private methods: Methods
        , private authService: AuthServiceProvider
        , private network: Network
        , private translate: TranslateService) {
        this.validations_form = this.formBuilder.group({
            firstName: new FormControl(
                '', Validators.compose([
                    Validators.required
                ])
            ), hkidFirst: new FormControl(
                '', Validators.compose([
                    Validators.required,
                    Validators.minLength(9),
                    //Validators.pattern('^[A-Za-z0-9]+$')
                ])
            ), number: new FormControl(
                '', Validators.compose([
                    Validators.required,
                    Validators.pattern('^[0-9]+$')
                ])
            )
        });
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad ApplyPage hello');

        this.translate.get('PROMPT').subscribe(
            translated => {
                this.prompt = translated;
            }
        );

        this.translate.get('LOADER').subscribe(
            translated => {
                this.loader = translated;
            }
        );

        this.translate.get('BUTTON').subscribe(
            translated => {
                this.button = translated;
            }
        );

    }

    apply() {
        console.log("Apply clicked!");

        let hkidF = this.idHKID.value.toUpperCase();
        /* let hkidM = this.idMiddle.value.trim();
        let hkidL = this.idLast.value.trim(); 
        if (hkidF != "") {
            if (hkidF.length == 1) {
                hkidF = hkidF + ' '; 
            }
        } */

        let id = hkidF; /* + hkidM + hkidL */;

        var firstString = id.substr(0, 1);
        var secString = id.substr(1, 1);
        var thirdString = id.substr(2, 1);
        var fourthString = id.substr(3, 1);
        var fifthString = id.substr(4, 1);
        var sixthString = id.substr(5, 1);
        var seventhString = id.substr(6, 1);
        var eightString = id.substr(7, 1);
        var ninthString = id.substr(8, 1);


        if (id.length != 9) {
            this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.INV_HKID, this.button.OK);
            return;
        }

        if (this.network.type != 'none') {
            this.loading = this.methods.createLoader(this.loadingCtrl, `<ion-spinner >` + this.loader.PLS_WAIT + `</ion-spinner>`);
            this.loading.present().then(() => {
                this.authService.apply(this.firstName.value, this.lastName.value, id, this.number.value, 1).subscribe(
                    resp => {
                        console.log(resp);
                        this.loading.dismiss();

                        var status = resp.status;
                        if (status == "success") {
                            var second = resp.seconds;
                            var title = resp.title;
                            var subTitle = resp.subTitle;

                            this.navCtrl.push(MobileNumberVerificationPage, {
                                id: id
                                , number: this.number.value
                                , numberMask: this.methods.masknumber(this.number.value)
                                , time: second
                                , title: title
                                , subTitle: subTitle
                            });

                        } else if (status == "unauthorized") {
                            var title = resp.title;
                            var subTitle = resp.subTitle;

                            this.navCtrl.push(MobileNumberAttentionPage, {
                                id: id
                                , number: this.number.value
                                , numberMask: this.methods.masknumber(this.number.value)
                                , title: title
                                , subTitle: subTitle
                            });

                        } else if (status == "mismatched") {
                            var title = resp.title;
                            var subTitle = resp.subTitle;

                            this.navCtrl.push(MobileNumberMatchingPage, {
                                title: title
                                , subTitle: subTitle
                            });

                        } else {
                            this.methods.showErrorOkButton(this.alertCtrl, '', resp.message, this.button.OK);

                        }
                    }, error => {
                        console.log(error);
                        this.loading.dismiss();

                        if (error.name == 'TimeoutError') {
                            this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_ERR, this.prompt.MESSAGE.NET, this.button.OK);

                        } else {

                            let format_special_letter = new RegExp("^[^<>{}\"/|;:.,~!?@#$%^=&*\\]\\\\()\\[¿§«»ω⊙¤°℃℉€¥£¢¡®©0-9_+]*$");
                            let format_special_num = new RegExp("^[^<>{}\"/|;:.,~!?@#$%^=&*\\]\\\\()\\[¿§«»ω⊙¤°℃℉€¥£¢¡®©A-Za-z_+]*$");
                            let format_special_char = new RegExp("[^<>{}\"/|;:.,~!?@#$%^=&*\\]\\\\()\\[¿§«»ω⊙¤°℃℉€¥£¢¡®©_+]*$");

                            let format_o = new RegExp("^[Oo]*$");

                            if (firstString.match(format_special_num)) {
                                this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.INV_LETTER_ONLY, this.button.OK);
                            } else if (thirdString.match(format_special_letter) || fourthString.match(format_special_letter) || fifthString.match(format_special_letter) || sixthString.match(format_special_letter) || seventhString.match(format_special_letter) || eightString.match(format_special_letter)) {
                                this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.INV_NUMBERS_ONLY, this.button.OK);
                            } /* else if (ninthString.mat`ch(format_o)) {
                                    this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.INV_CHARACTER_O, this.button.OK);
                                } */ /* else if (ninthString.match(format_special_char) && ninthString != '') {
                                this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.INV_LAST_CHAR, this.button.OK);
                            } */ else if ((id.trim().length >= 10)) {
                                this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.INV_HKID_MAX, this.button.OK);
                            } else if ((id.trim().length < 9)) {
                                this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.INV_HKID_MAX, this.button.OK);
                            } 
                           

                        }

                    })
            });
        } else {
            this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);
        }
    }

    back() {
        this.navCtrl.pop();
    }

    /**
     * For autofocus to next textbox
     *
     * @param myText
     *
     * TODO: optimize because this is called twice
     */
    onChangeTextFirst(myText) {
        if (myText.value.length == 2) {
            this.idMiddle.setFocus();
        }
    }

    /**
     * For autofocus to next textbox
     *
     * @param myText
     *
     * TODO: optimize because this is called twice
     */
    onChangeTextSecond(myText) {
        if (myText.value.length == 6) {
            this.idLast.setFocus();
        }
    }

    onKeyupHKID(e) {
        let elementChecker: string;
        elementChecker = e.target.value;


        let format = new RegExp('^[A-Za-z0-9]+$');

        var firstString = elementChecker.substr(0, 1);
        var secString = elementChecker.substr(1, 1);
        var thirdString = elementChecker.substr(2, 1);
        var fourthString = elementChecker.substr(3, 1);
        var fifthString = elementChecker.substr(4, 1);
        var sixthString = elementChecker.substr(5, 1);
        var seventhString = elementChecker.substr(6, 1);
        var eightString = elementChecker.substr(7, 1);
        var ninthString = elementChecker.substr(8, 1);
        /* 
         console.log(this.device.version); */


        let platformVersion = this.platform.versions();

        if (platformVersion.android) {
            let androidV = platformVersion.android.num;
            console.log(androidV);
            if (androidV <= 4.4) {
                if (secString.charCodeAt(0) >= 48 && secString.charCodeAt(0) <= 57) {
                    this.getCapitalData = secString.replace(secString, firstString + ' ').concat(secString);
                }
            } else {
                if ((firstString.charCodeAt(0) >= 48 && firstString.charCodeAt(0) <= 57) || (firstString.charCodeAt(0) == 32) || (firstString.charCodeAt(0) == 95)) {
                    this.getCapitalData = elementChecker.slice(0, -1);

                } else if ((secString.charCodeAt(0) >= 48 && secString.charCodeAt(0) <= 57)) {
                    this.getCapitalData = secString.replace(secString, firstString + ' ').concat(secString);

                } else if (secString.charCodeAt(0) == 95) {
                    this.getCapitalData = elementChecker.slice(0, -1);

                } else if ((thirdString.charCodeAt(0) >= 65 && thirdString.charCodeAt(0) <= 90) || (thirdString.charCodeAt(0) == 32) || (thirdString.charCodeAt(0) == 95) || (fourthString.charCodeAt(0) >= 65 && fourthString.charCodeAt(0) <= 90) || (fourthString.charCodeAt(0) == 32) || (fourthString.charCodeAt(0) == 95) || (fifthString.charCodeAt(0) >= 65 && fifthString.charCodeAt(0) <= 90) || (fifthString.charCodeAt(0) == 32) || (fifthString.charCodeAt(0) == 95) || (sixthString.charCodeAt(0) >= 65 && sixthString.charCodeAt(0) <= 90) || (sixthString.charCodeAt(0) == 32) || (sixthString.charCodeAt(0) == 95) || (seventhString.charCodeAt(0) >= 65 && seventhString.charCodeAt(0) <= 90) || (seventhString.charCodeAt(0) == 32) || (seventhString.charCodeAt(0) == 95) || (eightString.charCodeAt(0) >= 65 && eightString.charCodeAt(0) <= 90) || (eightString.charCodeAt(0) == 32) || (eightString.charCodeAt(0) == 95)) {
                    this.getCapitalData = elementChecker.slice(0, -1);

                } else if ((ninthString.charCodeAt(0) == 79) || (ninthString.charCodeAt(0) == 32) || (ninthString.charCodeAt(0) == 95)) {
                    this.getCapitalData = elementChecker.slice(0, -1);
                } else {
                    this.getCapitalData = elementChecker.replace(/[^\w\s]/gi, '');
                }
            }

        } else {
            if ((firstString.charCodeAt(0) >= 48 && firstString.charCodeAt(0) <= 57) || (firstString.charCodeAt(0) == 32) || (firstString.charCodeAt(0) == 95)) {
                this.getCapitalData = elementChecker.slice(0, -1);

            } else if ((secString.charCodeAt(0) >= 48 && secString.charCodeAt(0) <= 57)) {
                this.getCapitalData = secString.replace(secString, firstString + ' ').concat(secString);

            } else if (secString.charCodeAt(0) == 95) {
                this.getCapitalData = elementChecker.slice(0, -1);

            } else if ((thirdString.charCodeAt(0) >= 65 && thirdString.charCodeAt(0) <= 90) || (thirdString.charCodeAt(0) == 32) || (thirdString.charCodeAt(0) == 95) || (fourthString.charCodeAt(0) >= 65 && fourthString.charCodeAt(0) <= 90) || (fourthString.charCodeAt(0) == 32) || (fourthString.charCodeAt(0) == 95) || (fifthString.charCodeAt(0) >= 65 && fifthString.charCodeAt(0) <= 90) || (fifthString.charCodeAt(0) == 32) || (fifthString.charCodeAt(0) == 95) || (sixthString.charCodeAt(0) >= 65 && sixthString.charCodeAt(0) <= 90) || (sixthString.charCodeAt(0) == 32) || (sixthString.charCodeAt(0) == 95) || (seventhString.charCodeAt(0) >= 65 && seventhString.charCodeAt(0) <= 90) || (seventhString.charCodeAt(0) == 32) || (seventhString.charCodeAt(0) == 95) || (eightString.charCodeAt(0) >= 65 && eightString.charCodeAt(0) <= 90) || (eightString.charCodeAt(0) == 32) || (eightString.charCodeAt(0) == 95)) {
                this.getCapitalData = elementChecker.slice(0, -1);

            } else if ((ninthString.charCodeAt(0) == 79) || (ninthString.charCodeAt(0) == 32) || (ninthString.charCodeAt(0) == 95)) {
                this.getCapitalData = elementChecker.slice(0, -1);
            } else {
                this.getCapitalData = elementChecker.replace(/[^\w\s]/gi, '');
            }
        }

        $('.inputHKID').find('input[type="text"]').val(function () {
            return this.value.toUpperCase();
        });

    }


}
