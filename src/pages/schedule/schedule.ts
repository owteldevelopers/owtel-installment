import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Loading, LoadingController, AlertController } from "ionic-angular/index";

import { Storage } from '@ionic/storage';

import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { Methods } from "../../globals/methods";
import { PayPage } from "../pay/pay";
import { LoginPage } from "../login/login";

import { Network } from '@ionic-native/network';
import { TranslateService } from '@ngx-translate/core';



@Component({
    selector: 'page-schedule',
    templateUrl: 'schedule.html',
})
export class SchedulePage {
    navParam: any;
    loading: Loading;
    schedule: Array<{ date: string, amount: string, isPaid: boolean, paidAmt: string, bal: string }>;
    nextDue: Array<{ date: string, amount: string }>;
    prompt: any;
    loader: any;
    button: any;

    constructor(public navCtrl: NavController
        , public navParams: NavParams
        , public loadingCtrl: LoadingController
        , public alertCtrl: AlertController
        , private authService: AuthServiceProvider
        , public methods: Methods
        , private storage: Storage
        , private network: Network
        , private translate: TranslateService) {
        this.navParam = navParams.data;
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad SchedulePage');

        this.translate.get('PROMPT').subscribe(
            translated => {
                this.prompt = translated;
            }
        );

        this.translate.get('LOADER').subscribe(
            translated => {
                this.loader = translated;
            }
        );

        this.translate.get('BUTTON').subscribe(
            translated => {
                this.button = translated;
            }
        );

        this.schedule = [];
        this.nextDue = [];
        this.getSchedule();
    }

    getSchedule() {
        if (this.network.type != 'none') {
            this.loading = this.methods.createLoader(this.loadingCtrl, `<ion-spinner >` + this.loader.LOAD + `</ion-spinner>`);
            this.loading.present().then(() => {
                this.storage.get(this.authService.storageKey).then((hkid) => {
                    this.authService.getInstallmentDetails(hkid, this.navParam.instId, this.authService.reqType2)
                        .subscribe(
                            resp => {
                                console.log(resp);
                                var status = resp.status;

                                if (status == "success") {
                                    let details = resp.details;
                                    details.forEach((v, i) => {
                                        if (i == 0) {
                                            this.schedule = v.schedule;

                                        } else if (i == 1) {
                                            this.nextDue = v.nextDue;

                                        }
                                    });

                                } else if (status == "unauthorized") {
                                    this.storage.clear();
                                    this.navCtrl.setRoot(LoginPage);

                                } else {
                                    this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.GEN_ERR, this.button.OK);

                                }

                                this.loading.dismiss();
                            }, error => {
                                console.log(error);
                                this.loading.dismiss();

                                if (error.name == 'TimeoutError') {
                                    this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_ERR, this.prompt.MESSAGE.NET, this.button.OK);

                                } else {
                                    this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.GEN_ERR, this.button.OK);

                                }
                            }
                        );
                });
            });
        } else {
            this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);
            
        }
    }

    btnPay() {
        console.log("Btn Pay Clicked!");
        this.navCtrl.push(PayPage, { id: this.navParam.id, instId: this.navParam.instId });
    }
}
