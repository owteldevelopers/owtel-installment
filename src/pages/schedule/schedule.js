var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { LoadingController, AlertController } from "ionic-angular/index";
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { Methods } from "../../globals/methods";
import { PayPage } from "../pay/pay";
import { LoginPage } from "../login/login";
import { Network } from '@ionic-native/network';
import { TranslateService } from '@ngx-translate/core';
var SchedulePage = /** @class */ (function () {
    function SchedulePage(navCtrl, navParams, loadingCtrl, alertCtrl, authService, methods, storage, network, translate) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.authService = authService;
        this.methods = methods;
        this.storage = storage;
        this.network = network;
        this.translate = translate;
        this.navParam = navParams.data;
    }
    SchedulePage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad SchedulePage');
        this.translate.get('PROMPT').subscribe(function (translated) {
            _this.prompt = translated;
        });
        this.translate.get('LOADER').subscribe(function (translated) {
            _this.loader = translated;
        });
        this.translate.get('BUTTON').subscribe(function (translated) {
            _this.button = translated;
        });
        this.schedule = [];
        this.nextDue = [];
        this.getSchedule();
    };
    SchedulePage.prototype.getSchedule = function () {
        var _this = this;
        if (this.network.type != 'none') {
            this.loading = this.methods.createLoader(this.loadingCtrl, "<ion-spinner >" + this.loader.LOAD + "</ion-spinner>");
            this.loading.present().then(function () {
                _this.storage.get(_this.authService.storageKey).then(function (hkid) {
                    _this.authService.getInstallmentDetails(hkid, _this.navParam.instId, _this.authService.reqType2)
                        .subscribe(function (resp) {
                        console.log(resp);
                        var status = resp.status;
                        if (status == "success") {
                            var details = resp.details;
                            details.forEach(function (v, i) {
                                if (i == 0) {
                                    _this.schedule = v.schedule;
                                }
                                else if (i == 1) {
                                    _this.nextDue = v.nextDue;
                                }
                            });
                        }
                        else if (status == "unauthorized") {
                            _this.storage.clear();
                            _this.navCtrl.setRoot(LoginPage);
                        }
                        else {
                            _this.methods.showErrorOkButton(_this.alertCtrl, '', _this.prompt.MESSAGE.GEN_ERR, _this.button.OK);
                        }
                        _this.loading.dismiss();
                    }, function (error) {
                        console.log(error);
                        _this.loading.dismiss();
                        if (error.name == 'TimeoutError') {
                            _this.methods.showErrorOkButton(_this.alertCtrl, _this.prompt.TITLE.NET_ERR, _this.prompt.MESSAGE.NET, _this.button.OK);
                        }
                        else {
                            _this.methods.showErrorOkButton(_this.alertCtrl, '', _this.prompt.MESSAGE.GEN_ERR, _this.button.OK);
                        }
                    });
                });
            });
        }
        else {
            this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);
        }
    };
    SchedulePage.prototype.btnPay = function () {
        console.log("Btn Pay Clicked!");
        this.navCtrl.push(PayPage, { id: this.navParam.id, instId: this.navParam.instId });
    };
    SchedulePage = __decorate([
        Component({
            selector: 'page-schedule',
            templateUrl: 'schedule.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            LoadingController,
            AlertController,
            AuthServiceProvider,
            Methods,
            Storage,
            Network,
            TranslateService])
    ], SchedulePage);
    return SchedulePage;
}());
export { SchedulePage };
//# sourceMappingURL=schedule.js.map