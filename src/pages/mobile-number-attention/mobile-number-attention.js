var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AlertController, LoadingController } from 'ionic-angular';
import { Network } from '@ionic-native/network';
import { Methods } from "../../globals/methods";
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { MobileNumberVerificationPage } from "../mobile-number-verification/mobile-number-verification";
import { TranslateService } from '@ngx-translate/core';
var MobileNumberAttentionPage = /** @class */ (function () {
    function MobileNumberAttentionPage(navCtrl, navParams, methods, loadingCtrl, alertCtrl, authService, network, translate) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.methods = methods;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.authService = authService;
        this.network = network;
        this.translate = translate;
        this.navParam = navParams.data;
    }
    MobileNumberAttentionPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad MobileNumberAttentionPage');
        this.translate.get('PROMPT').subscribe(function (translated) {
            _this.prompt = translated;
        });
        this.translate.get('LOADER').subscribe(function (translated) {
            _this.loader = translated;
        });
        this.translate.get('BUTTON').subscribe(function (translated) {
            _this.button = translated;
        });
    };
    MobileNumberAttentionPage.prototype.verifyMobile = function () {
        var _this = this;
        if (this.network.type != 'none') { /** check if device has network connection */
            this.loading = this.methods.createLoader(this.loadingCtrl, "<ion-spinner >" + this.loader.VERIFY + "</ion-spinner>");
            this.loading.present().then(function () {
                _this.authService.verifyID(_this.navParam.id, _this.navParam.number, 2).subscribe(function (resp) {
                    console.log(resp);
                    var status = resp.status;
                    var title = resp.title;
                    var subTitle = resp.subTitle;
                    if (status == "success" || status == "unauthorized") {
                        var second = resp.seconds;
                        _this.navCtrl.push(MobileNumberVerificationPage, {
                            id: _this.navParam.id,
                            number: _this.navParam.number,
                            numberMask: _this.methods.masknumber(_this.navParam.number),
                            time: second,
                            title: title,
                            subTitle: subTitle
                        });
                    }
                    else {
                        _this.loading.dismiss();
                        _this.methods.showErrorOkButton(_this.alertCtrl, '', resp.message, _this.button.OK);
                    }
                }, function (error) {
                    console.log(error);
                    _this.loading.dismiss();
                    if (error.name == 'TimeoutError') {
                        _this.methods.showErrorOkButton(_this.alertCtrl, _this.prompt.TITLE.NET_ERR, _this.prompt.MESSAGE.NET, _this.button.OK);
                    }
                    else {
                        _this.methods.showErrorOkButton(_this.alertCtrl, '', _this.prompt.MESSAGE.GEN_ERR, _this.button.OK);
                    }
                });
            });
        }
        else {
            this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);
        }
    };
    MobileNumberAttentionPage = __decorate([
        Component({
            selector: 'page-mobile-number-attention',
            templateUrl: 'mobile-number-attention.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            Methods,
            LoadingController,
            AlertController,
            AuthServiceProvider,
            Network,
            TranslateService])
    ], MobileNumberAttentionPage);
    return MobileNumberAttentionPage;
}());
export { MobileNumberAttentionPage };
//# sourceMappingURL=mobile-number-attention.js.map