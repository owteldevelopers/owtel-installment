import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController, LoadingController, Loading } from 'ionic-angular';

import { Network } from '@ionic-native/network';

import { Methods } from "../../globals/methods";
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { LoginPage } from "../login/login";
import { MobileNumberVerificationPage } from "../mobile-number-verification/mobile-number-verification";

import { TranslateService } from '@ngx-translate/core';



@Component({
    selector: 'page-mobile-number-attention',
    templateUrl: 'mobile-number-attention.html',
})
export class MobileNumberAttentionPage {
    private navParam;
    loading: Loading;

    prompt: any;
    loader: any;
    button: any;

    constructor(public navCtrl: NavController
        , public navParams: NavParams
        , public methods: Methods
        , public loadingCtrl: LoadingController
        , public alertCtrl: AlertController
        , public authService: AuthServiceProvider
        , private network: Network
        , private translate: TranslateService) {
        this.navParam = navParams.data;

    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad MobileNumberAttentionPage');

        this.translate.get('PROMPT').subscribe(
            translated => {
                this.prompt = translated;
            }
        );

        this.translate.get('LOADER').subscribe(
            translated => {
                this.loader = translated;
            }
        );

        this.translate.get('BUTTON').subscribe(
            translated => {
                this.button = translated;
            }
        );
    }

    verifyMobile() {
        if (this.network.type != 'none') { /** check if device has network connection */
            this.loading = this.methods.createLoader(this.loadingCtrl, `<ion-spinner >` + this.loader.VERIFY + `</ion-spinner>`);
            this.loading.present().then(() => {
                this.authService.verifyID(this.navParam.id, this.navParam.number, 2).subscribe(
                    resp => {
                        console.log(resp);
                        var status = resp.status;
                        var title = resp.title;
                        var subTitle = resp.subTitle;

                        if (status == "success" || status == "unauthorized") {
                            var second = resp.seconds;
                            this.navCtrl.push(MobileNumberVerificationPage, {
                                id: this.navParam.id
                                , number: this.navParam.number
                                , numberMask: this.methods.masknumber(this.navParam.number)
                                , time: second
                                , title: title
                                , subTitle: subTitle
                            });

                        } else {
                            this.loading.dismiss();
                            this.methods.showErrorOkButton(this.alertCtrl, '', resp.message, this.button.OK);

                        }

                    }, error => {
                        console.log(error);
                        this.loading.dismiss();

                        if (error.name == 'TimeoutError') {
                            this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_ERR, this.prompt.MESSAGE.NET, this.button.OK);

                        } else {
                            this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.GEN_ERR, this.button.OK);

                        }

                    })
            });
        } else {
            this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);

        }
    }
}
