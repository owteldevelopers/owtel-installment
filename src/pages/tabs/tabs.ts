import { Component } from '@angular/core';
import { NavController, NavParams, Keyboard } from 'ionic-angular';
import { DocumentsPage } from "../documents/documents";
import { SchedulePage } from "../schedule/schedule";
import { PayPage } from "../pay/pay";
import { HomePage } from "../home/home";
import { CouponPage } from "../coupon/coupon";
import { PromotionPage } from "../promotion/promotion";
import { ReferralsPage } from "../referrals/referrals";
import { Storage } from '@ionic/storage';


/**
 * Generated class for the TabsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {

  showTabs : boolean = true;
  tab1Root = HomePage;
  tab2Root = CouponPage;
  tab3Root = PromotionPage;
  tab4Root = ReferralsPage;

  navParam: any;
  constructor(public navCtrl: NavController
    , public navParams: NavParams
    , private keyboard: Keyboard) {

    console.log(navParams.data);

  }

  ionViewDidEnter() {

    console.log(this.keyboard.isOpen());
    if (this.keyboard.isOpen()) {
      this.showTabs = false;
      console.log('tab is false');
    } else {
      this.showTabs = true;
      console.log('tab is open');
    }
  }

}
