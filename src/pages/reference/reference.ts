import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Refresher, Platform } from 'ionic-angular';
import { LoadingController, Loading, AlertController } from "ionic-angular/index";
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { Storage } from '@ionic/storage';
import { Network } from '@ionic-native/network';

import { ApplicationDetailsPage } from "../application-details/application-details";
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { Methods } from "../../globals/methods";
import { LoginPage } from "../login/login";
import { HomePage } from "../home/home";
import { DocumentsPage } from "../documents/documents";
import { TranslateService } from '@ngx-translate/core';
import * as $ from 'jquery';
/**
 * Generated class for the ReferencePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-reference',
  templateUrl: 'reference.html',
})
export class ReferencePage {
  @ViewChild('refIdfirst') refIdfirst;
  @ViewChild('refIdsec') refIdsec;
  @ViewChild('refHKID') refHKID;
  @ViewChild('refCustnumfirst') refCustnumfirst;
  @ViewChild('refCustnumsec') refCustnumsec;
  @ViewChild('refNamefirst') refNamefirst;
  @ViewChild('refNumberfirst') refNumberfirst;
  @ViewChild('refNamesec') refNamesec;
  @ViewChild('refNumbersec') refNumbersec;
  @ViewChild('refCustId') refCustId;


  loading: Loading;
  installment_details: Array<{ id: string, status: string, date: string, statusCode: number }>;
  userName : any;

  navParam : any;
  prompt: any;
  loader: any;
  button: any;
  getHKID: any;
  reference : any;
  getCustId: any;
  validations_form: FormGroup;
  retail: any;

  constructor(public navCtrl: NavController
    , public navParams: NavParams
    , public loadingCtrl: LoadingController
    , public alertCtrl: AlertController
    , private authService: AuthServiceProvider
    , public methods: Methods
    , public formBuilder: FormBuilder
    , private storage: Storage
    , private network: Network
    , private translate: TranslateService) {

    this.navParam = navParams.data;

    this.validations_form = this.formBuilder.group({
      refNamefirst: new FormControl(
        '', Validators.compose([
          Validators.required
        ])
      ), refNumberfirst: new FormControl(
        '', Validators.compose([
          Validators.required,
          Validators.minLength(8),
          Validators.pattern('^[0-9]+$')
        ])
      ),
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReferencePage');
    console.log(this.navParam.instId);
    this.retail = this.navParams.data.custId;
   

    this.translate.get('PROMPT').subscribe(
      translated => {
        this.prompt = translated;
      }
    );

    this.translate.get('LOADER').subscribe(
      translated => {
        this.loader = translated;
      }
    );

    this.translate.get('BUTTON').subscribe(
      translated => {
        this.button = translated;
      }
    );

    this.getReferences();

    
  }

  getReferences() {
    this.loading = this.loadingCtrl.create({
      content: '<ion-spinner >' + this.loader.LOAD + '</ion-spinner>'
    });

    if (this.network.type != 'none') {
      this.loading.present().then(() => {
        this.storage.get(this.authService.storageKey).then((hkid) => {
          this.authService.getReference(hkid).subscribe(
            resp => {

              var status = resp.status;
              var details_proof = resp.details;

              if (status == "success") {

                this.reference = resp.details;

              } else if (status == "unauthorized") { /** auto-logout */
                this.storage.clear();
                this.navCtrl.setRoot(LoginPage);

              } else {
                this.methods.showErrorOkButton(this.alertCtrl, '', resp.message, this.button.OK);

              }
              this.loading.dismiss();
            }, error => {
              console.log(error);
              this.loading.dismiss();

              if (error.name == 'TimeoutError') {
                this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_ERR, this.prompt.MESSAGE.NET, this.button.OK);

              } else if ((error.status >= 400) || (error.status == 500)) {  /** catch http request errors */
                this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.GEN_ERR, this.button.OK);

              } else {
                this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_INT, this.prompt.MESSAGE.NET, this.button.OK);

              }
            }
          );
        });
      });
    } else {

      this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);
    }
  }

  getRefName(firstname, lastname){
    if(firstname == '' || lastname == ''){
      return '';
    }else{
      return firstname + ' ' + lastname 
    }
    
  }

  createNewApplication() {
    if (this.network.type != 'none') {
      this.loading = this.methods.createLoader(this.loadingCtrl, `<ion-spinner >` + this.loader.LOAD + `</ion-spinner>`);
      this.loading.present().then(() => {
        this.storage.get(this.authService.storageKey).then((hkid) => {
          this.authService.createInstallment(hkid).subscribe(
            resp => {
              console.log(resp);

              this.loading.dismiss();
              var status = resp.status;

              if (status == "success") {
                this.storage.get(this.authService.storageKey).then((hkid) => {
                  this.navCtrl.setRoot(HomePage, {
                    hkid: hkid
                  });
                });

               

              } else if (status == "unauthorized") { /** auto-logout */
                this.storage.clear();
                this.navCtrl.setRoot(LoginPage);

              } else {
                this.methods.showErrorOkButton(this.alertCtrl, '', resp.message, this.button.OK);

              }
            }, error => {
              console.log(error);
              this.loading.dismiss();

              if (error.name == 'TimeoutError') {
                this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_ERR, this.prompt.MESSAGE.NET, this.button.OK);

              } else {
                this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.GEN_ERR, this.button.OK);

              }
            }
          );
        });
      });
    } else {
      this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);
    }
  }

  update() {

    let fid = this.refIdfirst.value;
    let sid = this.refIdsec.value;
    let fcustNum = this.refCustnumfirst.value;
    let scustNum = this.refCustnumsec.value;
    let fName = this.refNamefirst.value.trim();
    let fNum = this.refNumberfirst.value.trim();
    let sName = this.refNamesec.value.trim();
    let sNum = this.refNumbersec.value.trim();
    let hkid = this.refHKID.value;


    if (this.network.type != 'none') {
      this.loading = this.methods.createLoader(this.loadingCtrl, `<ion-spinner >` + this.loader.PLS_WAIT + `</ion-spinner>`);
      this.loading.present().then(() => {
        this.storage.get(this.authService.storageKey).then((hkid) => {
         
        if (this.checkerFirst(fName, fNum, sNum) == true)
        this.authService.getUpdateRef(fid, fcustNum, hkid, fName, fNum).subscribe(
          resp => {
            
            if (sid == undefined || scustNum == undefined || sid == ''){
              this.authService.getAddRef(fcustNum, hkid, sName, sNum).subscribe(
                  resp => {
                    this.showConfirmBack(fName, fNum, sName, sNum);
                    this.loading.dismiss();

                  }, error => {
                    console.log(error);
                    this.loading.dismiss();

                  });

              }else{
                this.authService.getUpdateRef(sid, scustNum, hkid, sName, sNum).subscribe(
                  resp => {

                 
                    this.showConfirmBack(fName, fNum, sName, sNum);
                    this.loading.dismiss();

                  }, error => {
                  
                    this.loading.dismiss();

                    if (error.name == 'TimeoutError') {
                      this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_ERR, this.prompt.MESSAGE.NET, this.button.OK);
                    }

                  });
               
              }

            
          }, error => {
            console.log(error);
            this.loading.dismiss();

            if (error.name == 'TimeoutError') {
              this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_ERR, this.prompt.MESSAGE.NET, this.button.OK);

            } 

          })

        
         });
      });
    } else {
      this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);
    }
  }

  checkerFirst(fname, fnum, snum){
    console.log(fnum.length);
    if ((fnum.length !== 0 && fnum.length < 8) || (snum.length !== 0 && snum.length < 8)){
      this.loading.dismiss();
      this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NUM_LENGTH, this.button.OK);
    
   /*  if (fname.length == 0 && fnum.length !== 0) {
      this.loading.dismiss();
      this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.REF_NAME_REQ, this.button.OK);
      
      return false;
    } else if (fname.length !== 0 && fnum.length == 0){
      this.loading.dismiss();
      this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.MOBILE_REQ, this.button.OK);
      
      return false;
    } else if (fname.length == 0 && fnum.length == 0) {
      this.loading.dismiss();
      this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.MOBILE_NAME_REQ, this.button.OK);
      
      return false; 
    /* } else  */
    } else if (isNaN(fnum) || isNaN(snum) ){
      this.loading.dismiss();
      this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.MOBILE_VALID, this.button.OK);
      return false;
    }else {
      return  true;
    }

    
  }

  
  showConfirmBack(fname,fnum,sname,snum) {
    if (fname.length == 0 && fnum.length == 0 && sname.length == 0 && snum.length == 0 ){

       let alert = this.alertCtrl.create({
        title: '',
        subTitle: this.prompt.MESSAGE.NO_REF,
        buttons: [
          {
            text: this.button.OK,
            handler: () => {
              this.navCtrl.setRoot(HomePage);
            }
          }
        ]
      });
      alert.present();
    

    }else {
      let alert = this.alertCtrl.create({
        title: '',
        subTitle: this.prompt.MESSAGE.SUCCESS,
        buttons: [
          {
            text: this.button.OK,
            handler: () => {
              this.navCtrl.setRoot(HomePage);
            }
          }
        ]
      });
      alert.present();
    }
  }

  

  showConfirmDialog() {
    
    let alert = this.alertCtrl.create({
      title: '',
      subTitle: this.prompt.MESSAGE.APPLY,
      buttons: [
        {
          text: this.button.CANCEL,
          handler: () => {
            alert.dismiss();
          }
        }, {
          text: this.button.CONFIRM,
          handler: () => {
            this.createNewApplication();
            
          }
        }
      ]
    });
    alert.present();
  }


  add() {

    let fName = this.refNamefirst.value.trim();
    let fNum = this.refNumberfirst.value.trim();
    let sName = this.refNamesec.value.trim();
    let sNum = this.refNumbersec.value.trim();
    let cust_num = this.refCustId.value;
   
    
    //let hkid = this.navParams.data.hkid;;


    if (this.network.type != 'none') {
      this.loading = this.methods.createLoader(this.loadingCtrl, `<ion-spinner >` + this.loader.PLS_WAIT + `</ion-spinner>`);
      this.loading.present().then(() => {
        this.storage.get(this.authService.storageKey).then((hkid) => {
         
          if (this.checkerFirst(fName, fNum, sNum) == true)
          this.authService.getAddRef(cust_num, hkid, fName, fNum).subscribe(
            resp => {
              this.authService.getAddRef(cust_num, hkid, sName, sNum).subscribe(
                  resp => {

                    
                    this.showConfirmBack(fName, fNum, sName, sNum);
            
                      
                    this.loading.dismiss();


                  }, error => {
                    console.log(error);
                    this.loading.dismiss();

                    if (error.name == 'TimeoutError') {
                      this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_ERR, this.prompt.MESSAGE.NET, this.button.OK);

                    }

                  });

            }, error => {
              console.log(error);
              this.loading.dismiss();

              if (error.name == 'TimeoutError') {
                this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_ERR, this.prompt.MESSAGE.NET, this.button.OK);

              }

            });

      
         });
      });
    } else {
      this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);
    }
  }

  getRetailCustIdData(hkid){
    this.authService.getRetailCustId(hkid).subscribe(
      resp => {
        status = resp.status;

        if (status == "success") {
          let details = resp.data;

          this.getCustId = details;

         } else {
          this.methods.showErrorOkButton(this.alertCtrl, '', resp.message, this.button.OK);

        }
      }
    );
  }

  removeWhiteSpace(number){
    return number.replace(/\s/g, "");
  }

  onKeyupNumber(e) {
    let elementChecker: string;
    elementChecker = e.target.value;


    let format = new RegExp('^[0-9]*$');



  
  }

  
}
