import { PromotionitemdetailsPage } from './../promotionitemdetails/promotionitemdetails';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { LoadingController, Loading, AlertController } from "ionic-angular/index";

import { Storage } from '@ionic/storage';
import { Network } from '@ionic-native/network';

import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { Methods } from "../../globals/methods";
import { LoginPage } from "../login/login";


/**
 * Generated class for the PromotionitemsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-promotionitems',
  templateUrl: 'promotionitems.html',
})
export class PromotionitemsPage {

  prompt: any;
  loader: any;
  button: any;
  loading: Loading;
  promoItemDetails: any;
  category_name : any;


  constructor(public navCtrl: NavController
    , public loadingCtrl: LoadingController
    , public alertCtrl: AlertController
    , private authService: AuthServiceProvider
    , public methods: Methods
    , private storage: Storage
    , private network: Network
    , private translate: TranslateService
    , public navParams: NavParams) {
  }


  ionViewDidEnter() {
    console.log('ionViewDidLoad PromotionPage');
    this.translate.get('PROMPT').subscribe(
      translated => {
        this.prompt = translated;
      }
    );

    this.translate.get('LOADER').subscribe(
      translated => {
        this.loader = translated;
      }
    );

    this.translate.get('BUTTON').subscribe(
      translated => {
        this.button = translated;
      }
    );

    this.getPromotionItem();
  }


  getPromotionItem() {
    if (this.network.type != 'none') {
      this.loading = this.methods.createLoader(this.loadingCtrl, `<ion-spinner >` + this.loader.LOAD + `</ion-spinner>`);
      this.loading.present().then(() => {
        this.getPromotionItemRequest();
      });
    } else {
      this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);
    }
  }

  getPromotionItemRequest(refresher?) {
    
    let category_id = this.navParams.data.category_id;

   
    this.authService.getPromotionItem(category_id).subscribe(
      resp => {
        status = resp.status;

        if (refresher != null) refresher.complete();
        

        if (status == "success") {
          let details = resp.data;

          console.log(details);

          this.promoItemDetails = details;

          details.forEach((v, i) => {
            this.category_name = v.category_name;
           

          });
          
          

        } else if (status == "unauthorized") {
          this.storage.clear();
          this.navCtrl.setRoot(LoginPage);

        } else {
          this.methods.showErrorOkButton(this.alertCtrl, '', resp.message, this.button.OK);

        }

        this.loading.dismiss();
      }, error => {
        console.log(error);

        if (refresher != null) refresher.complete();
        this.loading.dismiss();

        if (error.name == 'TimeoutError') {
          this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_ERR, this.prompt.MESSAGE.NET, this.button.OK);

        } else {
          this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.GEN_ERR, this.button.OK);

        }
      }
    );

  }

  getImageDetails(code, id){
    this.navCtrl.push(PromotionitemdetailsPage, {
      product_code: code,
      id : id
    });
  }

  getDiscount(amount){
   
    var concat_string = amount.split("$").pop();
    var get_discount = Math.round(parseInt(concat_string.replace(',','')) - (parseInt(concat_string.replace(',','')) * 0.2));
    
   return 'HK$' + get_discount.toString();

   
  }

  addParagraph(name){
    
    if(name.length < 23){
      return name + '\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0';
    }else{
      return name;
    }
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    if (this.network.type != 'none') {
      this.getPromotionItemRequest(refresher);
    } else {
      refresher.complete();
      this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);

    }
  }
  
}
