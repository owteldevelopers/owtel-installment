
import { QrcouponPage } from './../qrcoupon/qrcoupon';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { LoadingController, Loading, AlertController } from "ionic-angular/index";

import { Storage } from '@ionic/storage';
import { Network } from '@ionic-native/network';

import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { Methods } from "../../globals/methods";
import { LoginPage } from "../login/login";


/**
 * Generated class for the CouponPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-coupon',
  templateUrl: 'coupon.html',
})
export class CouponPage {

  prompt : any;
  loader: any;
  button : any;
  loading: Loading;
  couponDetails : any;
  userName : any;
  active : number = 1;

  constructor(public navCtrl: NavController
    , public loadingCtrl: LoadingController
    , public alertCtrl: AlertController
    , private authService: AuthServiceProvider
    , public methods: Methods
    , private storage: Storage
    , private network: Network
    , private translate: TranslateService
    , public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Coupon');

    this.translate.get('PROMPT').subscribe(
      translated => {
        this.prompt = translated;
      }
    );

    this.translate.get('LOADER').subscribe(
      translated => {
        this.loader = translated;
      }
    );

    this.translate.get('BUTTON').subscribe(
      translated => {
        this.button = translated;
      }
    );

    this.getCoupon();

  }



  getCoupon() {
    console.log(this.network.type);
    if (this.network.type != 'none') {
      this.loading = this.methods.createLoader(this.loadingCtrl, `<ion-spinner >` + this.loader.LOAD + `</ion-spinner>`);
      this.loading.present().then(() => {
        this.getCouponRequest();
      });
    } else {
      this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);
    }
  }

  getCouponQr(id) {

    this.active = id;

    this.navCtrl.push(QrcouponPage, {
      coupon_id: id
    });

  }

  getCouponRequest(refresher?) {

    this.storage.get(this.authService.storageKey).then((hkid) => {
      this.authService.getCouponDetails(hkid).subscribe(
        resp => {

          console.log(resp);


          if (refresher != null) {
            refresher.complete();
          }


          var status = resp.status;
          var getname = resp.name;

          let details = resp.data;

          this.couponDetails = details;
          this.userName = getname;

          if (status == "success") {
            console.log('pumasok d2');
            let details = resp.data;

            this.couponDetails = details;
            this.userName = getname;


          } else if (status == "unauthorized") {
            this.storage.clear();
            this.navCtrl.setRoot(LoginPage);

          } else {
            this.methods.showErrorOkButton(this.alertCtrl, '', resp.message, this.button.OK);

          }

          console.log('di sya pumasok tlaga');

          this.loading.dismiss();
        }, error => {
          console.log(error);

          if (refresher != null) refresher.complete();
          this.loading.dismiss();

          if (error.name == 'TimeoutError') {
            this.methods.showErrorOkButton(this.alertCtrl, this.prompt.TITLE.NET_ERR, this.prompt.MESSAGE.NET, this.button.OK);

          } else {
            this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.GEN_ERR, this.button.OK);

          }
        }
      );
    });

  }

  getFormatDate(date){
    var d = new Date(date);

    var day = (d.getDate() < 10 ? '0' : '') + d.getDate();
    // 01, 02, 03, ... 10, 11, 12
    var month = ((d.getMonth() + 1) < 10 ? '0' : '') + (d.getMonth() + 1);
    //Months are zero based
    var curr_year = d.getFullYear();

    var year =  curr_year.toString().substr(-2);

    return day + "/" + month + "/" + curr_year;

  }

  getDiscountType(discount_type, amount){
    var amount_length = amount.toString().length;
    if(discount_type == 1){
      if(amount_length == 1){
        return '\xa0\xa0$' + amount;
      }else if (amount_length == 2){
        return '\xa0$' + amount;
      }else {
        return '$' + amount;
      }

    }else{
      if(amount_length == 1){
        return '\xa0\xa0' +amount + '%';
      }else if (amount_length == 2){
        return '\xa0' +amount + '%';
      }else {
       return  +amount + '%';
      }

    }
  }

  doRefresh(refresher) {

    if (this.network.type != 'none') {
      this.getCouponRequest(refresher);
      console.log('pumasok');
    } else {
      refresher.complete();
      this.methods.showErrorOkButton(this.alertCtrl, '', this.prompt.MESSAGE.NET_NO, this.button.OK);

    }
  }

  getDetailsRef(refName) {
    console.log(refName.length);
    if (refName.length >= 2) {
      return 'Referred : ' + refName;
    }
  }

}
