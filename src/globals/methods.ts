import { Component, ViewChild  } from '@angular/core';
import * as $ from 'jquery';

export class Methods {
    constructor() { }

    // createToast(toastCtrl, type, message, position) {
    //     let toast;
    //     switch (type) {
    //         case 1:
    //             toast = toastCtrl.create({
    //                 message: message,
    //                 duration: 2000,
    //                 position: position
    //             });
    //             break;
    //         case 2:
    //             toast = toastCtrl.create({
    //                 message: message,
    //                 position: position,
    //                 showCloseButton: true,
    //                 closeButtonText: 'Ok'
    //             });
    //             break;
    //     }

    //     toast.onDidDismiss(() => {
    //         console.log('Dismissed toast');
    //     });

    //     toast.present();
    // }

    createLoader(loadingCtrl, message) {
        let loading = loadingCtrl.create({
            content: message,
            dismissOnPageChange: true
        });
        loading.present;
        return loading;
    }

  loadDuration(loadingCtrl, message) {
    let loading = loadingCtrl.create({
      content: message,
      duration: 7000
    });
    loading.present;
    return loading;
  }

    createPromotion(loadingCtrl, message) {
        let loading = loadingCtrl.create({
            content: message,
            duration: 3000
        });
        loading.present;
        return loading;
    }

    createCoupon(loadingCtrl, message) {
        let loading = loadingCtrl.create({
            content: message,
            duration: 1000
        });
        loading.present;
        return loading;
    }

    showErrorOkButton(alertCtrl, title, subtitle, buttonOk) {
        let alert = alertCtrl.create({
            title: title,
            subTitle: subtitle,
            buttons:[buttonOk]
        });
        alert.present();
    }

    showIcon(params, htmlVariable){

        if(params == 'success'){
         htmlVariable = `aaaaaaaaaaaaa`;
        }else{
          htmlVariable = `bbbbbbb`;
        }



        return htmlVariable;

    }
    // showNetworkErrorOkButton(alertCtrl) {
    //     let alert = alertCtrl.create({
    //         title: 'Network Error',
    //         subTitle: 'Please check your connection and try again.',
    //         buttons:['OK']
    //     });
    //     alert.present();
    // }

    // showNetworkInterruptedOkButton(alertCtrl) {
    //     let alert = alertCtrl.create({
    //         title: 'Network Interrupted',
    //         subTitle: 'Please check your connection and try again.',
    //         buttons:['OK']
    //     });
    //     alert.present();
    // }

    // showGeneralErrorOkButton(alertCtrl) {
    //     let alert = alertCtrl.create({
    //         title: '',
    //         subTitle: 'Something went wrong. Please try again.',
    //         buttons:['OK']
    //     });
    //     alert.present();
    // }

    // showInternetError(alertCtrl) {
    //     let alert = alertCtrl.create({
    //         title: '',
    //         subTitle: 'No internet connection',
    //         buttons:['OK']
    //     });
    //     alert.present();
    // }

    masknumber(number) {
        var lastChars = number.substr(number.length - 3);
        return '*'.repeat(number.length - 3) + lastChars;
    }



    getUploadSuccess(galleryId){
        var getSuccess = "";

        getSuccess += $('.getFrontImage_' + galleryId).remove();
        getSuccess += $('.getPrompt_' + galleryId).show();
        getSuccess += $('.getPrompt_' + galleryId).empty().append('<img name = "" src = "assets/icon/cloud-upload.png" class = "prompt-icon"/>');


        return getSuccess;

    }

     getUploadFailed(galleryId){
        /*  return $('.getPrompt_' + galleryId).empty().append('<img src = "assets/icon/retry.png" class = "prompt-retry" (click)="showDialog(' + galleryId +')" on-Tap="showDialog('+galleryId+')" /><span class = "prompt-retry-span"  style = "">Retry</span><img  name = "" src = "assets/icon/red.png" class = "prompt-icon-failed">'); */

        var getFailed = "";

         getFailed += $('.getFrontImage_' + galleryId).remove();
         getFailed += $('.getPrompt_' + galleryId).show()
         getFailed += $('.getPrompt_'+ galleryId).empty().append('<img name = "" src = "assets/icon/red.png" class = "prompt-icon-failed"/>');


         return getFailed;

    }








}
