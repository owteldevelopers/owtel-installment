var Methods = /** @class */ (function () {
    function Methods() {
    }
    // createToast(toastCtrl, type, message, position) {
    //     let toast;
    //     switch (type) {
    //         case 1:
    //             toast = toastCtrl.create({
    //                 message: message,
    //                 duration: 2000,
    //                 position: position
    //             });
    //             break;
    //         case 2:
    //             toast = toastCtrl.create({
    //                 message: message,
    //                 position: position,
    //                 showCloseButton: true,
    //                 closeButtonText: 'Ok'
    //             });
    //             break;
    //     }
    //     toast.onDidDismiss(() => {
    //         console.log('Dismissed toast');
    //     });
    //     toast.present();
    // }
    Methods.prototype.createLoader = function (loadingCtrl, message) {
        var loading = loadingCtrl.create({
            content: message,
            dismissOnPageChange: true
        });
        loading.present;
        return loading;
    };
    Methods.prototype.showErrorOkButton = function (alertCtrl, title, subtitle, buttonOk) {
        var alert = alertCtrl.create({
            title: title,
            subTitle: subtitle,
            buttons: [buttonOk]
        });
        alert.present();
    };
    Methods.prototype.showIcon = function (params, htmlVariable) {
        if (params == 'success') {
            htmlVariable = "aaaaaaaaaaaaa";
        }
        else {
            htmlVariable = "bbbbbbb";
        }
        return htmlVariable;
    };
    // showNetworkErrorOkButton(alertCtrl) {
    //     let alert = alertCtrl.create({
    //         title: 'Network Error',
    //         subTitle: 'Please check your connection and try again.',
    //         buttons:['OK']
    //     });
    //     alert.present();
    // }
    // showNetworkInterruptedOkButton(alertCtrl) {
    //     let alert = alertCtrl.create({
    //         title: 'Network Interrupted',
    //         subTitle: 'Please check your connection and try again.',
    //         buttons:['OK']
    //     });
    //     alert.present();
    // }
    // showGeneralErrorOkButton(alertCtrl) {
    //     let alert = alertCtrl.create({
    //         title: '',
    //         subTitle: 'Something went wrong. Please try again.',
    //         buttons:['OK']
    //     });
    //     alert.present();
    // }
    // showInternetError(alertCtrl) {
    //     let alert = alertCtrl.create({
    //         title: '',
    //         subTitle: 'No internet connection',
    //         buttons:['OK']
    //     });
    //     alert.present();
    // }
    Methods.prototype.masknumber = function (number) {
        var lastChars = number.substr(number.length - 3);
        return '*'.repeat(number.length - 3) + lastChars;
    };
    return Methods;
}());
export { Methods };
//# sourceMappingURL=methods.js.map