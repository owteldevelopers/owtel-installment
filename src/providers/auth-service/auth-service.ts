import { Http, RequestOptions, Headers, Response } from '@angular/http';
import { Injectable } from '@angular/core';

import { Storage } from '@ionic/storage';
import { Device } from '@ionic-native/device';

import { TranslateService } from '@ngx-translate/core';

import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/timeout'
import { MyApp } from '../../app/app.component';
import { CacheService } from "ionic-cache";


//test
/* let apiURL = "http://test.owtelappinstallment.owtel.com/api/v1";
let apiURLCoupon = "http://test.apiowtelstorecrm.owtel.com/api/owtelcrm"; */
let apiCallVerification = "https://api.checkmobi.com/v1/call";
//let apiVerificationCall = "http://api.owtelstorecrm.owtel.com/api/owtelcrm";

//live
let apiURLDocs = "http://owtelappinstallment.owtel.com/api/v1";
let apiURLCoupon = "http://api.owtelstorecrm.owtel.com/api/owtelcrm";
let apiURL = "http://owtelappinstallment.owtel.com/api/v1";

let apiURLNotif = "http://test.apiowtelstorecrm.owtel.com/api/v1";

const apiKey = '4309F868-089C-4D43-9E63-8A84370DE463';





@Injectable()
export class AuthServiceProvider {
  uuid: any;
  deviceLang: any;
  imageCache: Observable<any>;
  public imageCacheKey: string = 'image-cache-key';

  public storageKey: string = "HKID";
  public reqType1: String = "docs";
  public reqType2: String = "sched";
  public reqType3: String = "pay";

  constructor(public http: Http
    , private device: Device
    , public cache: CacheService
    , private storage: Storage
    , private translate: TranslateService) {
    console.log('Hello AuthServiceProvider Provider');
    this.deviceLang = this.translate.getBrowserLang();
    console.log("Lang: " + this.deviceLang);

    this.http = http;
  }

  /**
   * Calling API for verifying HKID and mobile number
   *
   * @param id: customer's HKID
   * @param number: customer's mobile number
   * @param req_num: 1 for normal request; 2 to bypass deviceID validation
   *
   * @returns {any}
   */
  public verifyID(id, number, req_num) {
    let params = {
      hkid: id,
      mobile: number,
      deviceid: this.device.uuid,
      req_num: req_num,
      locale: this.deviceLang
    }

    console.log('hkid:' + id +
      'mobile:' + number +
      'deviceid:' + this.device.uuid +
      'req_num:' +  req_num +
     'locale:' + this.deviceLang);

    return this.http.post(apiURL + "/verify_id", params)
      .timeout(60000)
      .map((res: Response) => res.json());
  }

  public apply(fName, lName, id, number, num) {
    let params = {
      firstName: fName,
      lastName: lName,
      hkid: id,
      mobile: number,
      deviceid: this.device.uuid,
      req_num: num,
      locale: this.deviceLang
    }

    console.log(params);

    return this.http.post(apiURL + "/new_application", params)
      .timeout(60000)
      .map((res: Response) => res.json());
  }



  /**
   * Calling API for verifying the verification code sent
   *
   * @param id: customer's HKID
   * @param number: customer's mobile number
   * @param code: verification code
   *
   * @returns {any}
   */
  public verifyMobile(id, number, code) {
    let params = {
      hkid: id,
      mobile: number,
      deviceid: this.device.uuid,
      code: code,
      locale: this.deviceLang
    }

    console.log('hkid:' + id +
    'mobile:' + number +
    'deviceid:' + this.device.uuid +
    'code:' + code +
    'locale:' + this.deviceLang);


    return this.http.post(apiURL + "/verify_mobile", params)
      .timeout(60000)
      .map((res: Response) => res.json());
  }

  public createInstallment(id) {
    let params = {
      hkid: id,
      deviceid: this.device.uuid,
      locale: this.deviceLang
    }
    console.log(params);

    return this.http.post(apiURL + "/create_installment", params)
      .timeout(60000)
      .map((res: Response) => res.json());
  }

  /**
   * Calling API for retrieving customer's installments list
   *
   * @param id: customer's HKID
   *
   * @returns {any}
   */
  public getInstallments(id) {
    let params = "?hkid=" + id + "&deviceid=" + this.device.uuid + "&locale=" + this.deviceLang;
    return this.http.get(apiURL + "/get_installments" + params)
      .timeout(60000)
      .map(res => res.json());
  }

  /**
   * get customer's installment details
   *
   * @param id: customer's HKID
   * @param instId: customer's installment ID
   * @param type: docs for documents; sched for payment schedule; pay for qr code page
   *
   * @returns {any}
   */
  public getInstallmentDetails(id, instId, type) {
    let params = "?hkid=" + id
      + "&deviceid=" + this.device.uuid
      + "&installmentID=" + instId
      + "&locale=" + this.deviceLang;
    console.log(params);

    switch (type) {
      case this.reqType1:
        let url = apiURL + "/get_documents" + params;
        let delayType = 'all';
        let ttl = 0.1;
        let req = this.http.get(apiURL + "/get_documents" + params)
          .timeout(300000)
          .map(res => res.json());



        /* this.imageCache = this.cache.loadFromObservable(url, req, this.imageCacheKey);

        console.log(req); */

        return req;


        /* if(this.imageCache != req){
          this.cache.clearGroup(this.imageCacheKey);
          return req;
        }else{
          return this.imageCache;
        } */

      case this.reqType2:
        return this.http.get(apiURL + "/get_schedule" + params)
          .timeout(60000)
          .map(res => res.json());



      case this.reqType3:
        return this.http.get(apiURL + "/get_qrcode" + params)
          .timeout(60000)
          .map(res => res.json());


    }


  }

  /**
   * Calling API for uploading pictures, saving it to DB and to Dropbox
   *
   * @param id: customer's HKID
   * @param instId: customer's installment ID
   * @param uploads: array of images
   *
   * @returns {any}
     */
  public uploadDocuments(id, instId, uploads) {

    let params = {
      hkid: id,
      deviceid: this.device.uuid,
      installmentID: instId,
      uploads: uploads,
      locale: this.deviceLang
    }


    return this.http.post(apiURL + "/update_documents", params)
      .timeout(300000)
      .map((res: Response) => res.json());

  }


    /**
  * Calling API for retrieving customer's installments list
  *
  * @param id: customer's HKID
  *
  * @returns {any}
  */
  public getPushNotification() {

    return this.http.get(apiURLNotif + "/get/notifications")
      .timeout(60000)
      .map(res => res.json());
  }

  public getCouponDetails(id) {
    console.log(id);
    let params = "?hkid=" + id + "&locale=" + this.deviceLang;
    return this.http.get(apiURLCoupon + "/get_coupon_app" + params)
      .timeout(60000)
      .map(res => res.json());
  }

  public getQRCode(id, hkid) {
    console.log(id);
    let params = "?couponid=" + id + "&hkid=" + hkid + "&locale=" + this.deviceLang;
    return this.http.get(apiURLCoupon + "/get_coupon_qr" + params)
      .timeout(60000)
      .map(res => res.json());
  }

  public getPromotion() {

    let params = "?locale=" + this.deviceLang;
    return this.http.get(apiURLCoupon + "/get_promotioncategory_app" + params)
      .timeout(60000)
      .map(res => res.json());
  }

  public getPromotionItem(id) {

    let params = "?categoryid=" + id + "&locale=" + this.deviceLang;
    return this.http.get(apiURLCoupon + "/get_promotioncategory_byid" + params)
      .timeout(60000)
      .map(res => res.json());
  }

  public getPromotionItemDetails(code, id) {

    let params = "?parentcode=" + code + "&productid=" + id + "&locale=" + this.deviceLang;
    return this.http.get(apiURLCoupon + "/get_product_image" + params)
      .timeout(60000)
      .map(res => res.json());
  }

 public getReference(id) {

    let params = "?HKID=" + id + "&locale=" + this.deviceLang;
    return this.http.get(apiURLCoupon + "/get_ref_cust_address" + params)
      .timeout(60000)
      .map(res => res.json());
  }

  public getUpdateRef(id, custNum, hkid, firstname, number ) {
    let params = {
      id : id,
      CustNo: custNum,
      HKID: hkid,
      firstname: firstname,
      PRNContactNo: number,
      userupdated: 'owtelappinstallment',
      deviceid: this.device.uuid,

      locale: this.deviceLang
    }


    return this.http.post(apiURLCoupon + "/update_ref_cust_address", params)
      .timeout(300000)
      .map((res: Response) => res.json());

  }

  public getAddRef(cust_num, hkid, firstname, number) {
    let params = {
      HKID: hkid,
      userupdated: 'owtelappinstallment',
      CustNo: cust_num,
      deviceid: this.device.uuid,
      firstname: firstname,
      PRNContactNo: number,
      locale: this.deviceLang
    }


    return this.http.post(apiURLCoupon + "/add_ref_cust_address", params)
      .timeout(300000)
      .map((res: Response) => res.json());

  }

  public getRetailCustId(id){
    let params = "?hkid=" + id + "&locale=" + this.deviceLang;
    return this.http.get(apiURLCoupon + "/get_retail_cust_no" + params)
      .timeout(60000)
      .map(res => res.json());
  }

  public getUnlistDocs(hkid, instId){
    let params = "?hkid=" + hkid + "&installmentID=" + instId + "&locale=" + this.deviceLang + "&deviceid=" + this.device.uuid;
    return this.http.get(apiURLDocs + "/get_optional_documents" + params)
      .timeout(60000)
      .map(res => res.json());
  }

  public getUpdateUnlistDocs(instId, columnName, id, value) {
    let params = {
      installmentID: instId,

      id: id,
      column: columnName,
      value: value,
      deviceid: this.device.uuid,
      locale: this.deviceLang
    }

    return this.http.post(apiURLDocs + "/alter_documentTemplate", params)
      .timeout(300000)
      .map((res: Response) => res.json());

  }


/*   filterItems(searchTerm) {

    return this.items.filter((item) => {
      return item.name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
    });

  } */

  // get_referral
  public getReferral(hkid) {

    let params = "?hkid=" + hkid + "&locale=" + this.deviceLang;
    return this.http.get(apiURL + "/get_referral" + params)
      .timeout(60000)
      .map(res => res.json());
  }

  getReferralByName(hkid, name) {
    let params = "?hkid=" + hkid + "&name=" + name + "&locale=" + this.deviceLang;
    return this.http.get(apiURL + "/get_referral" + params)
      .timeout(60000)
      .map(res => res.json());
  }



  public getReferralById(custid) {

    let params = "?refcustid=" + custid + "&locale=" + this.deviceLang;
    return this.http.get(apiURL + "/get_referral_byid" + params)
      .timeout(60000)
      .map(res => res.json());
  }


  public addReferral(hkid, name, mobile) {

    let params = "?hkid=" + hkid + "&name=" + name + "&phone=" + mobile;

    return this.http.get(apiURL + "/add_referral" + params)
      .timeout(300000)
      .map((res: Response) => res.json());

  }
  public deleteReferral(custid) {
    let params = "?refcustid=" + custid + "&locale=" + this.deviceLang;

    return this.http.get(apiURL + "/delete_referral" + params)
      .timeout(300000)
      .map((res: Response) => res.json());

  }

  public updateReferral(refcustid, name, remarks, phone) {
    let params = {
      refcustid: refcustid,
      name: name,
      remarks: remarks,
      phone: phone,
      locale: this.deviceLang
    }

    console.log(params);

    return this.http.post(apiURL + "/edit_referral", params)
      .timeout(300000)
      .map((res: Response) => res.json());


  }


  public addBatch(params) {
    console.log(params);

    return this.http.post(apiURL + "/add_referral_batch", params)
      .timeout(300000)
      .map((res: Response) => res.json());

  }

  public verifyMobileCall(number, code) {
    let headers = new Headers();
    headers.append('Authorization', apiKey);
    headers.append('Content-Type', 'application/json;charset=UTF-8');
    headers.append('Accept', 'application/json');
    let options = new RequestOptions({
      headers: headers
    });
    let convertString = code.toString();
    let firstNum = convertString.substring(0, 1);
    let secNum = convertString.substring(2, 1);
    let thirdNum = convertString.substring(3, 2);
    let fourthNum = convertString.substring(4, 3);
    let fifthNum = convertString.substring(5, 4);
    let params = {
      to: number,
      /* to: '+639 959678662', */
      events: [
        {
          "action": "speak",
          "text": "Your OWTEL Installment verification code is " + firstNum + " " + secNum + " " + thirdNum + " " + fourthNum + " " + fifthNum + " ",
          "loop": 2,
          "language": "en-US",
          "voice": "WOMAN"
        }
      ]
    }
    return this.http.post(apiCallVerification, params, options)
      .timeout(60000)
      .map((res: Response) => res.json());
  }

  public verificationCode(number) {
    //http://test.apiowtelstorecrmsg.owtel.com/api/owtelcrm/get_verification_code?mobile_number=64522664

    let params = "?mobile_number=" + number;
    return this.http.get(apiURLCoupon + "/get_verification_code" + params)
      .timeout(60000)
      .map(res => res.json());

  }


}
