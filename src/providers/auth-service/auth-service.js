var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Device } from '@ionic-native/device';
import { TranslateService } from '@ngx-translate/core';
import 'rxjs/Rx';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/timeout';
var apiURL = "http://owtelappinstallment.owtel.com/api/v1";
var AuthServiceProvider = /** @class */ (function () {
    function AuthServiceProvider(http, device, storage, translate) {
        this.http = http;
        this.device = device;
        this.storage = storage;
        this.translate = translate;
        this.storageKey = "HKID";
        this.reqType1 = "docs";
        this.reqType2 = "sched";
        this.reqType3 = "pay";
        console.log('Hello AuthServiceProvider Provider');
        this.deviceLang = this.translate.getBrowserLang();
        console.log("Lang: " + this.deviceLang);
        this.http = http;
    }
    /**
     * Calling API for verifying HKID and mobile number
     *
     * @param id: customer's HKID
     * @param number: customer's mobile number
     * @param req_num: 1 for normal request; 2 to bypass deviceID validation
     *
     * @returns {any}
     */
    AuthServiceProvider.prototype.verifyID = function (id, number, req_num) {
        var params = {
            hkid: id,
            mobile: number,
            deviceid: this.device.uuid,
            req_num: req_num,
            locale: this.deviceLang
        };
        return this.http.post(apiURL + "/verify_id", params)
            .timeout(60000)
            .map(function (res) { return res.json(); });
    };
    AuthServiceProvider.prototype.apply = function (fName, lName, id, number, num) {
        var params = {
            firstName: fName,
            lastName: lName,
            hkid: id,
            mobile: number,
            deviceid: this.device.uuid,
            req_num: num,
            locale: this.deviceLang
        };
        console.log(params);
        return this.http.post(apiURL + "/new_application", params)
            .timeout(60000)
            .map(function (res) { return res.json(); });
    };
    /**
     * Calling API for verifying the verification code sent
     *
     * @param id: customer's HKID
     * @param number: customer's mobile number
     * @param code: verification code
     *
     * @returns {any}
     */
    AuthServiceProvider.prototype.verifyMobile = function (id, number, code) {
        var params = {
            hkid: id,
            mobile: number,
            deviceid: this.device.uuid,
            code: code,
            locale: this.deviceLang
        };
        return this.http.post(apiURL + "/verify_mobile", params)
            .timeout(60000)
            .map(function (res) { return res.json(); });
    };
    AuthServiceProvider.prototype.createInstallment = function (id) {
        var params = {
            hkid: id,
            deviceid: this.device.uuid,
            locale: this.deviceLang
        };
        console.log(params);
        return this.http.post(apiURL + "/create_installment", params)
            .timeout(60000)
            .map(function (res) { return res.json(); });
    };
    /**
     * Calling API for retrieving customer's installments list
     *
     * @param id: customer's HKID
     *
     * @returns {any}
     */
    AuthServiceProvider.prototype.getInstallments = function (id) {
        var params = "?hkid=" + id + "&deviceid=" + this.device.uuid + "&locale=" + this.deviceLang;
        return this.http.get(apiURL + "/get_installments" + params)
            .timeout(60000)
            .map(function (res) { return res.json(); });
    };
    /**
     * get customer's installment details
     *
     * @param id: customer's HKID
     * @param instId: customer's installment ID
     * @param type: docs for documents; sched for payment schedule; pay for qr code page
     *
     * @returns {any}
     */
    AuthServiceProvider.prototype.getInstallmentDetails = function (id, instId, type) {
        var params = "?hkid=" + id
            + "&deviceid=" + this.device.uuid
            + "&installmentID=" + instId
            + "&locale=" + this.deviceLang;
        console.log(params);
        switch (type) {
            case this.reqType1:
                return this.http.get(apiURL + "/get_documents" + params)
                    .timeout(300000)
                    .map(function (res) { return res.json(); });
            case this.reqType2:
                return this.http.get(apiURL + "/get_schedule" + params)
                    .timeout(60000)
                    .map(function (res) { return res.json(); });
            case this.reqType3:
                return this.http.get(apiURL + "/get_qrcode" + params)
                    .timeout(60000)
                    .map(function (res) { return res.json(); });
        }
    };
    /**
     * Calling API for uploading pictures, saving it to DB and to Dropbox
     *
     * @param id: customer's HKID
     * @param instId: customer's installment ID
     * @param uploads: array of images
     *
     * @returns {any}
       */
    AuthServiceProvider.prototype.uploadDocuments = function (id, instId, uploads) {
        var params = {
            hkid: id,
            deviceid: this.device.uuid,
            installmentID: instId,
            uploads: uploads,
            locale: this.deviceLang
        };
        return this.http.post(apiURL + "/update_documents", params)
            .timeout(300000)
            .map(function (res) { return res.json(); });
    };
    AuthServiceProvider = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [Http,
            Device,
            Storage,
            TranslateService])
    ], AuthServiceProvider);
    return AuthServiceProvider;
}());
export { AuthServiceProvider };
//# sourceMappingURL=auth-service.js.map