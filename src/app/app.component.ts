import { TutorialPage } from './../pages/tutorial/tutorial';
import { Component } from '@angular/core';
import { Platform, AlertController, Events, Keyboard  } from 'ionic-angular';

import { Storage } from '@ionic/storage';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { CouponPage } from "../pages/coupon/coupon";
import { QrcouponPage } from "../pages/qrcoupon/qrcoupon";

import { LoginPage } from "../pages/login/login";


import { DocumentsPage } from "../pages/documents/documents";
import { SchedulePage } from "../pages/schedule/schedule";
import { PayPage } from "../pages/pay/pay";

import { MobileNumberVerificationPage } from "../pages/mobile-number-verification/mobile-number-verification";
import { ApplicationDetailsPage } from "../pages/application-details/application-details";
import { MobileNumberMatchingPage } from "../pages/mobile-number-matching/mobile-number-matching";
import { ApplyPage } from "../pages/apply/apply";
import { TranslateService } from '@ngx-translate/core';
import { AuthServiceProvider } from "../providers/auth-service/auth-service";
import { CacheService } from "ionic-cache";
import { Network } from '@ionic-native/network';
import { NetworkProvider } from '../providers/network/network';

import { Methods } from "../globals/methods";
import { Push, PushObject, PushOptions } from '@ionic-native/push';//version 3.native
import { LocalNotifications } from '@ionic-native/local-notifications';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any;
  public locale: any;
  pages: Array<{title: string, component: any}>;
  message_id : any;
  title : any;
  message: any;
  image : any;
  status : any;
  schedule : any;
  redirect : any;
  showTabs : any;

  prompt: any;
  loader: any;
  button: any;

  constructor(public cache: CacheService,
    public localNotifications: LocalNotifications,
    public authService: AuthServiceProvider,
    private push: Push,
    public alertCtrl: AlertController,
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    private storage: Storage,
    private translate: TranslateService,
    private network: Network,
    public networkProvider: NetworkProvider,
    public events: Events,
    public methods: Methods,
    public keyboard: Keyboard) {

    this.initializeApp(translate);
    this.cache.setDefaultTTL(60 * 60); //set default cache TTL for 1 hour
    this.cache.setOfflineInvalidate(false);

      this.storage.get('HKID').then((val) => {
        if (val != null && val != undefined) {
          this.rootPage = TabsPage;
          /* this.rootPage = HomePage; */
        } else {
          this.rootPage = LoginPage;
          /* this.storage.get('loginInit').then((login) => {
            if (login != null && login != undefined) {
             this.rootPage = LoginPage;
            }else{
              this.rootPage = TutorialPage;
            }
          }); */
        }
      });
  }

  initializeApp(translate) {
      translate.setDefaultLang('en');

        this.networkProvider.initializeNetworkEvents();

        // Offline event
        this.events.subscribe('network:offline', () => {
          //alert('network:offline ==> ' + this.network.type);
        });

        // Online event
        this.events.subscribe('network:online', () => {
          //this.methods.showErrorOkButton(this.alertCtrl, '', 'Pull to Refresh', 'OK');
        });

      this.locale = translate.getBrowserLang();
      console.log("Lang: " + this.locale);
      translate.use(this.locale.match(/en|id/) ? this.locale : 'en');

  }

  ionViewWillEnter() {
    this.translate.setDefaultLang('en');

    this.locale = this.translate.getBrowserLang();
    console.log("Lang: " + this.locale);
    this.translate.use(this.locale.match(/en|id/) ? this.locale : 'en');

  }

  keyboardCheck() {
    return this.keyboard.isOpen();
  }

 /*  ionViewDidLoad() {

    console.log(this.keyboard.isOpen());
    if (this.keyboard.isOpen()) {
      this.showTabs = false;
      console.log('tab is false');
    } else {
      this.showTabs = false;
      console.log('tab is open');
    }
  } */

}

