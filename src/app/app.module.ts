import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { HttpModule, Http } from '@angular/http';
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { IonicStorageModule } from '@ionic/storage';
import { IonicImageViewerModule } from 'ionic-img-viewer';

import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Camera } from '@ionic-native/camera';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Device } from '@ionic-native/device';
import { Network } from '@ionic-native/network';
import { Contacts, ContactFieldType, IContactFindOptions } from "@ionic-native/contacts"


import { MyApp } from './app.component';
import { Methods } from "../globals/methods";
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { HomePage } from '../pages/home/home';
import { LoginPage } from "../pages/login/login";
import { MobileNumberVerificationPage } from "../pages/mobile-number-verification/mobile-number-verification";
import { MobileNumberSuccessPage } from "../pages/mobile-number-success/mobile-number-success";
import { ApplicationDetailsPage } from "../pages/application-details/application-details";
import { DocumentsPage } from "../pages/documents/documents";
import { SchedulePage } from "../pages/schedule/schedule";
import { PayPage } from "../pages/pay/pay";
import { TabsPage } from '../pages/tabs/tabs';
import { CouponPage } from '../pages/coupon/coupon';
import { QrcouponPage } from '../pages/qrcoupon/qrcoupon';
import { PromotionPage } from '../pages/promotion/promotion';
import { PromotionitemsPage } from '../pages/promotionitems/promotionitems';
import { PromotionitemdetailsPage } from '../pages/promotionitemdetails/promotionitemdetails';
import { TutorialPage } from "../pages/tutorial/tutorial";
import { ReferencePage } from "../pages/reference/reference";
import { UnlistdocPage } from "../pages/unlistdoc/unlistdoc";
import { ReferralsPage } from "../pages/referrals/referrals";
import { UpdatereferralPage } from "../pages/updatereferral/updatereferral";
import { ContactsPage } from "../pages/contacts/contacts";
import { AddreferralPage } from "../pages/addreferral/addreferral";

import { MobileNumberAttentionPage } from "../pages/mobile-number-attention/mobile-number-attention";
import {ApplyPage} from "../pages/apply/apply";
import {MobileNumberMatchingPage} from "../pages/mobile-number-matching/mobile-number-matching";
import { Push } from '@ionic-native/push';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { CacheModule } from "ionic-cache";
import { NetworkProvider } from '../providers/network/network';


export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ApplicationDetailsPage,
    DocumentsPage,
    SchedulePage,
    PayPage,
    LoginPage,
    TabsPage,
    TutorialPage,
    MobileNumberVerificationPage,
    MobileNumberSuccessPage,
    MobileNumberAttentionPage,
    ApplyPage,
    CouponPage,
    QrcouponPage,
    PromotionPage,
    PromotionitemdetailsPage,
    PromotionitemsPage,
    ReferencePage,
    UnlistdocPage,
    MobileNumberMatchingPage,
    ReferralsPage,
    AddreferralPage,
    UpdatereferralPage,
    ContactsPage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    CacheModule.forRoot(),
    IonicStorageModule.forRoot({
        name: '__mydb',
        driverOrder: ['sqlite', 'websql', 'indexeddb']
      }
    ),
    HttpModule,
    IonicImageViewerModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ApplicationDetailsPage,
    DocumentsPage,
    SchedulePage,
    PayPage,
    LoginPage,
    TabsPage,
    CouponPage,
    QrcouponPage,
    TutorialPage,
    PromotionPage,
    PromotionitemsPage,
    PromotionitemdetailsPage,
    MobileNumberVerificationPage,
    MobileNumberSuccessPage,
    MobileNumberAttentionPage,
    ApplyPage,
    ReferencePage,
    UnlistdocPage,
    MobileNumberMatchingPage,
    ReferralsPage,
    AddreferralPage,
    UpdatereferralPage,
    ContactsPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Methods,
    AuthServiceProvider,
    Camera,
    AndroidPermissions,
    Device,
    Network,
    Push,
    LocalNotifications,
    NetworkProvider,
    HttpModule,
    Contacts
  ]
})

export class AppModule {}
