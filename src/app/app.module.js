var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { IonicStorageModule } from '@ionic/storage';
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Camera } from '@ionic-native/camera';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Device } from '@ionic-native/device';
import { Network } from '@ionic-native/network';
import { MyApp } from './app.component';
import { Methods } from "../globals/methods";
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { HomePage } from '../pages/home/home';
import { LoginPage } from "../pages/login/login";
import { MobileNumberVerificationPage } from "../pages/mobile-number-verification/mobile-number-verification";
import { MobileNumberSuccessPage } from "../pages/mobile-number-success/mobile-number-success";
import { ApplicationDetailsPage } from "../pages/application-details/application-details";
import { DocumentsPage } from "../pages/documents/documents";
import { SchedulePage } from "../pages/schedule/schedule";
import { PayPage } from "../pages/pay/pay";
import { MobileNumberAttentionPage } from "../pages/mobile-number-attention/mobile-number-attention";
import { ApplyPage } from "../pages/apply/apply";
import { MobileNumberMatchingPage } from "../pages/mobile-number-matching/mobile-number-matching";
export function createTranslateLoader(http) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        NgModule({
            declarations: [
                MyApp,
                HomePage,
                ApplicationDetailsPage,
                DocumentsPage,
                SchedulePage,
                PayPage,
                LoginPage,
                MobileNumberVerificationPage,
                MobileNumberSuccessPage,
                MobileNumberAttentionPage,
                ApplyPage,
                MobileNumberMatchingPage
            ],
            imports: [
                BrowserModule,
                IonicModule.forRoot(MyApp),
                IonicStorageModule.forRoot(),
                HttpModule,
                IonicImageViewerModule,
                HttpClientModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: (createTranslateLoader),
                        deps: [HttpClient]
                    }
                })
            ],
            bootstrap: [IonicApp],
            entryComponents: [
                MyApp,
                HomePage,
                ApplicationDetailsPage,
                DocumentsPage,
                SchedulePage,
                PayPage,
                LoginPage,
                MobileNumberVerificationPage,
                MobileNumberSuccessPage,
                MobileNumberAttentionPage,
                ApplyPage,
                MobileNumberMatchingPage
            ],
            providers: [
                StatusBar,
                SplashScreen,
                { provide: ErrorHandler, useClass: IonicErrorHandler },
                Methods,
                AuthServiceProvider,
                Camera,
                AndroidPermissions,
                Device,
                Network
            ]
        })
    ], AppModule);
    return AppModule;
}());
export { AppModule };
//# sourceMappingURL=app.module.js.map