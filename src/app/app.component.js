var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/home/home';
import { LoginPage } from "../pages/login/login";
import { TranslateService } from '@ngx-translate/core';
var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, storage, translate) {
        var _this = this;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.storage = storage;
        this.translate = translate;
        this.initializeApp(translate);
        this.storage.get('HKID').then(function (val) {
            if (val != null && val != undefined) {
                _this.rootPage = HomePage;
            }
            else {
                _this.rootPage = LoginPage;
            }
        });
    }
    MyApp.prototype.initializeApp = function (translate) {
        translate.setDefaultLang('en');
        this.locale = translate.getBrowserLang();
        console.log("Lang: " + this.locale);
        translate.use(this.locale.match(/en|id/) ? this.locale : 'en');
    };
    MyApp.prototype.ionViewWillEnter = function () {
        this.translate.setDefaultLang('en');
        this.locale = this.translate.getBrowserLang();
        console.log("Lang: " + this.locale);
        this.translate.use(this.locale.match(/en|id/) ? this.locale : 'en');
    };
    MyApp = __decorate([
        Component({
            templateUrl: 'app.html'
        }),
        __metadata("design:paramtypes", [Platform,
            StatusBar,
            SplashScreen,
            Storage,
            TranslateService])
    ], MyApp);
    return MyApp;
}());
export { MyApp };
//# sourceMappingURL=app.component.js.map